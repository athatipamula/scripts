USE [CIDashboards]
GO

IF OBJECT_ID('GetAllCycles', 'P') IS NOT NULL
DROP PROCEDURE GetAllCycles
GO


SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE GetAllCycles
   
AS
SET NOCOUNT ON
  /*-------------------------------------------------------------------------------------*
   * Procedure Name:         GetAllCycles                                                *
   *                                                                                     *
   * Purpose:                This procedure will get all Cycles information.			 *
   *                         get the Login only and only if the account is valid.        *
   *                                                                                     *
   * Customization ID#       None                                                        *
   *                                                                                     *
   * Parameters:             N/A										                 *
   *																					 *
   *                                                                                     *
   * Databases:              1) CIDashboards                                             *
   *                                                                                     *   
   *-------------------------------------------------------------------------------------*
   * Revision | Date Modified | Developer          | Change Summary                      *
   *-------------------------------------------------------------------------------------*
   * 1.0      | 07/07/2016    | Aravind T         | Initial Creation                     *
   *-------------------------------------------------------------------------------------*
   *          |               |                    |                                     *
   *-------------------------------------------------------------------------------------*
   *                                                                                     *
   * Medixsoft Inc. Ekhoi Inc. (c) 2015 All rights reserved								 *
   *-------------------------------------------------------------------------------------*/
   	 Begin
	  SELECT cyl.id_cycle
			  ,cyl.id_account
			  ,cyl.semester_number
			  ,cyl.cycle_number
			  ,cyl.cycle_code
			  ,cyl.cycle_name
			  ,cyl.official_start_date
			  ,cyl.official_end_date
			  ,cyl.extended_end_date
			  ,cyl.instruction_days
			  ,cyl.pk_minutes
			  ,cyl.elementary_school_minutes
			  ,cyl.blended_academy_minutes
			  ,cyl.middle_school_minutes
			  ,cyl.high_school_minutes
			  ,cyl.Comments
			  ,cyl.creation_date
			  ,cyl.created_by
			  ,cyl.last_update_date
			  ,cyl.last_updated_by
			  ,cyl.id_external_reference
	FROM cycle cyl (NOLOCK)
		INNER JOIN account act (NOLOCK) ON cyl.id_account = act.id_account AND
			                            act.enabled_flag = 1    		
		      ORDER BY cyl.id_cycle			  	  
   END

GO

-----------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------

USE [CIDashboards]
GO

IF OBJECT_ID('GetAllSchools', 'P') IS NOT NULL
DROP PROCEDURE GetAllSchools
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE GetAllSchools
   
AS
SET NOCOUNT ON
  /*-------------------------------------------------------------------------------------*
   * Procedure Name:         GetAllSchools                                                *
   *                                                                                     *
   * Purpose:                This procedure will get all school information.			 *
   *                         get the Login only and only if the account is valid.        *
   *                                                                                     *
   * Customization ID#       None                                                        *
   *                                                                                     *
   * Parameters:             N/A										                 *
   *																					 *
   *                                                                                     *
   * Databases:              1) CIDashboards                                             *
   *                                                                                     *   
   *-------------------------------------------------------------------------------------*
   * Revision | Date Modified | Developer          | Change Summary                      *
   *-------------------------------------------------------------------------------------*
   * 1.0      | 07/07/2016    | Aravind T         | Initial Creation                     *
   *-------------------------------------------------------------------------------------*
   *          |               |                    |                                     *
   *-------------------------------------------------------------------------------------*
   *                                                                                     *
   * Medixsoft Inc. Ekhoi Inc. (c) 2015 All rights reserved								 *
   *-------------------------------------------------------------------------------------*/
   	 Begin
	  SELECT  scl.id_school
			  ,scl.id_account
			  ,scl.school_number
			  ,scl.school_code
			  ,scl.school_name
			  ,scl.instruction_type
			  ,scl.school_category
			  ,scl.charter_type
			  ,scl.school_street_address
			  ,scl.school_city
			  ,scl.school_state
			  ,scl.school_zip
			  ,scl.school_phone
			  ,scl.school_fax
			  ,scl.school_email_address
			  ,scl.school_web_page_address
			  ,scl.principal_last_name
			  ,scl.principal_first_name
			  ,scl.principal_middle_name
			  ,scl.principal_email
			  ,scl.Low_grade_level
			  ,scl.high_grade_level
			  ,scl.school_status
			  ,scl.school_latest_enrollment
			  ,scl.latitude
			  ,scl.longitude
			  ,scl.enabled_flag
			  ,scl.Comments
			  ,scl.creation_date
			  ,scl.created_by
			  ,scl.last_update_date
			  ,scl.last_updated_by
			  ,scl.id_external_reference
		FROM school scl (NOLOCK)
		INNER JOIN account act (NOLOCK) ON scl.id_account = act.id_account AND
			                            act.enabled_flag = 1   AND
										scl.enabled_flag = 1   		
		 					
     ORDER BY scl.id_school			
	  	  
   END

GO

-----------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------

USE [CIDashboards]
GO

IF OBJECT_ID('Sp_Cycle_Select', 'P') IS NOT NULL
DROP PROCEDURE Sp_Cycle_Select
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE Sp_Cycle_Select
   @pm_ID_Account bigint ,
   @pm_Cycle_ID  Varchar(100)
AS
SET NOCOUNT ON
  /*-------------------------------------------------------------------------------------*
   * Procedure Name:         Sp_Cycle_Select                                             *
   *                                                                                     *
   * Purpose:                This procedure will get a specific Cycle information,It will*
   *                         get the cycle only and only if the account is valid.        *
   *                                                                                     *
   * Customization ID#       None                                                        *
   *                                                                                     *
   * Parameters:             1) @pm_ID_Account: Internal Account Identifier              *
   *                         2) @pm_Cycle_ID: Internal Cycle Identifier                   *
   *                                                                                     *
   * Databases:              1) CIDashboards                                             *
   *                                                                                     *   
   *-------------------------------------------------------------------------------------*
   * Revision | Date Modified | Developer          | Change Summary                      *
   *-------------------------------------------------------------------------------------*
   * 1.0      | 07/07/2016    | Aravind T         | Initial Creation                     *
   *-------------------------------------------------------------------------------------*
   *          |               |                    |                                     *
   *-------------------------------------------------------------------------------------*
   *                                                                                     *
   * Medixsoft Inc. Ekhoi Inc. (c) 2015 All rights reserved								 *
   *-------------------------------------------------------------------------------------*/
    BEGIN	
      DECLARE @ErrMsg	VARCHAR(500)
      SET @ErrMsg = NULL 
	  IF @pm_ID_Account IS NULL
	     BEGIN
		    SET @ErrMsg = 'Invalid Account ID'
			RETURN @ErrMsg
	     END
      IF @pm_Cycle_ID IS NULL
	     BEGIN
		    SET @ErrMsg = 'Invalid Cycle ID'
			RETURN @ErrMsg
	     END
		 
		SELECT cyl.id_cycle
			  ,cyl.id_account
			  ,cyl.semester_number
			  ,cyl.cycle_number
			  ,cyl.cycle_code
			  ,cyl.cycle_name
			  ,cyl.official_start_date
			  ,cyl.official_end_date
			  ,cyl.extended_end_date
			  ,cyl.instruction_days
			  ,cyl.pk_minutes
			  ,cyl.elementary_school_minutes
			  ,cyl.blended_academy_minutes
			  ,cyl.middle_school_minutes
			  ,cyl.high_school_minutes
			  ,cyl.Comments
			  ,cyl.creation_date
			  ,cyl.created_by
			  ,cyl.last_update_date
			  ,cyl.last_updated_by
			  ,cyl.id_external_reference
  FROM cycle cyl (NOLOCK)
		INNER JOIN account act (NOLOCK) ON cyl.id_account = act.id_account AND
			                            act.enabled_flag = 1  AND  
               							act.ID_Account = @pm_ID_Account		
		WHERE cyl.id_cycle = @pm_Cycle_ID 					
     ORDER BY cyl.id_cycle								  
       
   END
   
GO
-----------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------
USE [CIDashboards]
GO

IF OBJECT_ID('Sp_Document_ActivateInActivate', 'P') IS NOT NULL
DROP PROCEDURE Sp_Document_ActivateInActivate
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


  /*-------------------------------------------------------------------------------------*
   * Procedure Name:         Sp_Document_ActivateInActivate 							 *
   *                                                                                     *
   * Purpose:                This procedure will Update a row into uploaded_document     *
   *                         table	                                                     *
   *                         															 *
   *                                                                                     *
   * Customization ID#       None                                                        *
   *                                                                                     *
   * Parameters:             1) @pm_AccountID                                            *
                             2) @pm_UploadedDocumentID:									 *
   *						 3)	@pm_ActiveFlag:											 *
   *						 4) @pm_LastUpdatedBy:								         *
   *						 5) @pm_ErrMsg:	Output parameter					         *
   *                                                                                     *
   * Databases:              1) CIDashboards                                             *
   *                                                                                     *   
   *-------------------------------------------------------------------------------------*
   * Revision | Date Modified | Developer          | Change Summary                      *
   *-------------------------------------------------------------------------------------*
   * 1.0      | 07/07/2016    | Neelima R          | Initial Creation                    *
   *-------------------------------------------------------------------------------------*
   *          |               |                    |                                     *
   *-------------------------------------------------------------------------------------*
   *                                                                                     *
   * Medixsoft Inc. Ekhoi Inc. (c) 2015 All rights reserved								 *
   *-------------------------------------------------------------------------------------*/
   
   CREATE PROCEDURE Sp_Document_ActivateInActivate
       @pm_AccountID BIGINT ,
	   @pm_UploadedDocumentID BIGINT,
	   @pm_ActiveFlag BIT,
	   @pm_LastUpdatedBy VARCHAR(100),
	   @pm_ErrMsg  VARCHAR(250) OUTPUT
	   	  
	AS
	SET NOCOUNT ON
	BEGIN
	--Update the [uploaded_document] table with ActiveFlag and LastUpdatedBy based on UploadedDocumentID 
		SET @pm_ErrMsg = 'SUCCESS' 
	  IF LTRIM(RTRIM(@pm_AccountID)) IS NULL OR
		 LTRIM(RTRIM(@pm_AccountID)) = ''
	     BEGIN
		    SET @pm_ErrMsg = 'AccountID is required'
			RETURN @pm_ErrMsg
	     END
	  IF LTRIM(RTRIM(@pm_UploadedDocumentID)) IS NULL OR
		 LTRIM(RTRIM(@pm_UploadedDocumentID)) = ''
	     BEGIN
		    SET @pm_ErrMsg = 'UploadedDocumentID is required'
			RETURN @pm_ErrMsg
	     END
	  IF LTRIM(RTRIM(@pm_ActiveFlag)) IS NULL OR
		 LTRIM(RTRIM(@pm_ActiveFlag)) = ''
	     BEGIN
		    SET @pm_ErrMsg = 'ActiveFlag  is required'
			RETURN @pm_ErrMsg
	     END
	   IF LTRIM(RTRIM(@pm_LastUpdatedBy)) IS NULL OR
		  LTRIM(RTRIM(@pm_LastUpdatedBy)) = ''
	     BEGIN
		    SET @pm_ErrMsg = 'Last Update Name  is required'
			RETURN @pm_ErrMsg
	     END
		 IF EXISTS ( SELECT 1 FROM [dbo].[uploaded_document] WHERE id_uploaded_document = @pm_UploadedDocumentID and active_flag = @pm_ActiveFlag )
		BEGIN
		    SELECT @pm_ErrMsg = CASE WHEN @pm_ActiveFlag = 1 THEN 'Uploaded Document is already exists and is Active'
			                         ELSE  'Uploaded Document is already exists and is InActive'    END             
			RETURN @pm_ErrMsg
	     END
		-- IF EXISTS ( SELECT 1 FROM [dbo].[uploaded_document] WHERE id_uploaded_document = @pm_UploadedDocumentID )
		--BEGIN
		--    SET @pm_ErrMsg = 'Uploaded Document ID doesnot exist'
		--	RETURN @pm_ErrMsg
	 --    END
		--Update the uploaded_document with ActiveFlag and LastUpdatedBy
	
		UPDATE [uploaded_document]
			SET  active_flag		 =  @pm_ActiveFlag,
				 last_update_date    = getdate(),
				 last_updated_by	 = @pm_LastUpdatedBy	
		Where id_uploaded_document   = @pm_UploadedDocumentID AND id_account = @pm_AccountID 
										      
	END      
	
GO
-----------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------
USE [CIDashboards]
GO

IF OBJECT_ID('Sp_Document_Delete', 'P') IS NOT NULL
DROP PROCEDURE Sp_Document_Delete
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


  /*-------------------------------------------------------------------------------------*
   * Procedure Name:         Sp_Document_Delete											 *
   *                                                                                     *
   * Purpose:                This procedure will soft Delete a row from uploaded_document*
   *                         table.														 *
   *                                                                                     *
   * Customization ID#       None                                                        *
   *                                                                                     *
   * Parameters:             1) @pm_DocumentID:											 *
   *						 2)	@pm_last_updated_by:									 *
   *                                                                                     *
   * Databases:              1) CIDashboards                                             *
   *                                                                                     *   
   *-------------------------------------------------------------------------------------*
   * Revision | Date Modified | Developer          | Change Summary                      *
   *-------------------------------------------------------------------------------------*
   * 1.0      | 07/06/2016    | Aravind T          | Initial Creation                    *
   *-------------------------------------------------------------------------------------*
   *          |               |                    |                                     *
   *-------------------------------------------------------------------------------------*
   *                                                                                     *
   * Medixsoft Inc. Ekhoi Inc. (c) 2015 All rights reserved								 *
   *-------------------------------------------------------------------------------------*/
   
   CREATE PROCEDURE [dbo].[Sp_Document_Delete] 
       @pm_AccountID BIGINT ,
	   @pm_DocumentID VARCHAR(30),
	   @pm_DeletedFlag bit,
	   @pm_LastUpdatedBy VARCHAR(100) ,
	   @pm_ErrMsg  VARCHAR(250) OUTPUT  
	  
	AS
	SET NOCOUNT ON
		
	BEGIN	

	SET @pm_ErrMsg = 'SUCCESS' 
	  IF LTRIM(RTRIM(@pm_AccountID)) IS NULL OR
		 LTRIM(RTRIM(@pm_AccountID)) = ''
	     BEGIN
		    SET @pm_ErrMsg = 'AccountID is required'
			RETURN @pm_ErrMsg
	     END
	  IF LTRIM(RTRIM(@pm_DocumentID)) IS NULL OR
		 LTRIM(RTRIM(@pm_DocumentID)) = ''
	     BEGIN
		    SET @pm_ErrMsg = 'UploadedDocumentID is required'
			RETURN @pm_ErrMsg
	     END
	  IF LTRIM(RTRIM(@pm_DeletedFlag)) IS NULL OR
		 LTRIM(RTRIM(@pm_DeletedFlag)) = ''
	     BEGIN
		    SET @pm_ErrMsg = 'DeleteFlag  is required'
			RETURN @pm_ErrMsg
	     END
	   IF LTRIM(RTRIM(@pm_LastUpdatedBy)) IS NULL OR
		  LTRIM(RTRIM(@pm_LastUpdatedBy)) = ''
	     BEGIN
		    SET @pm_ErrMsg = 'Last Update Name  is required'
			RETURN @pm_ErrMsg
	     END
		 IF EXISTS ( SELECT 1 FROM [dbo].[uploaded_document] WHERE id_uploaded_document = @pm_DocumentID and deleted_flag = @pm_DeletedFlag )
		BEGIN
		    SET @pm_ErrMsg = 'Uploaded Document is already exists and is Deleted'           
			RETURN @pm_ErrMsg
	     END
		
	/*Here we are soft delete the record thats why we setting active_flag = 0 and deleted_flag = 1 
	--so that it will not permanetly delete from the table*/

		UPDATE uploaded_document
			SET  active_flag = CASE WHEN @pm_DeletedFlag = 1 THEN 0 ELSE 1 END,
				 deleted_flag = @pm_DeletedFlag,
				 last_update_date = GETDATE(),
				 last_updated_by = @pm_LastUpdatedBy			
		    WHERE id_uploaded_document = @pm_DocumentID  AND id_account = @pm_AccountID 
																   
	END  

GO
-----------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------
USE [CIDashboards]
GO

IF OBJECT_ID('Sp_Document_Insert', 'P') IS NOT NULL
DROP PROCEDURE Sp_Document_Insert
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Sp_Document_Insert]
   	   @pm_AccountID BIGINT,
	   @pm_ApplicationID BIGINT,
	   @pm_ApplicationTabID BIGINT,
	   @pm_CycleID BIGINT,
	   @pm_CourseKey VARCHAR(20),
	   @pm_content_if_url VARCHAR(20),
	   @pm_MediaTypeID BIGINT,
	   @pm_DocumentFullName VARCHAR(100),
	   @pm_DocumentDisplayName VARCHAR(30),
	   @pm_document_url VARCHAR(300),
	   @pm_CreatedBy VARCHAR(100),
	   @pm_ErrMsg  VARCHAR(250) OUTPUT ,
	   @pm_DocumentPhysicalLocation VARCHAR(300) OUTPUT
AS
SET NOCOUNT ON
  /*----------------------------------------------------------------------------------------*
   * Procedure Name:         Sp_Document_Insert		   							            *
   *																						*
   * Purpose:                This procedure will get a specific DocumentPhysicalLocation,   *
   *                         For all matching input parameters from table Uploaded_Document	*
   *																						*
   * Customization ID#       None															*
   *																						*
   * Parameters:             1) @pm_AccountID											    *
   *						 2)	@pm_ApplicationID										    *
   *						 3)	@pm_ApplicationTabID									    *
   *						 4)	@pm_CycleID												    *
   *						 5)	@pm_CourseKey											    *
   *						 6)	@pm_MediaTypeID											    *
   *                         6) @pm_content_if_url                                          *
   *						 7)	@pm_DocumentFullName									    *
   *						 8)	@pm_DocumentDisplayName									    *
   *                         9) @pm_document_url                                            *
   *						10)	@pm_CreatedBy											    *
   *						11)	@pm_ErrMsg:	Output parameter							    *
   *                        12) @pm_DocumentPhysicalLocation:	Output parameter            *
   *																						*
   * Databases:              1) CIDashboards												*
   *																						*   
   *---------------------------------------------------------------------------------------	*
   * Revision | Date Modified | Developer          | Change Summary							*
   *---------------------------------------------------------------------------------------	*
   * 1.0      | 07/08/2016    | Neelima R         | Initial Creation						*
   *---------------------------------------------------------------------------------------	*
   *          |               |                    |										*
   *---------------------------------------------------------------------------------------	*
   *																						*
   * Medixsoft Inc. Ekhoi Inc. (c) 2015 All rights reserved									*
   *---------------------------------------------------------------------------------------	*/
   BEGIN	
      
	 SET @pm_ErrMsg = 'SUCCESS' 
	  IF LTRIM(RTRIM(@pm_AccountID)) IS  NULL OR
		 LTRIM(RTRIM(@pm_AccountID)) = ''
	     BEGIN
		    SET @pm_ErrMsg = 'AccountID is required'
			RETURN @pm_ErrMsg
	     END
		 IF LTRIM(RTRIM(@pm_ApplicationID)) IS NULL OR
			LTRIM(RTRIM(@pm_ApplicationID)) = ''
	     BEGIN
		    SET @pm_ErrMsg = 'ApplicationID is required' 
			RETURN @pm_ErrMsg
	     END
	     IF LTRIM(RTRIM(@pm_ApplicationTabID)) IS  NULL OR
	        LTRIM(RTRIM(@pm_ApplicationTabID)) = ''
	     BEGIN
		    SET @pm_ErrMsg = 'ApplicationTabID is required'
			RETURN @pm_ErrMsg
	     END
	  -- IF LTRIM(RTRIM(@pm_CycleID)) IS  NULL OR
	  --   LTRIM(RTRIM(@pm_CycleID)) = ''
    --   BEGIN
		--   SET @pm_ErrMsg = 'CycleID is required'
			--RETURN @pm_ErrMsg
	  --   END
		 IF LTRIM(RTRIM(@pm_CourseKey)) IS  NULL OR
	        LTRIM(RTRIM(@pm_CourseKey)) = ''
	     BEGIN
		    SET @pm_ErrMsg = 'CourseKey is required'
			RETURN @pm_ErrMsg
	     END
		 IF LTRIM(RTRIM(@pm_MediaTypeID)) IS  NULL OR
	     LTRIM(RTRIM(@pm_MediaTypeID)) = ''
	     BEGIN
		    SET @pm_ErrMsg = 'MediaTypeID is required'
			RETURN @pm_ErrMsg
	     END
		 IF (LTRIM(RTRIM(@pm_MediaTypeID)) IS  NULL OR
	     LTRIM(RTRIM(@pm_MediaTypeID)) = '' ) AND ( LTRIM(RTRIM(@pm_content_if_url)) IS  NULL OR
	     LTRIM(RTRIM(@pm_content_if_url)) = '' )
	     BEGIN
		    SET @pm_ErrMsg = 'ContentURL is required'
			RETURN @pm_ErrMsg
	     END
		 IF LTRIM(RTRIM(@pm_DocumentFullName)) IS  NULL OR
	     LTRIM(RTRIM(@pm_DocumentFullName)) = ''
	     BEGIN
		    SET @pm_ErrMsg = 'DocumentFullName is required'
			RETURN @pm_ErrMsg
	     END
		 IF LTRIM(RTRIM(@pm_DocumentDisplayName)) IS  NULL OR
	     LTRIM(RTRIM(@pm_DocumentDisplayName)) = ''
	     BEGIN
		    SET @pm_ErrMsg = 'DocumentDisplayName is required'
			RETURN @pm_ErrMsg
	     END
		  IF LTRIM(RTRIM(@pm_document_url)) IS  NULL OR
	     LTRIM(RTRIM(@pm_document_url)) = ''
	     BEGIN
		    SET @pm_ErrMsg = 'DocumentURL is required'
			RETURN @pm_ErrMsg
	     END
		 IF LTRIM(RTRIM(@pm_CreatedBy)) IS NULL OR
			LTRIM(RTRIM(@pm_CreatedBy)) = ''
	     BEGIN
		    SET @pm_ErrMsg = 'Creation Name is required'
			RETURN @pm_ErrMsg
	     END
	  IF EXISTS ( SELECT 1 FROM [dbo].[uploaded_document] (NOLOCK) WHERE id_account = @pm_AccountID AND id_application = @pm_ApplicationID AND
                                                                         id_application_tab = @pm_ApplicationTabID AND id_cycle = @pm_CycleID AND
                                                                         course_key = @pm_CourseKey AND id_media_type = @pm_MediaTypeID AND
                                                                         document_full_name = @pm_DocumentFullName AND 
																		 document_display_name = @pm_DocumentDisplayName AND
																		 document_url = @pm_document_url )
		BEGIN
		    SELECT @pm_DocumentPhysicalLocation = document_physical_location FROM [dbo].[uploaded_document] (NOLOCK) 
			           WHERE id_account = @pm_AccountID AND id_application = @pm_ApplicationID AND
                             id_application_tab = @pm_ApplicationTabID AND id_cycle = @pm_CycleID AND
                             course_key = @pm_CourseKey AND id_media_type = @pm_MediaTypeID AND
                             document_full_name = @pm_DocumentFullName AND document_display_name = @pm_DocumentDisplayName AND
							 document_url = @pm_document_url
			RETURN @pm_DocumentPhysicalLocation
	     END

		 		 IF LTRIM(RTRIM(@pm_MediaTypeID)) IS NOT NULL AND LTRIM(RTRIM(@pm_content_if_url)) IN ('NULL','')
	     BEGIN
		 INSERT INTO [dbo].[uploaded_document]
           ([id_account]
           ,[id_application]
           ,[id_application_tab]
           ,[id_cycle]
           ,[course_key]
           ,[id_media_type]
		   ,[content_if_url]
           ,[document_full_name]
           ,[document_display_name]
           ,[active_flag]
           ,[document_physical_location]
		   ,[document_url] 
           ,[creation_date]
           ,[created_by]
           ,[last_update_date]
           ,[last_updated_by])
     VALUES
		   (@pm_AccountID ,
			@pm_ApplicationID ,
			@pm_ApplicationTabID ,
			@pm_CycleID ,
			@pm_CourseKey ,
			@pm_MediaTypeID ,
			@pm_content_if_url,
			@pm_DocumentFullName ,
			@pm_DocumentDisplayName ,
			'1',
			--~/1/1/1/5-MATH/1
			CASE WHEN LTRIM(RTRIM(@pm_CycleID)) IS  NULL OR  LTRIM(RTRIM(@pm_CycleID)) = '' THEN 
			'~/'+@pm_AccountID+'/'+@pm_ApplicationID+'/'+@pm_ApplicationTabID+'/'+@pm_CourseKey+'/1'+@pm_MediaTypeID
			ELSE 
			'~/'+@pm_AccountID+'/'+@pm_ApplicationID+'/'+@pm_ApplicationTabID+'/'+@pm_CycleID+'/'+@pm_CourseKey+'/1'+@pm_MediaTypeID 
			END ,
			@pm_document_url ,
			GETDATE(),
			@pm_CreatedBy ,
			GETDATE() ,
			@pm_CreatedBy )

  SELECT @pm_DocumentPhysicalLocation = CASE WHEN LTRIM(RTRIM(@pm_CycleID)) IS  NULL OR  LTRIM(RTRIM(@pm_CycleID)) = '' THEN 
			                             '~/'+@pm_AccountID+'/'+@pm_ApplicationID+'/'+@pm_ApplicationTabID+'/'+@pm_CourseKey+'/1'+@pm_MediaTypeID
			                            ELSE 
			                             '~/'+@pm_AccountID+'/'+@pm_ApplicationID+'/'+@pm_ApplicationTabID+'/'+@pm_CycleID+'/'+@pm_CourseKey+'/1'+@pm_MediaTypeID 
			                            END 

										END

			IF LTRIM(RTRIM(@pm_MediaTypeID)) IS NULL AND LTRIM(RTRIM(@pm_content_if_url)) IS NOT NULL
			 BEGIN
			 INSERT INTO [dbo].[uploaded_document]
			   ([id_account]
			   ,[id_application]
			   ,[id_application_tab]
			   ,[id_cycle]
			   ,[course_key]
			   ,[id_media_type]
			   ,[content_if_url]
			   ,[document_full_name]
			   ,[document_display_name]
			   ,[active_flag]
			   ,[document_physical_location]
			   ,[document_url] 
			   ,[creation_date]
			   ,[created_by]
			   ,[last_update_date]
			   ,[last_updated_by])
		 VALUES
			   (@pm_AccountID ,
				@pm_ApplicationID ,
				@pm_ApplicationTabID ,
				@pm_CycleID ,
				@pm_CourseKey ,
				@pm_MediaTypeID ,
				@pm_content_if_url,
				@pm_DocumentFullName ,
				@pm_DocumentDisplayName ,
				'1',
				'',
				@pm_document_url ,
				GETDATE(),
				@pm_CreatedBy ,
				GETDATE() ,
				@pm_CreatedBy )
		END
	END
 GO
-----------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------
USE [CIDashboards]
GO

IF OBJECT_ID('Sp_Document_Select', 'P') IS NOT NULL
DROP PROCEDURE Sp_Document_Select
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


  /*-------------------------------------------------------------------------------------*
   * Procedure Name:         Sp_Document_Select								             *
   *                                                                                     *
   * Purpose:                This procedure will select all rows from  uploaded_document *
   *                         	table													 *
   *                                                                                     *
   * Customization ID#       None                                                        *
   *                                                                                     *
   * Parameters:             1) @pm_AccountID											 *
   *						 2)	@pm_ApplicationID										 *
   *						 3)	@pm_ApplicationTabID									 *
   *						 4)	@pm_CycleID												 *
   *						 5)	@pm_CourseKey											 *
   *						 6)	@pm_MediaTypeID											 *
   *						 7)	@pm_DocumentFullName									 *
   *						 8)	@pm_DocumentDisplayName									 *
   *						 9)	@pm_CreatedBy											 *
   *						10)	@pm_ErrMsg:	Output parameter							 *
   *                                                                                     *
   * Databases:              1) CIDashboards                                             *
   *                                                                                     *   
   *-------------------------------------------------------------------------------------*
   * Revision | Date Modified | Developer          | Change Summary                      *
   *-------------------------------------------------------------------------------------*
   * 1.0      | 07/07/2016    | Neelima R          | Initial Creation                    *
   *-------------------------------------------------------------------------------------*
   *          |               |                    |                                     *
   *-------------------------------------------------------------------------------------*
   *                                                                                     *
   * Medixsoft Inc. Ekhoi Inc. (c) 2015 All rights reserved								 *
   *-------------------------------------------------------------------------------------*/
   
   CREATE PROCEDURE [dbo].[Sp_Document_Select] 
   	   @pm_AccountID BIGINT,
	   @pm_ApplicationID BIGINT,
	   @pm_ApplicationTabID BIGINT,
	   @pm_CycleID BIGINT,
	   @pm_CourseKey VARCHAR(20),
	   @pm_MediaTypeID BIGINT,
	   @pm_DocumentFullName VARCHAR(100),
	   @pm_DocumentDisplayName VARCHAR(30),
	   @pm_CreatedBy VARCHAR(100),
	   @pm_ErrMsg  VARCHAR(250) OUTPUT

	  
	AS
	SET NOCOUNT ON

	BEGIN

	 SET @pm_ErrMsg = 'SUCCESS' 
	  IF LTRIM(RTRIM(@pm_AccountID)) IS  NULL OR
		 LTRIM(RTRIM(@pm_AccountID)) = ''
	     BEGIN
		    SET @pm_ErrMsg = 'AccountID is required'
			RETURN @pm_ErrMsg
	     END
		 IF LTRIM(RTRIM(@pm_ApplicationID)) IS NULL OR
			LTRIM(RTRIM(@pm_ApplicationID)) = ''
	     BEGIN
		    SET @pm_ErrMsg = 'ApplicationID is required' 
			RETURN @pm_ErrMsg
	     END
	     IF LTRIM(RTRIM(@pm_ApplicationTabID)) IS  NULL OR
	        LTRIM(RTRIM(@pm_ApplicationTabID)) = ''
	     BEGIN
		    SET @pm_ErrMsg = 'ApplicationTabID is required'
			RETURN @pm_ErrMsg
	     END
	  -- IF LTRIM(RTRIM(@pm_CycleID)) IS  NULL OR
	  --   LTRIM(RTRIM(@pm_CycleID)) = ''
    --   BEGIN
		--   SET @pm_ErrMsg = 'CycleID is required'
			--RETURN @pm_ErrMsg
	  --   END
		 IF LTRIM(RTRIM(@pm_CourseKey)) IS  NULL OR
	        LTRIM(RTRIM(@pm_CourseKey)) = ''
	     BEGIN
		    SET @pm_ErrMsg = 'CourseKey is required'
			RETURN @pm_ErrMsg
	     END
		 IF LTRIM(RTRIM(@pm_MediaTypeID)) IS  NULL OR
	     LTRIM(RTRIM(@pm_MediaTypeID)) = ''
	     BEGIN
		    SET @pm_ErrMsg = 'MediaTypeID is required'
			RETURN @pm_ErrMsg
	     END
		 IF LTRIM(RTRIM(@pm_DocumentFullName)) IS  NULL OR
	     LTRIM(RTRIM(@pm_DocumentFullName)) = ''
	     BEGIN
		    SET @pm_ErrMsg = 'DocumentFullName is required'
			RETURN @pm_ErrMsg
	     END
		 IF LTRIM(RTRIM(@pm_DocumentDisplayName)) IS  NULL OR
	     LTRIM(RTRIM(@pm_DocumentDisplayName)) = ''
	     BEGIN
		    SET @pm_ErrMsg = 'DocumentDisplayName is required'
			RETURN @pm_ErrMsg
	     END
		 IF LTRIM(RTRIM(@pm_CreatedBy)) IS NULL OR
			LTRIM(RTRIM(@pm_CreatedBy)) = ''
	     BEGIN
		    SET @pm_ErrMsg = 'Creation Name is required'
			RETURN @pm_ErrMsg
	     END
		SELECT [id_uploaded_document]
			  ,[id_account]
			  ,[id_application]
			  ,[id_application_tab]
			  ,[id_cycle]
			  ,[course_key]
			  ,[id_media_type]
			  ,[document_full_name]
			  ,[document_display_name]
			  ,[content_if_url]
			  ,[active_flag]
			  ,[deleted_flag]
			  ,[display_order]
			  ,[document_url]
			  ,[document_physical_location]
			  ,[Comments]
			  ,[creation_date]
			  ,[created_by]
			  ,[last_update_date]
			  ,[last_updated_by]
			  ,[id_external_reference]
		  FROM [dbo].[uploaded_document] (NOLOCK)
	  WHERE id_account = @pm_AccountID AND id_application = @pm_ApplicationID AND id_application_tab = @pm_ApplicationTabID AND id_cycle = @pm_CycleID AND
	        course_key = @pm_CourseKey AND id_media_type = @pm_MediaTypeID AND document_full_name = @pm_DocumentFullName AND 
			document_display_name = @pm_DocumentDisplayName 
							   
	END  
    
GO
-----------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------
USE [CIDashboards]
GO

IF OBJECT_ID('Sp_Document_Update', 'P') IS NOT NULL
DROP PROCEDURE Sp_Document_Update
GO


SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

  /*-------------------------------------------------------------------------------------*
   * Procedure Name:         Sp_Document_Update											 *
   *                                                                                     *
   * Purpose:                This procedure will Update a row into ApplicationSession	 *
   *                         	table													 *
   *                                                                                     *
   * Customization ID#       None                                                        *
   *                                                                                     *
   * Parameters:             1) @pm_DocumentID:											 *
   *						 2) @pm_AccountID:											 *
   *						 3)	@pm_ApplicationID:										 *
   *						 4)	@pm_ApplicationTabID:									 *
   *						 5)	@pm_CycleID:											 *
   *						 6)	@pm_CourseKey:											 *
   *						 7)	@pm_MediaTypeID:								         *
   *						 8)	@pm_DocumentFullName:								     *
   *						 9)	@pm_DocumentDisplayName:								 *
   *						10) @pm_LastUpdatedBy:								         *
   *						12)	@pm_ErrMsg: Output parameter					         *
   *                                                                                     *
   * Databases:              1) CIDashboards                                             *
   *                                                                                     *   
   *-------------------------------------------------------------------------------------*
   * Revision | Date Modified | Developer          | Change Summary                      *
   *-------------------------------------------------------------------------------------*
   * 1.0      | 01/23/2016    | Aravind T          | Initial Creation                    *
   *-------------------------------------------------------------------------------------*
   *          |               |                    |                                     *
   *-------------------------------------------------------------------------------------*
   *                                                                                     *
   * Medixsoft Inc. Ekhoi Inc. (c) 2015 All rights reserved								 *
   *-------------------------------------------------------------------------------------*/
   
   CREATE PROCEDURE [dbo].[Sp_Document_Update] 
       @pm_DocumentID BIGINT,
   	   @pm_AccountID BIGINT,
	   @pm_ApplicationID BIGINT,
	   @pm_ApplicationTabID BIGINT,
	   @pm_CycleID BIGINT,
	   @pm_CourseKey VARCHAR(20),
	   @pm_content_if_url VARCHAR(20),
	   @pm_MediaTypeCode VARCHAR(10),
	   @pm_DocumentFullName VARCHAR(100),
	   @pm_DocumentDisplayName VARCHAR(30),
	   @pm_document_url VARCHAR(300),
	   @pm_LastUpdatedBy VARCHAR(100),
	   @pm_ErrMsg  VARCHAR(250) OUTPUT 
	  
	AS
	SET NOCOUNT ON
	BEGIN
	SET @pm_ErrMsg = 'SUCCESS' 
	          IF LTRIM(RTRIM(@pm_DocumentID)) IS  NULL OR
				 LTRIM(RTRIM(@pm_DocumentID)) = ''
				 BEGIN
					SET @pm_ErrMsg = 'DocumentID is required'
					RETURN @pm_ErrMsg
				 END
			  IF LTRIM(RTRIM(@pm_AccountID)) IS  NULL OR
				 LTRIM(RTRIM(@pm_AccountID)) = ''
				 BEGIN
					SET @pm_ErrMsg = 'AccountID is required'
					RETURN @pm_ErrMsg
				 END
				 IF LTRIM(RTRIM(@pm_ApplicationID)) IS NULL OR
					LTRIM(RTRIM(@pm_ApplicationID)) = ''
				 BEGIN
					SET @pm_ErrMsg = 'ApplicationID is required' 
					RETURN @pm_ErrMsg
				 END
			  IF LTRIM(RTRIM(@pm_ApplicationTabID)) IS  NULL OR
				 LTRIM(RTRIM(@pm_ApplicationTabID)) = ''
				 BEGIN
					SET @pm_ErrMsg = 'ApplicationTabID is required'
					RETURN @pm_ErrMsg
				 END
				 --IF LTRIM(RTRIM(@pm_CycleID)) IS  NULL OR
				 --LTRIM(RTRIM(@pm_CycleID)) = ''
				 --BEGIN
					--SET @pm_ErrMsg = 'ApplicationSetting Description is required'
					--RETURN @pm_ErrMsg
				 --END
				 IF LTRIM(RTRIM(@pm_CourseKey)) IS  NULL OR
				 LTRIM(RTRIM(@pm_CourseKey)) = ''
				 BEGIN
					SET @pm_ErrMsg = 'CourseKey is required'
					RETURN @pm_ErrMsg
				 END
				 IF LTRIM(RTRIM(@pm_content_if_url)) IS  NULL OR
				 LTRIM(RTRIM(@pm_content_if_url)) = ''
				 BEGIN
					SET @pm_ErrMsg = 'ContentIfUrl is required'
					RETURN @pm_ErrMsg
				 END
				 IF LTRIM(RTRIM(@pm_MediaTypeCode)) IS  NULL OR
				 LTRIM(RTRIM(@pm_MediaTypeCode)) = ''
				 BEGIN
					SET @pm_ErrMsg = 'MediaTypeID is required'
					RETURN @pm_ErrMsg
				 END
				  IF LTRIM(RTRIM(@pm_DocumentFullName)) IS  NULL OR
				 LTRIM(RTRIM(@pm_DocumentFullName)) = ''
				 BEGIN
					SET @pm_ErrMsg = 'DocumentFullName is required'
					RETURN @pm_ErrMsg
				 END
				  IF LTRIM(RTRIM(@pm_DocumentDisplayName)) IS  NULL OR
				 LTRIM(RTRIM(@pm_DocumentDisplayName)) = ''
				 BEGIN
					SET @pm_ErrMsg = 'DocumentDiplayName is required'
					RETURN @pm_ErrMsg
				 END
				  IF LTRIM(RTRIM(@pm_document_url)) IS  NULL OR
				 LTRIM(RTRIM(@pm_document_url)) = ''
				 BEGIN
					SET @pm_ErrMsg = 'DocumentURL is required'
					RETURN @pm_ErrMsg
				 END
			      IF LTRIM(RTRIM(@pm_LastUpdatedBy)) IS NULL OR
			         LTRIM(RTRIM(@pm_LastUpdatedBy)) = ''
				 BEGIN
					SET @pm_ErrMsg = 'Last Update Name is required'
					RETURN @pm_ErrMsg
				 END
         
		 DECLARE @pm_MediaTypeID BIGINT
		 SELECT @pm_MediaTypeID = [id_media_type] FROM [dbo].[media_type] WHERE [media_type_extension] = @pm_MediaTypeCode
		 
		 --Update the table with Application setting code,value and description,LastUpdateDate,LastUpdatedBy
		 IF EXISTS ( SELECT 1 FROM [dbo].[uploaded_document] WHERE [id_uploaded_document]= @pm_DocumentID AND [id_account] = @pm_AccountID AND
		                                                           [id_application] = @pm_ApplicationID AND [id_application_tab] = @pm_ApplicationTabID AND
		                                                           [id_cycle] = @pm_CycleID AND [course_key] = @pm_CourseKey AND 
																   [id_media_type] = @pm_MediaTypeID )
                 BEGIN
				 
		 UPDATE [dbo].[uploaded_document]
		 SET [document_full_name] = @pm_DocumentFullName
		    ,[document_display_name] = @pm_DocumentDisplayName
			,[id_media_type] = @pm_MediaTypeID
			,[document_url] = @pm_document_url
			,[last_update_date] = GETDATE()
			,[last_updated_by] = @pm_LastUpdatedBy
         WHERE [id_uploaded_document]= @pm_DocumentID AND [id_account] = @pm_AccountID AND [id_application] = @pm_ApplicationID AND 
		       [id_application_tab] = @pm_ApplicationTabID AND [id_cycle] = @pm_CycleID AND [course_key] = @pm_CourseKey

			   END

          IF EXISTS ( SELECT 1 FROM [dbo].[uploaded_document] WHERE [id_uploaded_document]= @pm_DocumentID AND [id_account] = @pm_AccountID AND
		                                                           [id_application] = @pm_ApplicationID AND [id_application_tab] = @pm_ApplicationTabID AND
		                                                           [id_cycle] = @pm_CycleID AND [course_key] = @pm_CourseKey AND 
																   [id_media_type] IN ('NULL',''))
                 BEGIN

		 UPDATE [dbo].[uploaded_document]
		 SET [document_full_name] = @pm_DocumentFullName
		    ,[document_display_name] = @pm_DocumentDisplayName
			,[content_if_url] = @pm_content_if_url
			,[last_update_date] = GETDATE()
			,[last_updated_by] = @pm_LastUpdatedBy
         WHERE [id_uploaded_document]= @pm_DocumentID AND [id_account] = @pm_AccountID AND [id_application] = @pm_ApplicationID AND 
		       [id_application_tab] = @pm_ApplicationTabID AND [id_cycle] = @pm_CycleID AND [course_key] = @pm_CourseKey

		END  
	END
    
GO
-----------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------
USE [CIDashboards]
GO

IF OBJECT_ID('Sp_GetAllAccounts', 'P') IS NOT NULL
DROP PROCEDURE Sp_GetAllAccounts
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE Sp_GetAllAccounts
   AS
   SET NOCOUNT ON
  /*-------------------------------------------------------------------------------------*
   * Procedure Name:         Sp_GetAllAccounts                                           *
   *                                                                                     *
   * Purpose:                This procedure will get all the accounts. This way the UI   *
   *                         will validate a user againt the account Chosen.             *
   *                                                                                     *
   * Customization ID#       None                                                        *
   *                                                                                     *
   * Parameters:             None                                                        *
   *                                                                                     *
   * Databases:              1) CIDashboards                                              *
   *                                                                                     *   
   *-------------------------------------------------------------------------------------*
   * Revision | Date Modified | Developer          | Change Summary                      *
   *-------------------------------------------------------------------------------------*
   * 1.0      | 07/05/2016    | Aravind T         | Initial Creation                    *
   *-------------------------------------------------------------------------------------*
   *          |               |                    |                                     *
   *-------------------------------------------------------------------------------------*
   *                                                                                     *
   * Medixsoft Inc. Ekhoi Inc. (c) 2015 All rights reserved                             *
   *-------------------------------------------------------------------------------------*/
   BEGIN	
        SELECT act.id_account
			  ,act.id_account_parent
			  ,act.account_code
			  ,act.account_name
			  ,act.effective_start_date
			  ,act.effective_end_date
			  ,act.domain
			  ,act.county_number
			  ,act.county_Name
			  ,act.region_number
			  ,act.district_number
			  ,act.district_name
			  ,act.district_type
			  ,act.district_street_address
			  ,act.district_city
			  ,act.district_state
			  ,act.district_zip
			  ,act.district_phone
			  ,act.district_fax
			  ,act.district_email_address
			  ,act.district_web_site
			  ,act.district_Superintendent
			  ,act.district_lastest_enrollment
			  ,act.enabled_flag
			  ,act.Comments
			  ,act.creation_date
			  ,act.created_by
			  ,act.last_update_date
			  ,act.last_updated_by
			  ,act.id_external_reference
	      FROM Account act
			   LEFT JOIN Account pact ON act.id_account_parent = pact.ID_Account
          WHERE act.enabled_flag = 1
       ORDER BY act.account_code
END   				  

GO

-----------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------
USE [CIDashboards]
GO

IF OBJECT_ID('Sp_GetColumnsToEncrptDecrypt', 'P') IS NOT NULL
DROP PROCEDURE Sp_GetColumnsToEncrptDecrypt
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE Sp_GetColumnsToEncrptDecrypt
   ( @pm_ID_Account        BigInt
   , @pm_TableName         VARCHAR(MAX) = NULL
   )
AS
   SET NOCOUNT ON
  /*-------------------------------------------------------------------------------------*
   * Procedure Name:         Sp_GetColumnsToEncrptDecrypt                                *
   *                                                                                     *
   * Purpose:                This procedure will get all the accounts. This way the UI   *
   *                         will validate a user againt the account Chosen.             *
   *                                                                                     *
   * Customization ID#       None                                                        *
   *                                                                                     *
   * Parameters:             1)@pm_ID_Account                                            *
   *                         2)@pm_TableName                                             *
   *                                                                                     *
   * Databases:              1)  CIDashboards                                            *
   *                                                                                     *   
   *-------------------------------------------------------------------------------------*
   * Revision | Date Modified | Developer          | Change Summary                      *
   *-------------------------------------------------------------------------------------*
   * 1.0      | 07/05/2016    | Mo. Lahlou         | Initial Creation                    *
   *-------------------------------------------------------------------------------------*
   *          |               |                    |                                     *
   *-------------------------------------------------------------------------------------*
   *                                                                                     *
   * Medixsoft Inc. Ekhoi Inc. (c) 2015 All rights reserved                              *
   *-------------------------------------------------------------------------------------*/
BEGIN
   
   DECLARE @vSqlStatment NVARCHAR(MAX) =''
    SET @vSqlStatment = 
       '
			SELECT GOS.global_option_code   AS TableName
				   , GOS.global_option_key  AS ColumnName
				FROM global_option    GOS 
					 INNER JOIN global_option_category GOC ON GOS.id_global_option_category = GOC.id_global_option_category AND 
															GOS.id_account = GOC.id_account  AND
															GOC.global_option_category_code = ''ENC''
				WHERE GOS.global_option_code = '''+'['+@pm_TableName+']'+''' 
				 AND GOC.enabled_flag = 1
				  AND GOS.enabled_flag = 1
				  AND GOS.global_option_value = ''1''
				  AND GOS.id_account = '+CONVERT(VARCHAR,@pm_ID_Account)+
		' ORDER BY GOS.display_order
       '
	  PRINT @vSqlStatment
	 -- select @vSqlStatment
	  EXECUTE sp_executesql @vSqlStatment  
	
	END

GO
-----------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------
USE [CIDashboards]
GO

IF OBJECT_ID('Sp_GetUserByPerson_Select', 'P') IS NOT NULL
DROP PROCEDURE Sp_GetUserByPerson_Select
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE Sp_GetUserByPerson_Select
    @pm_ID_Account bigint ,
    @pm_FirstName  VARCHAR(100),
    @pm_LastName  VARCHAR(100),
    @pm_DOB  Datetime
AS
SET NOCOUNT ON
  /*----------------------------------------------------------------------------------------*
   * Procedure Name:         Sp_GetUserByPerson_Select												*
   *																						*
   * Purpose:                This procedure will get a specific User Id attribute, It will	*
   *                         get the user only and only if the account,firstname,lastname	*
   *						 Dateof birth is valid.											*
   *																						*
   * Customization ID#       None															*
   *																						*
   * Parameters:             1) @pm_ID_Account: Internal Account Identifier					*
   *                         2) @pm_FirstName: Internal UserFirstName Identifier			*
   *                         2) @pm_LastName: Internal UserLastName Identifier				*
   *                         2) @pm_DOB: Internal UserDateOfBirth Identifier				*
   *																						*
   * Databases:              1) CIDashboards												*
   *																						*   
   *---------------------------------------------------------------------------------------	*
   * Revision | Date Modified | Developer          | Change Summary							*
   *---------------------------------------------------------------------------------------	*
   * 1.0      | 07/05/2016    | Aravind T         | Initial Creation						*
   *---------------------------------------------------------------------------------------	*
   *          |               |                    |										*
   *---------------------------------------------------------------------------------------	*
   *																						*
   * Medixsoft Inc. Ekhoi Inc. (c) 2015 All rights reserved									*
   *---------------------------------------------------------------------------------------	*/
   BEGIN	
      DECLARE @ErrMsg	VARCHAR(500)
      SET @ErrMsg = NULL 
	  IF @pm_ID_Account IS NULL
	     BEGIN
		    SET @ErrMsg = 'Invalid Account ID'
			RETURN @ErrMsg
	     END
      IF @pm_FirstName IS NULL
	     BEGIN
		    SET @ErrMsg = 'Invalid FirstName'
			RETURN @ErrMsg
	     END
	  IF @pm_LastName IS NULL
	     BEGIN
		    SET @ErrMsg = 'Invalid LastName'
			RETURN @ErrMsg
	     END
	  IF @pm_DOB IS NULL
	     BEGIN
		    SET @ErrMsg = 'Invalid Date Of Birth'
			RETURN @ErrMsg
	     END
    SELECT usr.id_user	
			, is_active
			, is_deleted
			, Islocked_out
			, is_approved           
	     FROM [user] usr (NOLOCK)
		      INNER JOIN account act (NOLOCK) ON usr.id_account = act.id_account AND
			                            act.enabled_flag = 1 AND
										act.id_account = @pm_ID_Account
     WHERE usr.first_name = @pm_FirstName
     AND usr.last_name = @pm_LastName
     AND usr.birth_date = @pm_DOB
     
  END
  
GO

-----------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------
USE [CIDashboards]
GO

IF OBJECT_ID('Sp_Global_Option_Category_Delete', 'P') IS NOT NULL
DROP PROCEDURE Sp_Global_Option_Category_Delete
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


  /*-------------------------------------------------------------------------------------*
   * Procedure Name:         Sp_Global_Option_Category_Delete						     *
   *                                                                                     *
   * Purpose:                This procedure will soft Delete a row/rows from Global 	 *
   *                         Option category table										 *
   *                                                                                     *
   * Customization ID#       None                                                        *
   *                                                                                     *
   * Parameters:             1) @pm_GlobalOptionCategoryID:								 *
   *						 2)	@pm_LastUpdatedBy:										 *
   *                                                                                     *
   * Databases:              1) CIDashboards	                                         *
   *                                                                                     *   
   *-------------------------------------------------------------------------------------*
   * Revision | Date Modified | Developer          | Change Summary                      *
   *-------------------------------------------------------------------------------------*
   * 1.0      | 07/07/2016    | Aravind T          | Initial Creation                    *
   *-------------------------------------------------------------------------------------*
   *          |               |                    |                                     *
   *-------------------------------------------------------------------------------------*
   *                                                                                     *
   * Medixsoft Inc. Ekhoi Inc. (c) 2015 All rights reserved								 *
   *-------------------------------------------------------------------------------------*/
   
   CREATE PROCEDURE Sp_Global_Option_Category_Delete
	   @pm_GlobalOptionID bigint,
	   @pm_GlobalOptionCategoryID bigint,
	   @pm_LastUpdatedBy VARCHAR(100)
	   --@pm_ErrMsg  VARCHAR(250) OUTPUT  
	AS
	SET NOCOUNT ON
		
	BEGIN	
			--First Soft Delete Child table then Deleting the record from the Parent table
			--soft delete Child table
			UPDATE global_option				
				SET enabled_flag = 0,
				last_update_date = getdate(),
				last_updated_by = @pm_LastUpdatedBy
			Where id_global_option = @pm_GlobalOptionID
			AND id_global_option_category = @pm_GlobalOptionCategoryID
			--soft delete Parent table
			UPDATE global_option_category				
				SET enabled_flag = 0,
				last_update_date = getdate(),
				last_updated_by = @pm_LastUpdatedBy
			Where id_global_option_category = @pm_GlobalOptionCategoryID
																   
	END  
	
GO
-----------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------
USE [CIDashboards]
GO

IF OBJECT_ID('Sp_Global_Option_Category_Insert', 'P') IS NOT NULL
DROP PROCEDURE Sp_Global_Option_Category_Insert
GO


SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

  /*-------------------------------------------------------------------------------------*
   * Procedure Name:         Sp_Global_Option_Category_Insert							 *
   *                                                                                     *
   * Purpose:                This procedure will Insert a new row into					 *
   *                         	Global_Option_Category table							 *
   *                                                                                     *
   * Customization ID#       None                                                        *
   *                                                                                     *
   * Parameters:             1) @pm_GlobalOptionCategoryID:								 *
   *						 2) @pm_GlobalOptionCategoryCode:							 *
   *						 3)	@pm_GlobalOptionCategoryName:							 *
   *						 4)	@pm_GOC_DisplayOrder:									 *
   *						 5)	@pm_GOC_Comments:										 *
   *						 6)	@pm_GOC_EnableFlag:										 *
   *						 2) @pm_GlobalOptionID:										 *
   *						 3)	@pm_GlobalOptionCode:									 *
   *						 2) @pm_GlobalOptionKey:									 *
   *						 3)	@pm_GlobalOptionValue:									 *
   *						 4)	@pm_GO_DisplayOrder:									 *
   *						 5)	@pm_GO_Comments:										 *
   *						 6)	@pm_GO_EnableFlag:										 *
   *						 7)	@pm_LastUpdatedBy:								         *
   *						 8)	@pm_ErrMsg: Output parameter					         *
   *                                                                                     *
   * Databases:              1) CIDashboards			                                 *
   *                                                                                     *   
   *-------------------------------------------------------------------------------------*
   * Revision | Date Modified | Developer          | Change Summary                      *
   *-------------------------------------------------------------------------------------*
   * 1.0      | 07/07/2016    | Aravind T          | Initial Creation                    *
   *-------------------------------------------------------------------------------------*
   *          |               |                    |                                     *
   *-------------------------------------------------------------------------------------*
   *                                                                                     *
   * Medixsoft Inc. Ekhoi Inc. (c) 2015 All rights reserved								 *
   *-------------------------------------------------------------------------------------*/
   
   CREATE PROCEDURE Sp_Global_Option_Category_Insert 
	   @pm_GlobalOptionCategoryID BIGINT,
	   @pm_GlobalOptionCategoryCode VARCHAR(100),
	   @pm_GlobalOptionCategoryName VARCHAR(250),
	   @pm_GOC_DisplayOrder bigint,
	   @pm_GOC_Comments VARCHAR(4000),
	   @pm_GOC_EnableFlag bit,
	   @pm_GlobalOptionID BIGINT,
	   @pm_GlobalOptionCode VARCHAR(100),
	   @pm_GlobalOptionKey VARCHAR(100),
	   @pm_GlobalOptionValue VARCHAR(250),
	   @pm_GO_DisplayOrder bigint,
	   @pm_GO_Comments VARCHAR(4000),
	   @pm_GO_EnableFlag bit,
	   @pm_CreatedBy VARCHAR(100),
	   @pm_ErrMsg  VARCHAR(250) OUTPUT
	  
	AS
	SET NOCOUNT ON
	BEGIN
	SET @pm_ErrMsg = 'SUCCESS' 
			  IF LTRIM(RTRIM(@pm_GlobalOptionCategoryCode)) IS  NULL OR
				 LTRIM(RTRIM(@pm_GlobalOptionCategoryCode)) = ''
				 BEGIN
					SET @pm_ErrMsg = 'Global Option Category Code is required'
					RETURN @pm_ErrMsg
				 END
				 IF LTRIM(RTRIM(@pm_GlobalOptionCategoryName)) IS NULL OR
					LTRIM(RTRIM(@pm_GlobalOptionCategoryName)) = ''
				 BEGIN
					SET @pm_ErrMsg = 'Global Option Category Name is required' 
					RETURN @pm_ErrMsg
				 END
				IF LTRIM(RTRIM(@pm_GOC_DisplayOrder)) IS  NULL OR
				   LTRIM(RTRIM(@pm_GOC_DisplayOrder)) = ''
				 BEGIN
					SET @pm_ErrMsg = 'Dispay order is required'
					RETURN @pm_ErrMsg
				 END
				 IF LTRIM(RTRIM(@pm_GOC_Comments)) IS  NULL OR
				 LTRIM(RTRIM(@pm_GOC_Comments)) = ''
				 BEGIN
					SET @pm_ErrMsg = 'Comments required'
					RETURN @pm_ErrMsg
				 END
				 IF LTRIM(RTRIM(@pm_GOC_EnableFlag)) IS  NULL OR
				 LTRIM(RTRIM(@pm_GOC_EnableFlag)) = ''
				 BEGIN
					SET @pm_ErrMsg = 'Enable flag is required'
					RETURN @pm_ErrMsg
				 END
				 IF LTRIM(RTRIM(@pm_GlobalOptionCode)) IS  NULL OR
				 LTRIM(RTRIM(@pm_GlobalOptionCode)) = ''
				 BEGIN
					SET @pm_ErrMsg = 'Global Option Code is required'
					RETURN @pm_ErrMsg
				 END
				 IF LTRIM(RTRIM(@pm_GlobalOptionKey)) IS NULL OR
					LTRIM(RTRIM(@pm_GlobalOptionKey)) = ''
				 BEGIN
					SET @pm_ErrMsg = 'Global Option Key is required' 
					RETURN @pm_ErrMsg
				 END
				 IF LTRIM(RTRIM(@pm_GlobalOptionValue)) IS NULL OR
					LTRIM(RTRIM(@pm_GlobalOptionValue)) = ''
				 BEGIN
					SET @pm_ErrMsg = 'Global Option Value is required' 
					RETURN @pm_ErrMsg
				 END
				IF LTRIM(RTRIM(@pm_GO_DisplayOrder)) IS  NULL OR
				   LTRIM(RTRIM(@pm_GO_DisplayOrder)) = ''
				 BEGIN
					SET @pm_ErrMsg = 'Dispay order is required'
					RETURN @pm_ErrMsg
				 END
				 IF LTRIM(RTRIM(@pm_GO_Comments)) IS  NULL OR
				 LTRIM(RTRIM(@pm_GO_Comments)) = ''
				 BEGIN
					SET @pm_ErrMsg = 'Comments required'
					RETURN @pm_ErrMsg
				 END
				 IF LTRIM(RTRIM(@pm_GO_EnableFlag)) IS  NULL OR
				 LTRIM(RTRIM(@pm_GO_EnableFlag)) = ''
				 BEGIN
					SET @pm_ErrMsg = 'Enable flag is required'
					RETURN @pm_ErrMsg
				 END
				 IF LTRIM(RTRIM(@pm_CreatedBy)) IS NULL OR
			     LTRIM(RTRIM(@pm_CreatedBy)) = ''
				 BEGIN
					SET @pm_ErrMsg = 'Created Name is required'
					RETURN @pm_ErrMsg
				 END
				IF EXISTS ( SELECT 1 FROM global_option_category WHERE global_option_category_code = @pm_GlobalOptionCategoryCode
												AND	id_global_option_category != @pm_GlobalOptionCategoryID	)
				BEGIN
					SET @pm_ErrMsg = 'Global Option Category Code already exists'
					RETURN @pm_ErrMsg
				 END
				 IF EXISTS ( SELECT 1 FROM global_option WHERE global_option_code = @pm_GlobalOptionCode
												AND	id_global_option != @pm_GlobalOptionID
												AND id_global_option_category = @pm_GlobalOptionCategoryID	)
				BEGIN
					SET @pm_ErrMsg = 'Global Option,Global Option Category already exists'
					RETURN @pm_ErrMsg
				 END
		 
		 --First Inserting Parent table then inserting into child table
		 --Insert a new Global Option Category Code,Name,display order,Comments,enabled flag,LastUpdateDate,LastUpdatedBy in Global option category table
			INSERT into global_option_category				
				(	 global_option_category_code,
					 global_option_category_name,
					 display_order,
					 comments ,
					 enabled_flag,
					 creation_date,
					 created_by,
					 last_update_date,
					 last_updated_by
				)
				Values
				(	@pm_GlobalOptionCategoryCode,	
					@pm_GlobalOptionCategoryName,
					@pm_GOC_DisplayOrder,
					@pm_GOC_Comments,
					@pm_GOC_EnableFlag,
					getdate(),
					@pm_CreatedBy,
					getdate(),
					@pm_CreatedBy
				)
				--Inserting into child table
				--Insert a new Global Option  Code,Key,Value,display order,Comments,enabled flag,LastUpdateDate,LastUpdatedBy in Global option  table
				Insert into global_option
					  (    global_option_code
						  ,global_option_key
						  ,global_option_value
						  ,enabled_flag
						  ,display_order
						  ,Comments
						  ,creation_date
						  ,created_by
						  ,last_update_date
						  ,last_updated_by
					  )
					  Values
					  ( @pm_GlobalOptionCode,
						@pm_GlobalOptionKey,
						@pm_GlobalOptionValue,
						@pm_GO_EnableFlag,
						@pm_GO_DisplayOrder,
						@pm_GO_Comments,				
						getdate(),
						@pm_CreatedBy,
						getdate(),
						@pm_CreatedBy
					  )

			  SELECT @pm_GlobalOptionID = SCOPE_IDENTITY()								      
	END  
    
GO
-----------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------
USE [CIDashboards]
GO

IF OBJECT_ID('Sp_Global_Option_Category_Select', 'P') IS NOT NULL
DROP PROCEDURE Sp_Global_Option_Category_Select
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

  /*-------------------------------------------------------------------------------------*
   * Procedure Name:         Sp_Global_Option_Category_Select							 *
   *                                                                                     *
   * Purpose:                This procedure will get all Global Option Category			 *
   *                         Information												 *
   *                                                                                     *
   * Customization ID#       None                                                        *
   *                                                                                     *
   * Parameters:             1) @pm_GlobalOptionCategoryCode							 *
   *						 2) @pm_GlobalOptionCategoryname							 *
   *						 3) @pm_DisplayOrder										 *
   *                                                                                     *
   * Databases:              1) CIDashboards			                                 *
   *                                                                                     *   
   *-------------------------------------------------------------------------------------*
   * Revision | Date Modified | Developer          | Change Summary                      *
   *-------------------------------------------------------------------------------------*
   * 1.0      | 07/07/2016    | Aravind T          | Initial Creation                    *
   *-------------------------------------------------------------------------------------*
   *          |               |                    |                                     *
   *-------------------------------------------------------------------------------------*
   *                                                                                     *
   * Medixsoft Inc. Ekhoi Inc. (c) 2015 All rights reserved								 *
   *-------------------------------------------------------------------------------------*/
   
   CREATE PROCEDURE Sp_Global_Option_Category_Select
	    (    @pm_GlobalOptionCategoryCode          VARCHAR(MAX) 
		   , @pm_GlobalOptionCategoryname		 VARCHAR(MAX)
		   , @pm_DisplayOrder   VARCHAR(MAX) 
	     )
	AS
	SET NOCOUNT ON

	BEGIN
	 DECLARE @vWhereClause NVARCHAR(MAX) =' WHERE '
		  -- Application Setting Code
		  IF @pm_GlobalOptionCategoryCode   != ''
		  	 BEGIN
				SET @vWhereClause = @vWhereClause + ' global_option_category_code    IN (' + @pm_GlobalOptionCategoryCode +') AND '
			 END 

		 -- Application Setting Value
		 IF @pm_GlobalOptionCategoryname   != ''
		  	 BEGIN
				SET @vWhereClause = @vWhereClause + '  global_option_category_name   IN (' + @pm_GlobalOptionCategoryname +') AND '
			 END
		 -- Application Setting Description
		 IF @pm_DisplayOrder   != ''
		  	 BEGIN
				SET @vWhereClause = @vWhereClause + ' display_order    IN (' + @pm_DisplayOrder +') AND '
			 END
		  
		IF @vWhereClause = ' WHERE '
			BEGIN 
			SET @vWhereClause =' '
			END
		ELSE
			BEGIN
			SET @vWhereClause = SUBSTRING(@vWhereClause,1,LEN(@vWhereClause)-4)
			END 
		print @vWhereClause

		DECLARE @vSqlStatment NVARCHAR(MAX) =''
		SET @vSqlStatment = 
		' SELECT id_global_option_category
				  ,id_global_option_category_parent
				  ,id_account
				  ,display_order
				  ,global_option_category_code
				  ,global_option_category_name
				  ,comments
				  ,enabled_flag
				  ,creation_date
				  ,created_by
				  ,last_update_date
				  ,last_updated_by
				  ,id_external_reference
			  FROM global_option_category'+ @vWhereClause +
						' order by display_order'

			EXECUTE sp_executesql @vSqlStatment			   
	END  

GO
-----------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------
USE [CIDashboards]
GO

IF OBJECT_ID('Sp_Global_Option_Category_Update', 'P') IS NOT NULL
DROP PROCEDURE Sp_Global_Option_Category_Update
GO


SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

  /*-------------------------------------------------------------------------------------*
   * Procedure Name:         Sp_Global_Option_Category_Update							 *
   *                                                                                     *
   * Purpose:                This procedure will Update a row into Global_Option_Category*
   *                         	table													 *
   *                                                                                     *
   * Customization ID#       None                                                        *
   *                                                                                     *
   * Parameters:             1) @pm_GlobalOptionCategoryID:								 *
   *						 2) @pm_GlobalOptionCategoryCode:							 *
   *						 3)	@pm_GlobalOptionCategoryName:							 *
   *						 4)	@pm_DisplayOrder:										 *
   *						 5)	@pm_Comments:											 *
   *						 6)	@pm_EnableFlag:											 *
   *						 7)	@pm_LastUpdatedBy:								         *
   *						 8)	@pm_ErrMsg: Output parameter					         *
   *                                                                                     *
   * Databases:              1) CIDashboards			                                 *
   *                                                                                     *   
   *-------------------------------------------------------------------------------------*
   * Revision | Date Modified | Developer          | Change Summary                      *
   *-------------------------------------------------------------------------------------*
   * 1.0      | 07/07/2016    | Aravind T          | Initial Creation                    *
   *-------------------------------------------------------------------------------------*
   *          |               |                    |                                     *
   *-------------------------------------------------------------------------------------*
   *                                                                                     *
   * Medixsoft Inc. Ekhoi Inc. (c) 2015 All rights reserved								 *
   *-------------------------------------------------------------------------------------*/
   
   CREATE PROCEDURE Sp_Global_Option_Category_Update 
	   @pm_GlobalOptionCategoryID BIGINT,
	   @pm_GlobalOptionCategoryCode VARCHAR(100),
	   @pm_GlobalOptionCategoryName VARCHAR(250),
	   @pm_GlobalOptionID BIGINT,
	   @pm_GlobalOptionCode VARCHAR(100),
	   @pm_GlobalOptionKey VARCHAR(100),
	   @pm_GlobalOptionValue VARCHAR(250),
	   @pm_GO_DisplayOrder bigint,
	   @pm_GO_Comments VARCHAR(4000),
	   @pm_GO_EnableFlag bit,
	   @pm_GOC_DisplayOrder bigint,
	   @pm_GOC_Comments VARCHAR(4000),
	   @pm_GOC_EnableFlag bit,
	   @pm_LastUpdatedBy VARCHAR(100),	   
	   @pm_ErrMsg  VARCHAR(250) OUTPUT
	  
	AS
	SET NOCOUNT ON
	BEGIN
	SET @pm_ErrMsg = 'SUCCESS' 
			  IF LTRIM(RTRIM(@pm_GlobalOptionCategoryCode)) IS  NULL OR
				 LTRIM(RTRIM(@pm_GlobalOptionCategoryCode)) = ''
				 BEGIN
					SET @pm_ErrMsg = 'Global Option Category Code is required'
					RETURN @pm_ErrMsg
				 END
				 IF LTRIM(RTRIM(@pm_GlobalOptionCategoryName)) IS NULL OR
					LTRIM(RTRIM(@pm_GlobalOptionCategoryName)) = ''
				 BEGIN
					SET @pm_ErrMsg = 'Global Option Category Name is required' 
					RETURN @pm_ErrMsg
				 END
				IF LTRIM(RTRIM(@pm_GOC_DisplayOrder)) IS  NULL OR
				   LTRIM(RTRIM(@pm_GOC_DisplayOrder)) = ''
				 BEGIN
					SET @pm_ErrMsg = 'Dispay order is required'
					RETURN @pm_ErrMsg
				 END
				 IF LTRIM(RTRIM(@pm_GOC_Comments)) IS  NULL OR
				 LTRIM(RTRIM(@pm_GOC_Comments)) = ''
				 BEGIN
					SET @pm_ErrMsg = 'Comments required'
					RETURN @pm_ErrMsg
				 END
				 IF LTRIM(RTRIM(@pm_GOC_EnableFlag)) IS  NULL OR
				 LTRIM(RTRIM(@pm_GOC_EnableFlag)) = ''
				 BEGIN
					SET @pm_ErrMsg = 'Enable flag is required'
					RETURN @pm_ErrMsg
				 END
				 IF LTRIM(RTRIM(@pm_GlobalOptionCode)) IS  NULL OR
				 LTRIM(RTRIM(@pm_GlobalOptionCode)) = ''
				 BEGIN
					SET @pm_ErrMsg = 'Global Option Code is required'
					RETURN @pm_ErrMsg
				 END
				 IF LTRIM(RTRIM(@pm_GlobalOptionKey)) IS NULL OR
					LTRIM(RTRIM(@pm_GlobalOptionKey)) = ''
				 BEGIN
					SET @pm_ErrMsg = 'Global Option Key is required' 
					RETURN @pm_ErrMsg
				 END
				 IF LTRIM(RTRIM(@pm_GlobalOptionValue)) IS NULL OR
					LTRIM(RTRIM(@pm_GlobalOptionValue)) = ''
				 BEGIN
					SET @pm_ErrMsg = 'Global Option Value is required' 
					RETURN @pm_ErrMsg
				 END
				IF LTRIM(RTRIM(@pm_GO_DisplayOrder)) IS  NULL OR
				   LTRIM(RTRIM(@pm_GO_DisplayOrder)) = ''
				 BEGIN
					SET @pm_ErrMsg = 'Dispay order is required'
					RETURN @pm_ErrMsg
				 END
				 IF LTRIM(RTRIM(@pm_GO_Comments)) IS  NULL OR
				 LTRIM(RTRIM(@pm_GO_Comments)) = ''
				 BEGIN
					SET @pm_ErrMsg = 'Comments required'
					RETURN @pm_ErrMsg
				 END
				 IF LTRIM(RTRIM(@pm_GO_EnableFlag)) IS  NULL OR
				 LTRIM(RTRIM(@pm_GO_EnableFlag)) = ''
				 BEGIN
					SET @pm_ErrMsg = 'Enable flag is required'
					RETURN @pm_ErrMsg
				 END
			  IF LTRIM(RTRIM(@pm_LastUpdatedBy)) IS NULL OR
			     LTRIM(RTRIM(@pm_LastUpdatedBy)) = ''
				 BEGIN
					SET @pm_ErrMsg = 'Last Update Name is required'
					RETURN @pm_ErrMsg
				 END
			  IF EXISTS ( SELECT 1 FROM global_option_category WHERE global_option_category_code = @pm_GlobalOptionCategoryCode
												AND	id_global_option_category != @pm_GlobalOptionCategoryID	)
				BEGIN
					SET @pm_ErrMsg = 'Global Option Category Code already exists'
					RETURN @pm_ErrMsg
				 END
			  IF EXISTS ( SELECT 1 FROM global_option WHERE global_option_code = @pm_GlobalOptionCode
												AND	id_global_option != @pm_GlobalOptionID
												AND id_global_option_category = @pm_GlobalOptionCategoryID	)
				BEGIN
					SET @pm_ErrMsg = 'Global Option,Global Option Category already exists'
					RETURN @pm_ErrMsg
				 END
		--Update the table with Global Option Code,Key,Value,display order,Comments,enabled flag,LastUpdateDate,LastUpdatedBy
			UPDATE global_option				
				SET  global_option_code= @pm_GlobalOptionCode,
					 global_option_key =  @pm_GlobalOptionKey,
					 global_option_value =  @pm_GlobalOptionValue,
					 display_order= @pm_GO_DisplayOrder,
					 comments =  @pm_GO_Comments,
					 enabled_flag = @pm_GO_EnableFlag,
					  last_update_date = getdate(),
					  last_updated_by = @pm_LastUpdatedBy
			  Where id_global_option = @pm_GlobalOptionID	 
			  AND id_global_option_category = @pm_GlobalOptionCategoryID
		
		 --Update the table with Global Option Category Code,Name,display order,Comments,enabled flag,LastUpdateDate,LastUpdatedBy
			UPDATE global_option_category				
				SET  global_option_category_code= @pm_GlobalOptionCategoryCode,
					 global_option_category_name =  @pm_GlobalOptionCategoryName,
					 display_order= @pm_GOC_DisplayOrder,
					 comments =  @pm_GOC_Comments,
					 enabled_flag = @pm_GOC_EnableFlag,
					  last_update_date = getdate(),
					  last_updated_by = @pm_LastUpdatedBy
			  Where id_global_option_category = @pm_GlobalOptionCategoryID
										      
	END  
    

GO
-----------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------
USE [CIDashboards]
GO

IF OBJECT_ID('Sp_Global_Option_Select', 'P') IS NOT NULL
DROP PROCEDURE Sp_Global_Option_Select
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


  /*-------------------------------------------------------------------------------------*
   * Procedure Name:         Sp_Global_Option_Select									 *
   *                                                                                     *
   * Purpose:                This procedure will get all Global Option Category			 *
   *                         Information												 *
   *                                                                                     *
   * Customization ID#       None                                                        *
   *                                                                                     *
   * Parameters:             1) @pm_GlobalOptionCode									 *
   *						 2) @pm_GlobalOptionKey										 *
   *						 3) @pm_GlobalOptionValue									 *
   *																					 *
   *                                                                                     *
   * Databases:              1) CIDashboards			                                 *
   *                                                                                     *   
   *-------------------------------------------------------------------------------------*
   * Revision | Date Modified | Developer          | Change Summary                      *
   *-------------------------------------------------------------------------------------*
   * 1.0      | 07/07/2016    | Aravind T          | Initial Creation                    *
   *-------------------------------------------------------------------------------------*
   *          |               |                    |                                     *
   *-------------------------------------------------------------------------------------*
   *                                                                                     *
   * Medixsoft Inc. Ekhoi Inc. (c) 2015 All rights reserved								 *
   *-------------------------------------------------------------------------------------*/
   
   CREATE PROCEDURE Sp_Global_Option_Select 
	    (    @pm_GlobalOptionCode          VARCHAR(MAX) 
		   , @pm_GlobalOptionKey		 VARCHAR(MAX)
		   , @pm_GlobalOptionValue		 VARCHAR(MAX)
	    )
	AS
	SET NOCOUNT ON

	BEGIN
	 DECLARE @vWhereClause NVARCHAR(MAX) =' WHERE '
		  -- global_option_code
		  IF @pm_GlobalOptionCode   != ''
		  	 BEGIN
				SET @vWhereClause = @vWhereClause + ' global_option_code    IN (' + @pm_GlobalOptionCode +') AND '
			 END 

		 -- global_option_key
		 IF @pm_GlobalOptionKey   != ''
		  	 BEGIN
				SET @vWhereClause = @vWhereClause + '  global_option_key  IN (' + @pm_GlobalOptionKey +') AND '
			 END

		 -- global_option_value
		 IF @pm_GlobalOptionValue   != ''
		  	 BEGIN
				SET @vWhereClause = @vWhereClause + '  global_option_value  IN (' + @pm_GlobalOptionValue +') AND '
			 END
			 		 		  
		IF @vWhereClause = ' WHERE '
			BEGIN 
			SET @vWhereClause =' '
			END
		ELSE
			BEGIN
			SET @vWhereClause = SUBSTRING(@vWhereClause,1,LEN(@vWhereClause)-4)
			END 
		print @vWhereClause

		DECLARE @vSqlStatment NVARCHAR(MAX) =''
		SET @vSqlStatment = 
		' SELECT id_global_option
				  ,id_global_option_category
				  ,id_account
				  ,global_option_code
				  ,global_option_key
				  ,global_option_value
				  ,enabled_flag
				  ,display_order
				  ,Comments
				  ,creation_date
				  ,created_by
				  ,last_update_date
				  ,last_updated_by
				  ,id_external_reference
			FROM global_option'+ @vWhereClause +
						' order by display_order'

			EXECUTE sp_executesql @vSqlStatment			   
	END  

GO
-----------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------
USE [CIDashboards]
GO

IF OBJECT_ID('Sp_Honorific_Select', 'P') IS NOT NULL
DROP PROCEDURE Sp_Honorific_Select
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE Sp_Honorific_Select
AS
SET NOCOUNT ON
  /*-------------------------------------------------------------------------------------*
   * Procedure Name:         Sp_Honorific_Select                                         *
   *                                                                                     *
   * Purpose:                This procedure will get all Honorific titles                *
   *                                                                                     *
   * Customization ID#       None                                                        *
   *                                                                                     *
   * Parameters:             None                                                        *
   *                                                                                     *
   * Databases:              1) CIDashboards                                              *
   *                                                                                     *   
   *-------------------------------------------------------------------------------------*
   * Revision | Date Modified | Developer          | Change Summary                      *
   *-------------------------------------------------------------------------------------*
   * 1.0      | 07/05/2016    | Mo. Lahlou         | Initial Creation                    *
   *-------------------------------------------------------------------------------------*
   *          |               |                    |                                     *
   *-------------------------------------------------------------------------------------*
   *                                                                                     *
   * Medixsoft Inc. Ekohi Inc. (c) 2015 All rights reserved                              *
   *-------------------------------------------------------------------------------------*/
   BEGIN                  	   
      DECLARE @ErrMsg	VARCHAR(500)
      SET @ErrMsg = NULL 
       SELECT  hon.id_honorific
      ,hon.honorific_code
      ,hon.honorfic_name
      ,hon.enabled_flag
      ,hon.creation_date
      ,hon.created_by
      ,hon.last_update_date
      ,hon.last_updated_by
      ,hon.id_external_reference
	     FROM honorific hon
        WHERE hon.enabled_flag = 1
     ORDER BY hon.id_honorific desc 
   END

GO

-----------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------
USE [CIDashboards]
GO

IF OBJECT_ID('Sp_Login_Select', 'P') IS NOT NULL
DROP PROCEDURE Sp_Login_Select
GO


SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE Sp_Login_Select
   @pm_ID_Account bigint ,
   @pm_Login_ID  Varchar(100)
AS
SET NOCOUNT ON
  /*-------------------------------------------------------------------------------------*
   * Procedure Name:         Sp_Login_Select                                             *
   *                                                                                     *
   * Purpose:                This procedure will get a specific Login attributes, It will*
   *                         get the Login only and only if the account is valid.        *
   *                                                                                     *
   * Customization ID#       None                                                        *
   *                                                                                     *
   * Parameters:             1) @pm_ID_Account: Internal Account Identifier              *
   *                         2) @pm_Login_ID: Internal User Identifier                   *
   *                                                                                     *
   * Databases:              1) CIDashboards                                             *
   *                                                                                     *   
   *-------------------------------------------------------------------------------------*
   * Revision | Date Modified | Developer          | Change Summary                      *
   *-------------------------------------------------------------------------------------*
   * 1.0      | 07/05/2016    | Aravind T         | Initial Creation                     *
   *-------------------------------------------------------------------------------------*
   *          |               |                    |                                     *
   *-------------------------------------------------------------------------------------*
   *                                                                                     *
   * Medixsoft Inc. Ekhoi Inc. (c) 2015 All rights reserved								 *
   *-------------------------------------------------------------------------------------*/
   BEGIN	
      DECLARE @ErrMsg	VARCHAR(500)
      SET @ErrMsg = NULL 
	  IF @pm_ID_Account IS NULL
	     BEGIN
		    SET @ErrMsg = 'Invalid Account ID'
			RETURN @ErrMsg
	     END
      IF @pm_Login_ID IS NULL
	     BEGIN
		    SET @ErrMsg = 'Invalid Login ID'
			RETURN @ErrMsg
	     END
       SELECT usr.id_user	
			, is_active
			, is_deleted
			, Islocked_out
			, is_approved       
	     FROM [user] usr (NOLOCK)
		      INNER JOIN account act (NOLOCK) ON usr.id_account = act.id_account AND
			                            act.enabled_flag = 1 AND
										act.id_account = @pm_ID_Account
              									  
       WHERE usr.Login_id = @pm_Login_ID 					
     ORDER BY usr.Login_id
   END
   

GO

-----------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------
USE [CIDashboards]
GO

IF OBJECT_ID('Sp_MobileCarrier_Select', 'P') IS NOT NULL
DROP PROCEDURE  Sp_MobileCarrier_Select
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE  Sp_MobileCarrier_Select
AS
SET NOCOUNT ON
  /*-------------------------------------------------------------------------------------*
   * Procedure Name:         Sp_MobileCarrier_Select                                     *
   *                                                                                     *
   * Purpose:                This procedure will get all Mobile Carriers                 *
   *                                                                                     *
   * Customization ID#       None                                                        *
   *                                                                                     *
   * Parameters:             None                                                        *
   *                                                                                     *
   * Databases:              1) CIDashboards                                              *
   *                                                                                     *   
   *-------------------------------------------------------------------------------------*
   * Revision | Date Modified | Developer          | Change Summary                      *
   *-------------------------------------------------------------------------------------*
   * 1.0      | 07/05/2016    | Mo. Lahlou         | Initial Creation                    *
   *-------------------------------------------------------------------------------------*
   *          |               |                    |                                     *
   *-------------------------------------------------------------------------------------*
   *                                                                                     *
   * Medixsoft Inc. Ekhoi Inc. (c) 2015 All rights reserved                              *
   *-------------------------------------------------------------------------------------*/
   BEGIN               	   
      DECLARE @ErrMsg	VARCHAR(500)
      SET @ErrMsg = NULL 
       SELECT mcr.id_mobile_carrier
			  ,mcr.mobile_carrier_code
			  ,mcr.mobile_carrier_name
			  ,mcr.gateway
			  ,mcr.enabled_flag
			  ,mcr.creation_date
			  ,mcr.created_by
			  ,mcr.last_update_date
			  ,mcr.last_updated_by
			  ,mcr.id_external_reference
	     FROM mobile_carrier mcr
        WHERE mcr.enabled_flag = 1
     ORDER BY mcr.mobile_carrier_code 
   END

GO

-----------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------
USE [CIDashboards]
GO

IF OBJECT_ID('Sp_School_Select', 'P') IS NOT NULL
DROP PROCEDURE Sp_School_Select
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE  Sp_School_Select
   @pm_ID_Account bigint ,
   @pm_School_ID  Varchar(100)
AS
SET NOCOUNT ON
  /*-------------------------------------------------------------------------------------*
   * Procedure Name:         Sp_School_Select                                            *
   *                                                                                     *
   * Purpose:                This procedure will get a specific School , It will		 *
   *                         get the particular school only and only if the account      *
   *                         is valid                                                    *
   * Customization ID#       None                                                        *
   *                                                                                     *
   * Parameters:             1) @pm_ID_Account: Internal Account Identifier              *
   *                         2) @pm_School_ID: Internal User Identifier                  *
   *                                                                                     *
   * Databases:              1) CIDashboards                                             *
   *                                                                                     *   
   *-------------------------------------------------------------------------------------*
   * Revision | Date Modified | Developer          | Change Summary                      *
   *-------------------------------------------------------------------------------------*
   * 1.0      | 07/07/2016    | Aravind T         | Initial Creation                     *
   *-------------------------------------------------------------------------------------*
   *          |               |                    |                                     *
   *-------------------------------------------------------------------------------------*
   *                                                                                     *
   * Medixsoft Inc. Ekhoi Inc. (c) 2015 All rights reserved								 *
   *-------------------------------------------------------------------------------------*/
   	 BEGIN	
      DECLARE @ErrMsg	VARCHAR(500)
      SET @ErrMsg = NULL 
	  IF @pm_ID_Account IS NULL
	     BEGIN
		    SET @ErrMsg = 'Invalid Account ID'
			RETURN @ErrMsg
	     END
      IF @pm_School_ID IS NULL
	     BEGIN
		    SET @ErrMsg = 'Invalid School ID'
			RETURN @ErrMsg
	     END 
		 SELECT  scl.id_school
			  ,scl.id_account
			  ,scl.school_number
			  ,scl.school_code
			  ,scl.school_name
			  ,scl.instruction_type
			  ,scl.school_category
			  ,scl.charter_type
			  ,scl.school_street_address
			  ,scl.school_city
			  ,scl.school_state
			  ,scl.school_zip
			  ,scl.school_phone
			  ,scl.school_fax
			  ,scl.school_email_address
			  ,scl.school_web_page_address
			  ,scl.principal_last_name
			  ,scl.principal_first_name
			  ,scl.principal_middle_name
			  ,scl.principal_email
			  ,scl.Low_grade_level
			  ,scl.high_grade_level
			  ,scl.school_status
			  ,scl.school_latest_enrollment
			  ,scl.latitude
			  ,scl.longitude
			  ,scl.enabled_flag
			  ,scl.Comments
			  ,scl.creation_date
			  ,scl.created_by
			  ,scl.last_update_date
			  ,scl.last_updated_by
			  ,scl.id_external_reference
		FROM school scl (NOLOCK)
		INNER JOIN account act (NOLOCK) ON scl.id_account = act.id_account AND
			                            act.enabled_flag = 1        AND
										act.ID_Account = @pm_ID_Account		
		WHERE scl.id_school = @pm_School_ID 					
     ORDER BY scl.id_school			
	  	  
   END
   
GO

-----------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------
USE [CIDashboards]
GO

IF OBJECT_ID('Sp_StateSession_Insert', 'P') IS NOT NULL
DROP PROCEDURE Sp_StateSession_Insert
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE Sp_StateSession_Insert
   @pm_SessionGUID Varchar(100)
      
AS
SET NOCOUNT ON
  /*-------------------------------------------------------------------------------------*
   * Procedure Name:         Sp_StateSession_Insert                                       *
   *                                                                                     *
   * Purpose:                This procedure will Insert a new row SessionID	if there	 *
   *                         are no SessionGUID	and SessionStartTime					 *
   *                                                                                     *
   * Customization ID#       None                                                        *
   *                                                                                     *
   * Parameters:             1) @pm_SessionGUID: Internal Session GUID Identifier        *
   *                                                                                     *
   * Databases:              1) CIDashboards                                             *
   *                                                                                     *   
   *-------------------------------------------------------------------------------------*
   * Revision | Date Modified | Developer          | Change Summary                      *
   *-------------------------------------------------------------------------------------*
   * 1.0      | 07/05/2016    | Aravind T          | Initial Creation                    *
   *-------------------------------------------------------------------------------------*
   *          |               |                    |                                     *
   *-------------------------------------------------------------------------------------*
   *                                                                                     *
   * Medixsoft Inc. Ekhoi Inc. (c) 2015 All rights reserved								 *
   *-------------------------------------------------------------------------------------*/
   BEGIN	
      IF @pm_SessionGUID IS NOT NULL
	     BEGIN
		    INSERT INTO state_session(session_guid) OUTPUT INSERTED.id_session VALUES(@pm_SessionGUID)
	     END
   END

GO
-----------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------
USE [CIDashboards]
GO

IF OBJECT_ID('Sp_StateSession_Select', 'P') IS NOT NULL
DROP PROCEDURE Sp_StateSession_Select
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE Sp_StateSession_Select
  @pm_ID_Session bigint 
AS
SET NOCOUNT ON
  /*-------------------------------------------------------------------------------------*
   * Procedure Name:         Sp_StateSession_Select                                      *
   *                                                                                     *
   * Purpose:                This procedure will get a specific StateSession  attributes,*
   *                         It will get the Session information only and only if the	 *
   *						 account and user is valid.									 *
   *                                                                                     *
   * Customization ID#       None                                                        *
   *                                                                                     *
   * Parameters:             1) @pm_ID_Account: Internal Account Identifier              *
   *                         2) @pm_ID_User: Internal User Identifier                    *
   *                                                                                     *
   * Databases:              1) CIDashboards                                              *
   *                                                                                     *   
   *-------------------------------------------------------------------------------------*
   * Revision | Date Modified | Developer          | Change Summary                      *
   *-------------------------------------------------------------------------------------*
   * 1.0      | 07/05/2016    | Aravind T          | Initial Creation                    *
   *-------------------------------------------------------------------------------------*
   *          |               |                    |                                     *
   *-------------------------------------------------------------------------------------*
   *                                                                                     *
   * Medixsoft Inc. Ekhoi Inc. (c) 2015 All rights reserved                             *
   *-------------------------------------------------------------------------------------*/
   BEGIN	
       SELECT ss.id_session
			  ,ss.session_guid
			  ,ss.session_start_time
			  ,ss.expiration_date
			  ,ss.session_end_time
			  ,ss.lock_date
			  ,ss.lock_date_local
			  ,ss.lock_cookie
			  ,ss.[time_out]
			  ,ss.locked
			  ,ss.session_data_short
			  ,ss.session_data_image
			  ,ss.flags
			  ,ss.session_data_XML
			  ,ss.host_ip
			  ,ss.cookie_data_encryption_salt
			  ,ss.id_account
			  ,ss.id_user
			  ,ss.creation_date
		FROM state_session ss 
			where id_session =  @pm_ID_Session		      
   END

GO

-----------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------
USE [CIDashboards]
GO

IF OBJECT_ID('Sp_StateSession_Timeout_Select', 'P') IS NOT NULL
DROP PROCEDURE Sp_StateSession_Timeout_Select
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE Sp_StateSession_Timeout_Select
		 @pm_ID_Session bigint
		AS
	/*-------------------------------------------------------------------------------------*
   * Procedure Name:         Sp_StateSession_Timeout_Select                                *
   *                                                                                       *
   * Purpose:                This procedure will Update a particular session if			   *
   *						 												               *
   *																		               *
   *																		               *
   * Customization ID#       None														   *
   *                                                                                       *
   * Parameters:             1) @pm_ID_Session: Internal Session ID                        *
   *                                                                                       *
   * Databases:              1)  CIDashboards                                              *
   *                                                                                       *   
   *-------------------------------------------------------------------------------------  *
   * Revision | Date Modified | Developer          | Change Summary                        *
   *-------------------------------------------------------------------------------------  *
   * 1.0      | 07/05/2016    | Aravind T          | Initial Creation                      *
   *-------------------------------------------------------------------------------------  *
   *          |               |                    |                                       *
   *-------------------------------------------------------------------------------------  *
   *                                                                                       *
   * Medixsoft Inc. Ekhoi Inc. (c) 2015 All rights reserved                                *
   *-------------------------------------------------------------------------------------  */
   
	BEGIN
	SELECT 
		[time_out] FROM state_session
		WHERE id_session = @pm_ID_Session   
	END

GO
-----------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------
USE [CIDashboards]
GO

IF OBJECT_ID('Sp_StateSession_Update', 'P') IS NOT NULL
DROP PROCEDURE Sp_StateSession_Update
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

	/*-------------------------------------------------------------------------------------*
   * Procedure Name:         Sp_StateSession_Update                                        *
   *                                                                                       *
   * Purpose:                This procedure will Update a particular Application session.  *
   *                         It will update all the values based on application Session ID *
   *                         and Session ID is tied to ApplicationSessionID                *
   * Customization ID#       None														   *
   *                                                                                       *
   * Parameters:             None                                                          *
   *                                                                                       *
   * Databases:              1) CIDashboards                                               *
   *                                                                                       *   
   *-------------------------------------------------------------------------------------  *
   * Revision | Date Modified | Developer          | Change Summary                        *
   *-------------------------------------------------------------------------------------  *
   * 1.0      | 07/05/2016    | Aravind T          | Initial Creation                      *
   *-------------------------------------------------------------------------------------  *
   *          |               |                    |                                       *
   *-------------------------------------------------------------------------------------  *
   *                                                                                       *
   * Medixsoft Inc. Ekhoi Inc. (c) 2015 All rights reserved                                *
   *-------------------------------------------------------------------------------------  */

	CREATE PROCEDURE Sp_StateSession_Update
		 @pm_ID_Session bigint
		,@pm_ID_Account bigint
		,@pm_ID_User bigint
		,@pm_SessionGUID nvarchar(250)
		,@pm_SessionStartTime datetime
		,@pm_ExpirationDate datetime
		,@pm_SessionEndTime datetime
		,@pm_LockDate datetime
		,@pm_LockDateLocal datetime
		,@pm_LockCookie bit
		,@pm_Timeout bit
		,@pm_Locked bit
		,@pm_SessionDataShort varbinary(max)
		,@pm_SessionDataImage image
		,@pm_Flags	bigint
		,@pm_SessionDataXML xml
		,@pm_HostIP	varchar(50)
		,@pm_CookieDataEncryptionSalt nvarchar(200)
		
		AS
			BEGIN
				IF @pm_ID_Session IS NOT NULL
					UPDATE state_session
						SET		 id_user				= @pm_ID_User
								,id_account				= @pm_ID_Account
								,session_start_time		= @pm_SessionStartTime
								,expiration_date		= @pm_ExpirationDate
								,session_end_time		= @pm_SessionEndTime
								,lock_date				= @pm_LockDate
								,lock_date_local		= @pm_LockDateLocal
								,lock_cookie			= @pm_LockCookie
								,[time_out]				= @pm_Timeout
								,locked					= @pm_Locked
								,session_data_short		= @pm_SessionDataShort
								,session_data_image		= @pm_SessionDataImage
								,flags					= @pm_Flags
								,session_data_XML		= @pm_SessionDataXML  
								,host_ip				= @pm_HostIP
								,cookie_data_encryption_salt = @pm_CookieDataEncryptionSalt
								
					FROM state_session 
						 Where id_session = @pm_ID_Session
						 AND session_guid = @pm_SessionGUID
		END

	GO

-----------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------
USE [CIDashboards]
GO

IF OBJECT_ID('Sp_StateSessionApplication_Insert', 'P') IS NOT NULL
DROP PROCEDURE Sp_StateSessionApplication_Insert
GO


SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE Sp_StateSessionApplication_Insert
   @pm_SessionApplicationStartTime Varchar(100),
   @pm_ID_Session bigint
AS
SET NOCOUNT ON
  /*-------------------------------------------------------------------------------------*
   * Procedure Name:         Sp_StateSessionApplication_Insert                           *
   *                                                                                     *
   * Purpose:                This procedure will Insert a new row ApplicationSessionID	 *
   *                         if ApplcationSession Starts								 *
   *                                                                                     *
   * Customization ID#       None                                                        *
   *                                                                                     *
   * Parameters:             1) @pm_SessionGUID: Internal Session GUID Identifier        *
   *                                                                                     *
   * Databases:              1) CIDashboards                                             *
   *                                                                                     *   
   *-------------------------------------------------------------------------------------*
   * Revision | Date Modified | Developer          | Change Summary                      *
   *-------------------------------------------------------------------------------------*
   * 1.0      | 07/05/2016    | Aravind T          | Initial Creation                    *
   *-------------------------------------------------------------------------------------*
   *          |               |                    |                                     *
   *-------------------------------------------------------------------------------------*
   *                                                                                     *
   * Medixsoft Inc. Ekhoi Inc. (c) 2015 All rights reserved								 *
   *-------------------------------------------------------------------------------------*/
   BEGIN	
      IF @pm_SessionApplicationStartTime IS NOT NULL
	     BEGIN
		    INSERT INTO state_session_application(session_application_start_time,id_session) OUTPUT INSERTED.id_session_application
				VALUES(@pm_SessionApplicationStartTime,@pm_ID_Session)
	     END
   END

GO

-----------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------
USE [CIDashboards]
GO

IF OBJECT_ID('Sp_StateSessionApplication_Select', 'P') IS NOT NULL
DROP PROCEDURE Sp_StateSessionApplication_Select
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE Sp_StateSessionApplication_Select
   @pm_ID_Account bigint, 
   @pm_ID_Session bigint,
   @pm_ID_User bigint,
   @pm_ID_SessionApplication bigint
AS
SET NOCOUNT ON
  /*-------------------------------------------------------------------------------------*
   * Procedure Name:         Sp_StateSessionApplication_Select                           *
   *                                                                                     *
   * Purpose:                This procedure will get a specific StateSessionApplication  *
   *                         attributes,It will get the Session information only and	 *
   *						  only if the account is valid.								 *
   *                                                                                     *
   * Customization ID#       None                                                        *
   *                                                                                     *
   * Parameters:             1) @pm_ID_Account: Internal Account Identifier              *
   *                         2) @pm_ID_Session: Internal Account Identifier              *
   *                         3) @pm_ID_User: Internal Account Identifier                 *
   *                         4) @pm_ID_SessionApplication: Internal Account Identifier   *
   *                                                                                     *
   *                                                                                     *
   * Databases:              1) CIDashboards                                             *
   *                                                                                     *   
   *-------------------------------------------------------------------------------------*
   * Revision | Date Modified | Developer          | Change Summary                      *
   *-------------------------------------------------------------------------------------*
   * 1.0      | 07/05/2016    | Aravind T          | Initial Creation                    *
   *-------------------------------------------------------------------------------------*
   *          |               |                    |                                     *
   *-------------------------------------------------------------------------------------*
   *                                                                                     *
   * Medixsoft Inc. Ekhoi Inc. (c) 2015 All rights reserved                             *
   *-------------------------------------------------------------------------------------*/
   BEGIN	
      DECLARE @WhereClause NVARCHAR(MAX) = ' WHERE '
      DECLARE @SqlStatment NVARCHAR(MAX) = ''
      DECLARE @OrderBy     NVARCHAR(MAX) = 'ssa.session_application_start_time'
      DECLARE @Direction   NVARCHAR(MAX) = 'DESC'
      DECLARE @ErrMsg	   VARCHAR(500)
      SET @ErrMsg = NULL 
	  IF @pm_ID_SessionApplication IS NULL
	     BEGIN
		    SET @ErrMsg = 'Invalid ApplicationSession ID'
			RETURN @ErrMsg
	     END
      SET @WhereClause = @WhereClause + ' ssa.id_session_application = ' + CONVERT(VARCHAR,@pm_ID_SessionApplication) + ' AND '
	  -- Account
	     IF @pm_ID_User IS NOT NULL
	     BEGIN
	        SET @WhereClause = @WhereClause + ' ssa.id_account = ' + CONVERT(VARCHAR,@pm_ID_Account) + ' AND '
		END
      -- StateSession
	   IF @pm_ID_Session IS NOT NULL
	     BEGIN
            SET @WhereClause = @WhereClause + ' ssa.id_session = ' + CONVERT(VARCHAR,@pm_ID_Session) + ' AND '        
		 END
      -- Finalizing the Where Clause
      IF @WhereClause = ' WHERE '
         BEGIN 
            SET @WhereClause =' '
         END
      ELSE
         BEGIN
            SET @WhereClause = SUBSTRING(@WhereClause,1,LEN(@WhereClause)-4)
         END 
      SET @SqlStatment = 
	  'SELECT ssa.id_session_application
			  ,ssa.session_application_start_time
			  ,ssa.expiration_date
			  ,ssa.session_end_time
			  ,ssa.id_application
			  ,ssa.session_data_xml
			  ,ssa.id_account
			  ,ssa.id_session			 
		FROM state_session_application ssa
		     INNER JOIN account act ON ssa.id_account = act.id_account AND
			                            act.enabledflag = 1          
			LEFT JOIN state_session ss ON ss.id_session = ssa.id_session
			LEFT JOIN [user] usr ON usr.id_user = ss.id_user 					
		'
	  + @WhereClause 
	  + '  ORDER BY '+@OrderBy+' '+@Direction

      SELECT @SqlStatment
      EXECUTE sp_executesql @SqlStatment

   END

GO
-----------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------
USE [CIDashboards]
GO

IF OBJECT_ID('Sp_User_Select', 'P') IS NOT NULL
DROP PROCEDURE Sp_User_Select
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE Sp_User_Select
   @pm_ID_Account bigint ,
   @pm_ID_User    bigint
AS
SET NOCOUNT ON
  /*-------------------------------------------------------------------------------------*
   * Procedure Name:         Sp_User_Select                                              *
   *                                                                                     *
   * Purpose:                This procedure will get a specific User attributes, It will *
   *                         get the user only and only if the account is valid.         *
   *                                                                                     *
   * Customization ID#       None                                                        *
   *                                                                                     *
   * Parameters:             1) @pm_ID_Account: Internal Account Identifier               *
   *                         2) @pm_ID_User: Internal User Identifier                     *
   *                                                                                     *
   * Databases:              1) CIDashboards                                              *
   *                                                                                     *   
   *-------------------------------------------------------------------------------------*
   * Revision | Date Modified | Developer          | Change Summary                      *
   *-------------------------------------------------------------------------------------*
   * 1.0      | 07/05/2016    | Aravind T          | Initial Creation                    *
   *-------------------------------------------------------------------------------------*
   *          |               |                    |                                     *
   *-------------------------------------------------------------------------------------*
   *                                                                                     *
   * Medixsoft Inc. Ekhoi Inc. (c) 2015 All rights reserved                             *
   *-------------------------------------------------------------------------------------*/
   BEGIN	
      DECLARE @ErrMsg	VARCHAR(500)
      SET @ErrMsg = NULL 
	  IF @pm_ID_Account IS NULL
	     BEGIN
		    SET @ErrMsg = 'Invalid Account ID'
			RETURN @ErrMsg
	     END
      IF @pm_ID_User IS NULL
	     BEGIN
		    SET @ErrMsg = 'Invalid User ID'
			RETURN @ErrMsg
	     END
       SELECT usr.id_user
            , usr.first_name
            , usr.middle_name
            , usr.last_name
            , usr.full_name
            , usr.preferred_name
            , usr.alias_name
            , usr.employee_no
            , usr.generation
            , usr.comments
            , usr.birth_date
            , usr.ssn
            , usr.is_active
            , usr.creation_date
            , usr.created_by
            , usr.last_update_date
            , usr.last_updated_by
            , usr.id_external_reference
            , usr.Login_id
            , usr.domain
            , usr.email_address
            , usr.[password]
            , usr.password_salt
            , usr.is_deleted
            , usr.id_directory
            , usr.directory_type
            , usr.directory_cn
            , usr.directory_usn_changed
            , usr.directory_when_changed
            , usr.directory_when_sychronized
            , usr.Islocked_out
            , usr.last_login_date
            , usr.password_expiry_date
            , usr.mobile_phone_number
            , usr.password_format
            , usr.mobile_pin
            , usr.is_approved
            , usr.last_password_changed_date
            , usr.last_locked_out_date
            , usr.failed_password_attempt_count
            , usr.failed_password_answer_attempt_count
            , usr.id_honorific
			, hnr.honorific_code
			, hnr.honorfic_name
            , usr.id_mobile_carrier
            , mbc.mobile_carrier_code
            , mbc.mobile_carrier_name
            , mbc.gateway
            , usr.id_account
            , act.account_code
            , act.account_name
            , usr.last_approved_date
            , usr.last_active_date
            , usr.last_deleted_date
            , usr.failed_login_count			
	     FROM [user] usr
		      INNER JOIN account act ON usr.id_account = act.id_account AND
			                            act.enabled_flag = 1            AND
										act.id_account = @pm_ID_Account
              LEFT JOIN honorific hnr ON hnr.id_honorific = usr.id_honorific AND
			                             hnr.enabled_flag = 1                 
              LEFT JOIN mobile_carrier mbc ON mbc.id_mobile_carrier = usr.id_mobile_carrier AND
			                                 mbc.enabled_flag = 1  
        WHERE usr.id_user = @pm_ID_User
     ORDER BY usr.id_user
   END

GO
-----------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------
USE [CIDashboards]
GO

IF OBJECT_ID('Sp_GetMediaType', 'P') IS NOT NULL
DROP PROCEDURE Sp_GetMediaType
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


  /*-------------------------------------------------------------------------------------*
   * Procedure Name:         Sp_GetMediaType 							                 *
   *                                                                                     *
   * Purpose:                This procedure will select all rows from dbo.media_type     *
   *                         	table													 *
   *                                                                                     *
   * Customization ID#       None                                                        *
   *                                                                                     *
   * Parameters:             1) @pm_media_type_extension								 *
   *						 2)	@pm_ErrMsg:	Output parameter							 *
   *                                                                                     *
   * Databases:              1) CIDashboards                                             *
   *                                                                                     *   
   *-------------------------------------------------------------------------------------*
   * Revision | Date Modified | Developer          | Change Summary                      *
   *-------------------------------------------------------------------------------------*
   * 1.0      | 07/07/2016    | Neelima R          | Initial Creation                    *
   *-------------------------------------------------------------------------------------*
   *          |               |                    |                                     *
   *-------------------------------------------------------------------------------------*
   *                                                                                     *
   * Medixsoft Inc. Ekhoi Inc. (c) 2015 All rights reserved								 *
   *-------------------------------------------------------------------------------------*/
   
   CREATE PROCEDURE Sp_GetMediaType 
   	   @pm_media_type_extension VARCHAR(30),
	   @pm_ErrMsg  VARCHAR(250) OUTPUT

	  
	AS
	SET NOCOUNT ON

	BEGIN

	 SET @pm_ErrMsg = 'SUCCESS' 
	  IF LTRIM(RTRIM(@pm_media_type_extension)) IS  NULL OR
		 LTRIM(RTRIM(@pm_media_type_extension)) = ''
	     BEGIN
		    SET @pm_ErrMsg = 'Media Type Extension is required'
			RETURN @pm_ErrMsg
	     END
		 
		SELECT DISTINCT id_media_type,media_type_extension,media_type_desc,display_order,icon_location 
		FROM [dbo].[media_type]
		WHERE UPPER(media_type_extension) = CASE WHEN UPPER(@pm_media_type_extension) = 'ALL' THEN UPPER(media_type_extension)
                                                 ELSE UPPER(@pm_media_type_extension)
                                                 END
		AND id_account = 1 AND enabled_flag = 1
        ORDER BY 1
							   
	END  
    
GO
-----------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------

-----------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------







