USE [CIDashboards]
GO
/****** Object:  StoredProcedure [dbo].[Sp_Cycle_Select]    Script Date: 7/10/2016 4:07:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Sp_Cycle_Select]
   @pm_AccountID BIGINT ,
   @pm_Cycle_ID  Varchar(100),
   @pm_ErrMsg  VARCHAR(250) OUTPUT
AS
SET NOCOUNT ON
  /*-------------------------------------------------------------------------------------*
   * Procedure Name:         Sp_Cycle_Select                                             *
   *                                                                                     *
   * Purpose:                This procedure will get a specific Cycle information,It will*
   *                         get the cycle only and only if the account is valid.        *
   *                                                                                     *
   * Customization ID#       None                                                        *
   *                                                                                     *
   * Parameters:             1) @pm_ID_Account: Internal Account Identifier              *
   *                         2) @pm_Cycle_ID: Internal Cycle Identifier                   *
   *                                                                                     *
   * Databases:              1) CIDashboards                                             *
   *                                                                                     *   
   *-------------------------------------------------------------------------------------*
   * Revision | Date Modified | Developer          | Change Summary                      *
   *-------------------------------------------------------------------------------------*
   * 1.0      | 07/07/2016    | Aravind T         | Initial Creation                     *
   *-------------------------------------------------------------------------------------*
   *          |               |                    |                                     *
   *-------------------------------------------------------------------------------------*
   *                                                                                     *
   * Medixsoft Inc. Ekhoi Inc. (c) 2015 All rights reserved								 *
   *-------------------------------------------------------------------------------------*/
    BEGIN	
      SET @pm_ErrMsg = 'SUCCESS' 
	  IF LTRIM(RTRIM(@pm_AccountID)) IS NULL OR
		 LTRIM(RTRIM(@pm_AccountID)) = ''
	     BEGIN
		    SET @pm_ErrMsg = 'Account ID is required'
			RETURN @pm_ErrMsg
	     END
      IF  LTRIM(RTRIM(@pm_Cycle_ID)) IS NULL OR
		 LTRIM(RTRIM(@pm_Cycle_ID)) = ''
	     BEGIN
		    SET @pm_ErrMsg = 'Cycle ID is required'
			RETURN @pm_ErrMsg
	     END
		 
		SELECT cyl.id_cycle
			  ,cyl.id_account
			  ,cyl.semester_number
			  ,cyl.cycle_number
			  ,cyl.cycle_code
			  ,cyl.cycle_name
			  ,cyl.official_start_date
			  ,cyl.official_end_date
			  ,cyl.extended_end_date
			  ,cyl.instruction_days
			  ,cyl.pk_minutes
			  ,cyl.elementary_school_minutes
			  ,cyl.blended_academy_minutes
			  ,cyl.middle_school_minutes
			  ,cyl.high_school_minutes
			  ,cyl.Comments
			  ,cyl.creation_date
			  ,cyl.created_by
			  ,cyl.last_update_date
			  ,cyl.last_updated_by
			  ,cyl.id_external_reference
  FROM cycle cyl (NOLOCK)
		INNER JOIN account act (NOLOCK) ON cyl.id_account = act.id_account AND
			                            act.enabled_flag = 1  AND  
               							act.ID_Account = @pm_AccountID		
		WHERE cyl.id_cycle = @pm_Cycle_ID 					
     ORDER BY cyl.id_cycle								  
       
   END
   

GO
/****** Object:  StoredProcedure [dbo].[Sp_Document_ActivateInActivate]    Script Date: 7/10/2016 4:07:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
   CREATE PROCEDURE [dbo].[Sp_Document_ActivateInActivate]
       @pm_AccountID BIGINT ,
	   @pm_UploadedDocumentID BIGINT,
	   @pm_ActiveFlag BIT,
	   @pm_LastUpdatedBy VARCHAR(100),
	   @pm_ErrMsg  VARCHAR(250) OUTPUT
	   	  
	AS
	SET NOCOUNT ON
	BEGIN
	--Update the [uploaded_document] table with ActiveFlag and LastUpdatedBy based on UploadedDocumentID 
		SET @pm_ErrMsg = 'SUCCESS' 
	  IF LTRIM(RTRIM(@pm_AccountID)) IS NULL OR
		 LTRIM(RTRIM(@pm_AccountID)) = ''
	     BEGIN
		    SET @pm_ErrMsg = 'AccountID is required'
			RETURN @pm_ErrMsg
	     END
	  IF LTRIM(RTRIM(@pm_UploadedDocumentID)) IS NULL OR
		 LTRIM(RTRIM(@pm_UploadedDocumentID)) = ''
	     BEGIN
		    SET @pm_ErrMsg = 'UploadedDocumentID is required'
			RETURN @pm_ErrMsg
	     END
	  IF LTRIM(RTRIM(@pm_ActiveFlag)) IS NULL OR
		 LTRIM(RTRIM(@pm_ActiveFlag)) = ''
	     BEGIN
		    SET @pm_ErrMsg = 'ActiveFlag  is required'
			RETURN @pm_ErrMsg
	     END
	   IF LTRIM(RTRIM(@pm_LastUpdatedBy)) IS NULL OR
		  LTRIM(RTRIM(@pm_LastUpdatedBy)) = ''
	     BEGIN
		    SET @pm_ErrMsg = 'Last Update Name  is required'
			RETURN @pm_ErrMsg
	     END
		 IF EXISTS ( SELECT 1 FROM [dbo].[uploaded_document] WHERE id_uploaded_document = @pm_UploadedDocumentID and active_flag = @pm_ActiveFlag )
		BEGIN
		    SELECT @pm_ErrMsg = CASE WHEN @pm_ActiveFlag = 1 THEN 'Uploaded Document is already exists and is Active'
			                         ELSE  'Uploaded Document is already exists and is InActive'    END             
			RETURN @pm_ErrMsg
	     END
		-- IF EXISTS ( SELECT 1 FROM [dbo].[uploaded_document] WHERE id_uploaded_document = @pm_UploadedDocumentID )
		--BEGIN
		--    SET @pm_ErrMsg = 'Uploaded Document ID doesnot exist'
		--	RETURN @pm_ErrMsg
	 --    END
		--Update the uploaded_document with ActiveFlag and LastUpdatedBy
	
		UPDATE [uploaded_document]
			SET  active_flag		 =  @pm_ActiveFlag,
				 last_update_date    = getdate(),
				 last_updated_by	 = @pm_LastUpdatedBy	
		Where id_uploaded_document   = @pm_UploadedDocumentID AND id_account = @pm_AccountID 
										      
	END      
	



GO
/****** Object:  StoredProcedure [dbo].[Sp_Document_Application_Select]    Script Date: 7/10/2016 4:07:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
   CREATE PROCEDURE [dbo].[Sp_Document_Application_Select] 
   	   @pm_AccountID BIGINT,
	   @pm_ErrMsg  VARCHAR(250) OUTPUT
	   	  
	AS
	SET NOCOUNT ON

	BEGIN

	 SET @pm_ErrMsg = 'SUCCESS' 
	  IF LTRIM(RTRIM(@pm_AccountID)) IS  NULL OR
		 LTRIM(RTRIM(@pm_AccountID)) = ''
	     BEGIN
		    SET @pm_ErrMsg = 'AccountID is required'
			RETURN @pm_ErrMsg
	     END

	     
	SELECT 	   
	   AccApp.[id_account]
	  ,App.[id_application]
      ,App.[application_code]
      ,App.[application_name]
      ,App.[application_desc]
      ,App.[enabled_flag]
      ,App.[comments] As ApplicationComments
      ,App.[id_external_reference] As ApplicationExternalReference
	  ,AccApp.[purchased_date]
	  ,AccApp.[expired_date]
	  ,AccApp.[renewal_date]
	  ,AccApp.[registration_key_date]
	  ,AccApp.[registration_key]
	  ,AccApp.[Comments] As AccuntApplicationComments
	  ,AccApp.[domain]
	  ,AccApp.[id_external_reference] AS AccuntApplicationExternalReference
  FROM [dbo].[application] App (NOLOCK)
  INNER JOIN [dbo].[account_application] AccApp ON AccApp.[id_application] = App.[id_application]
	  WHERE App.enabled_flag = 1 AND AccApp.id_account = @pm_AccountID
							   
	END  
    



GO
/****** Object:  StoredProcedure [dbo].[Sp_Document_ApplicationTab_Select]    Script Date: 7/10/2016 4:07:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
   CREATE PROCEDURE [dbo].[Sp_Document_ApplicationTab_Select] 
   	   @pm_AccountID BIGINT,
	   @pm_ApplicationID BIGINT,
	   @pm_ErrMsg  VARCHAR(250) OUTPUT

	  
	AS
	SET NOCOUNT ON

	BEGIN

	 SET @pm_ErrMsg = 'SUCCESS' 
	  IF LTRIM(RTRIM(@pm_AccountID)) IS  NULL OR
		 LTRIM(RTRIM(@pm_AccountID)) = ''
	     BEGIN
		    SET @pm_ErrMsg = 'AccountID is required'
			RETURN @pm_ErrMsg
	     END
		 IF LTRIM(RTRIM(@pm_ApplicationID)) IS NULL OR
			LTRIM(RTRIM(@pm_ApplicationID)) = ''
	     BEGIN
		    SET @pm_ErrMsg = 'ApplicationID is required' 
			RETURN @pm_ErrMsg
	     END
	     
	SELECT [id_application_tab]
			,[id_account]
			,[id_application]
			,[application_tab_code]
			,[application_tab_name]
			,[display_order]
			,[has_cycle_flag]
			,[enabled_flag]
			,[Comments]
			,[creation_date]
			,[created_by]
			,[last_update_date]
			,[last_updated_by]
			,[id_external_reference]
		FROM [dbo].[application_tab] (NOLOCK)
	  WHERE id_account = @pm_AccountID AND id_application = @pm_ApplicationID 
							   
	END  
    



GO
/****** Object:  StoredProcedure [dbo].[Sp_Document_Delete]    Script Date: 7/10/2016 4:07:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
   CREATE PROCEDURE [dbo].[Sp_Document_Delete] 
       @pm_AccountID BIGINT ,
	   @pm_DocumentID VARCHAR(30),
	   @pm_DeletedFlag bit,
	   @pm_LastUpdatedBy VARCHAR(100) ,
	   @pm_ErrMsg  VARCHAR(250) OUTPUT  
	  
	AS
	SET NOCOUNT ON
		
	BEGIN	

	SET @pm_ErrMsg = 'SUCCESS' 
	  IF LTRIM(RTRIM(@pm_AccountID)) IS NULL OR
		 LTRIM(RTRIM(@pm_AccountID)) = ''
	     BEGIN
		    SET @pm_ErrMsg = 'AccountID is required'
			RETURN @pm_ErrMsg
	     END
	  IF LTRIM(RTRIM(@pm_DocumentID)) IS NULL OR
		 LTRIM(RTRIM(@pm_DocumentID)) = ''
	     BEGIN
		    SET @pm_ErrMsg = 'UploadedDocumentID is required'
			RETURN @pm_ErrMsg
	     END
	  IF LTRIM(RTRIM(@pm_DeletedFlag)) IS NULL OR
		 LTRIM(RTRIM(@pm_DeletedFlag)) = ''
	     BEGIN
		    SET @pm_ErrMsg = 'DeleteFlag  is required'
			RETURN @pm_ErrMsg
	     END
	   IF LTRIM(RTRIM(@pm_LastUpdatedBy)) IS NULL OR
		  LTRIM(RTRIM(@pm_LastUpdatedBy)) = ''
	     BEGIN
		    SET @pm_ErrMsg = 'Last Update Name  is required'
			RETURN @pm_ErrMsg
	     END
		 IF EXISTS ( SELECT 1 FROM [dbo].[uploaded_document] WHERE id_uploaded_document = @pm_DocumentID and deleted_flag = @pm_DeletedFlag )
		BEGIN
		    SET @pm_ErrMsg = 'Uploaded Document is already exists and is Deleted'           
			RETURN @pm_ErrMsg
	     END
		
	/*Here we are soft delete the record thats why we setting active_flag = 0 and deleted_flag = 1 
	--so that it will not permanetly delete from the table*/

		UPDATE uploaded_document
			SET  active_flag = CASE WHEN @pm_DeletedFlag = 1 THEN 0 ELSE 1 END,
				 deleted_flag = @pm_DeletedFlag,
				 last_update_date = GETDATE(),
				 last_updated_by = @pm_LastUpdatedBy			
		    WHERE id_uploaded_document = @pm_DocumentID  AND id_account = @pm_AccountID 
																   
	END  




GO
/****** Object:  StoredProcedure [dbo].[Sp_Document_Insert]    Script Date: 7/10/2016 4:07:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Sp_Document_Insert]
   	   @pm_AccountID BIGINT,
	   @pm_ApplicationID BIGINT,
	   @pm_ApplicationTabID BIGINT,
	   @pm_CycleID BIGINT = NULL,
	   @pm_CourseID BIGINT,
	   @pm_content_if_url VARCHAR(20)=NULL,
	   @pm_MediaTypeID BIGINT,
	   @pm_DocumentFullName VARCHAR(100),
	   @pm_DocumentDisplayName VARCHAR(30),
	   @pm_document_url VARCHAR(300)=NULL,
	   @pm_CreatedBy VARCHAR(100),
	   @pm_ErrMsg  VARCHAR(250) OUTPUT ,
	   @pm_DocumentPhysicalLocation VARCHAR(300) OUTPUT
AS
SET NOCOUNT ON
  /*----------------------------------------------------------------------------------------*
   * Procedure Name:         Sp_Document_Insert		   							            *
   *																						*
   * Purpose:                This procedure will get a specific DocumentPhysicalLocation,   *
   *                         For all matching input parameters from table Uploaded_Document	*
   *																						*
   * Customization ID#       None															*
   *																						*
   * Parameters:             1) @pm_AccountID											    *
   *						 2)	@pm_ApplicationID										    *
   *						 3)	@pm_ApplicationTabID									    *
   *						 4)	@pm_CycleID												    *
   *						 5)	@pm_CourseID											    *
   *						 6)	@pm_MediaTypeID											    *
   *                         6) @pm_content_if_url                                          *
   *						 7)	@pm_DocumentFullName									    *
   *						 8)	@pm_DocumentDisplayName									    *
   *                         9) @pm_document_url                                            *
   *						10)	@pm_CreatedBy											    *
   *						11)	@pm_ErrMsg:	Output parameter							    *
   *                        12) @pm_DocumentPhysicalLocation:	Output parameter            *
   *																						*
   * Databases:              1) CIDashboards												*
   *																						*   
   *---------------------------------------------------------------------------------------	*
   * Revision | Date Modified | Developer          | Change Summary							*
   *---------------------------------------------------------------------------------------	*
   * 1.0      | 07/08/2016    | Neelima R         | Initial Creation						*
   *---------------------------------------------------------------------------------------	*
   *          |               |                    |										*
   *---------------------------------------------------------------------------------------	*
   *																						*
   * Medixsoft Inc. Ekhoi Inc. (c) 2015 All rights reserved									*
   *---------------------------------------------------------------------------------------	*/
   BEGIN	
      
	 SET @pm_ErrMsg = 'SUCCESS' 


		 DECLARE @pm_CourseKey VARCHAR(20)
		 SELECT @pm_CourseKey = [course_key] FROM [dbo].[course] WHERE [id_course] = @pm_CourseID
		                                                           AND id_account = @pm_AccountID
																   AND [enabled_flag] = 1


		 	
		 INSERT INTO [dbo].[uploaded_document]
           ([id_account]
           ,[id_application]
           ,[id_application_tab]
           ,[id_cycle]
           ,[course_key]
           ,[id_media_type]
		   ,[content_if_url]
           ,[document_full_name]
           ,[document_display_name]
           ,[active_flag]
           ,[document_physical_location]
		   ,[document_url] 
           ,[creation_date]
           ,[created_by]
           ,[last_update_date]
           ,[last_updated_by])
     VALUES
		   (@pm_AccountID ,
			@pm_ApplicationID ,
			@pm_ApplicationTabID ,
			@pm_CycleID ,
			@pm_CourseKey ,
			@pm_MediaTypeID ,
			@pm_content_if_url,
			@pm_DocumentFullName ,
			@pm_DocumentDisplayName ,
			'1',
			--/1/1/1/5-MATH/1
			'' ,
			@pm_document_url ,
			GETDATE(),
			@pm_CreatedBy ,
			GETDATE() ,
			@pm_CreatedBy )

  DECLARE @pm_ID_Uploaded_Document BIGINT
  SELECT @pm_ID_Uploaded_Document = [id_uploaded_document] FROM [dbo].[uploaded_document]
  WHERE id_account = @pm_AccountID AND id_application = @pm_ApplicationID AND
                             id_application_tab = @pm_ApplicationTabID AND
                             course_key = @pm_CourseKey AND id_media_type = @pm_MediaTypeID AND
                             document_full_name = @pm_DocumentFullName AND document_display_name = @pm_DocumentDisplayName 
							 --AND document_url = @pm_document_url

UPDATE 	[dbo].[uploaded_document]	
SET [document_physical_location] = CASE WHEN LTRIM(RTRIM(@pm_CycleID)) IS  NULL OR  LTRIM(RTRIM(@pm_CycleID)) = '' THEN 
			                              CAST(@pm_AccountID AS VARCHAR(10))+'\'
										 +CAST(@pm_ApplicationID AS VARCHAR(10))+'\'
										 +CAST(@pm_ApplicationTabID AS VARCHAR(10))+'\'
										 +@pm_CourseKey+'\'
										 +(CAST(@pm_ID_Uploaded_Document AS VARCHAR(10)))
			                            ELSE 
			                             CAST(@pm_AccountID AS VARCHAR(10))+'\'
										 +CAST(@pm_ApplicationID AS VARCHAR(10))+'\'
										 +CAST(@pm_ApplicationTabID AS VARCHAR(10))+'\'
										 +CAST(@pm_CycleID AS VARCHAR(10))+'\'
										 +@pm_CourseKey+'\'
										 +(CAST(@pm_ID_Uploaded_Document AS VARCHAR(10))) 
			                            END 
WHERE 	id_account = @pm_AccountID AND id_application = @pm_ApplicationID AND
                             id_application_tab = @pm_ApplicationTabID AND
                             course_key = @pm_CourseKey AND id_media_type = @pm_MediaTypeID AND
                             document_full_name = @pm_DocumentFullName AND document_display_name = @pm_DocumentDisplayName 
							 --AND document_url = @pm_document_url		 

  SELECT @pm_DocumentPhysicalLocation = CASE WHEN LTRIM(RTRIM(@pm_CycleID)) IS  NULL OR  LTRIM(RTRIM(@pm_CycleID)) = '' THEN 
			                              CAST(@pm_AccountID AS VARCHAR(10))+'\'
										 +CAST(@pm_ApplicationID AS VARCHAR(10))+'\'
										 +CAST(@pm_ApplicationTabID AS VARCHAR(10))+'\'
										 +@pm_CourseKey+'\'
										 +(CAST(@pm_ID_Uploaded_Document AS VARCHAR(10)))
			                            ELSE 
			                             CAST(@pm_AccountID AS VARCHAR(10))+'\'
										 +CAST(@pm_ApplicationID AS VARCHAR(10))+'\'
										 +CAST(@pm_ApplicationTabID AS VARCHAR(10))+'\'
										 +CAST(@pm_CycleID AS VARCHAR(10))+'\'
										 +@pm_CourseKey+'\'
										 +(CAST(@pm_ID_Uploaded_Document AS VARCHAR(10))) 
			                            END 

										END





GO
/****** Object:  StoredProcedure [dbo].[Sp_Document_Select]    Script Date: 7/10/2016 4:07:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
   CREATE PROCEDURE [dbo].[Sp_Document_Select] 
   	   @pm_AccountID BIGINT,
	   @pm_ApplicationID BIGINT,
	   @pm_ApplicationTabID BIGINT,
	   @pm_CycleID BIGINT=NULL,
	   @pm_CourseKey VARCHAR(20),
	   @pm_MediaTypeID BIGINT = NULL,
	   @pm_DocumentFullName VARCHAR(100),
	   @pm_DocumentDisplayName VARCHAR(30),
	   @pm_CreatedBy VARCHAR(100),
	   @pm_ErrMsg  VARCHAR(250) OUTPUT

	  
	AS
	SET NOCOUNT ON

	BEGIN

	 SET @pm_ErrMsg = 'SUCCESS' 
	  IF LTRIM(RTRIM(@pm_AccountID)) IS  NULL OR
		 LTRIM(RTRIM(@pm_AccountID)) = ''
	     BEGIN
		    SET @pm_ErrMsg = 'AccountID is required'
			RETURN @pm_ErrMsg
	     END
		 IF LTRIM(RTRIM(@pm_ApplicationID)) IS NULL OR
			LTRIM(RTRIM(@pm_ApplicationID)) = ''
	     BEGIN
		    SET @pm_ErrMsg = 'ApplicationID is required' 
			RETURN @pm_ErrMsg
	     END
	     IF LTRIM(RTRIM(@pm_ApplicationTabID)) IS  NULL OR
	        LTRIM(RTRIM(@pm_ApplicationTabID)) = ''
	     BEGIN
		    SET @pm_ErrMsg = 'ApplicationTabID is required'
			RETURN @pm_ErrMsg
	     END
	  -- IF LTRIM(RTRIM(@pm_CycleID)) IS  NULL OR
	  --   LTRIM(RTRIM(@pm_CycleID)) = ''
    --   BEGIN
		--   SET @pm_ErrMsg = 'CycleID is required'
			--RETURN @pm_ErrMsg
	  --   END
		 IF LTRIM(RTRIM(@pm_CourseKey)) IS  NULL OR
	        LTRIM(RTRIM(@pm_CourseKey)) = ''
	     BEGIN
		    SET @pm_ErrMsg = 'CourseKey is required'
			RETURN @pm_ErrMsg
	     END
		 IF LTRIM(RTRIM(@pm_MediaTypeID)) IS  NULL OR
	     LTRIM(RTRIM(@pm_MediaTypeID)) = ''
	     BEGIN
		    SET @pm_ErrMsg = 'MediaTypeID is required'
			RETURN @pm_ErrMsg
	     END
		 IF LTRIM(RTRIM(@pm_DocumentFullName)) IS  NULL OR
	     LTRIM(RTRIM(@pm_DocumentFullName)) = ''
	     BEGIN
		    SET @pm_ErrMsg = 'DocumentFullName is required'
			RETURN @pm_ErrMsg
	     END
		 IF LTRIM(RTRIM(@pm_DocumentDisplayName)) IS  NULL OR
	     LTRIM(RTRIM(@pm_DocumentDisplayName)) = ''
	     BEGIN
		    SET @pm_ErrMsg = 'DocumentDisplayName is required'
			RETURN @pm_ErrMsg
	     END
		 IF LTRIM(RTRIM(@pm_CreatedBy)) IS NULL OR
			LTRIM(RTRIM(@pm_CreatedBy)) = ''
	     BEGIN
		    SET @pm_ErrMsg = 'Creation Name is required'
			RETURN @pm_ErrMsg
	     END
		SELECT [id_uploaded_document]
			  ,[id_account]
			  ,[id_application]
			  ,[id_application_tab]
			  ,[id_cycle]
			  ,[course_key]
			  ,[id_media_type]
			  ,[document_full_name]
			  ,[document_display_name]
			  ,[content_if_url]
			  ,[active_flag]
			  ,[deleted_flag]
			  ,[display_order]
			  ,[document_url]
			  ,[document_physical_location]
			  ,[Comments]
			  ,[creation_date]
			  ,[created_by]
			  ,[last_update_date]
			  ,[last_updated_by]
			  ,[id_external_reference]
		  FROM [dbo].[uploaded_document] (NOLOCK)
	  WHERE id_account = @pm_AccountID AND id_application = @pm_ApplicationID AND id_application_tab = @pm_ApplicationTabID AND id_cycle = @pm_CycleID AND
	        course_key = @pm_CourseKey AND id_media_type = @pm_MediaTypeID AND document_full_name = @pm_DocumentFullName AND 
			document_display_name = @pm_DocumentDisplayName 
							   
	END  
    



GO
/****** Object:  StoredProcedure [dbo].[Sp_Document_Update]    Script Date: 7/10/2016 4:07:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
   CREATE PROCEDURE [dbo].[Sp_Document_Update] 
       @pm_DocumentID BIGINT,
   	   @pm_AccountID BIGINT,
	   @pm_ApplicationID BIGINT,
	   @pm_ApplicationTabID BIGINT,
	   @pm_CycleID BIGINT,
	   @pm_CourseKey VARCHAR(20),
	   @pm_content_if_url VARCHAR(20),
	   @pm_MediaTypeCode VARCHAR(10),
	   @pm_DocumentFullName VARCHAR(100),
	   @pm_DocumentDisplayName VARCHAR(30),
	   @pm_document_url VARCHAR(300),
	   @pm_LastUpdatedBy VARCHAR(100),
	   @pm_ErrMsg  VARCHAR(250) OUTPUT 
	  
	AS
	SET NOCOUNT ON
	BEGIN
	SET @pm_ErrMsg = 'SUCCESS' 
	          IF LTRIM(RTRIM(@pm_DocumentID)) IS  NULL OR
				 LTRIM(RTRIM(@pm_DocumentID)) = ''
				 BEGIN
					SET @pm_ErrMsg = 'DocumentID is required'
					RETURN @pm_ErrMsg
				 END
			  IF LTRIM(RTRIM(@pm_AccountID)) IS  NULL OR
				 LTRIM(RTRIM(@pm_AccountID)) = ''
				 BEGIN
					SET @pm_ErrMsg = 'AccountID is required'
					RETURN @pm_ErrMsg
				 END
				 IF LTRIM(RTRIM(@pm_ApplicationID)) IS NULL OR
					LTRIM(RTRIM(@pm_ApplicationID)) = ''
				 BEGIN
					SET @pm_ErrMsg = 'ApplicationID is required' 
					RETURN @pm_ErrMsg
				 END
			  IF LTRIM(RTRIM(@pm_ApplicationTabID)) IS  NULL OR
				 LTRIM(RTRIM(@pm_ApplicationTabID)) = ''
				 BEGIN
					SET @pm_ErrMsg = 'ApplicationTabID is required'
					RETURN @pm_ErrMsg
				 END
				 --IF LTRIM(RTRIM(@pm_CycleID)) IS  NULL OR
				 --LTRIM(RTRIM(@pm_CycleID)) = ''
				 --BEGIN
					--SET @pm_ErrMsg = 'ApplicationSetting Description is required'
					--RETURN @pm_ErrMsg
				 --END
				 IF LTRIM(RTRIM(@pm_CourseKey)) IS  NULL OR
				 LTRIM(RTRIM(@pm_CourseKey)) = ''
				 BEGIN
					SET @pm_ErrMsg = 'CourseKey is required'
					RETURN @pm_ErrMsg
				 END
				 IF LTRIM(RTRIM(@pm_content_if_url)) IS  NULL OR
				 LTRIM(RTRIM(@pm_content_if_url)) = ''
				 BEGIN
					SET @pm_ErrMsg = 'ContentIfUrl is required'
					RETURN @pm_ErrMsg
				 END
				 IF LTRIM(RTRIM(@pm_MediaTypeCode)) IS  NULL OR
				 LTRIM(RTRIM(@pm_MediaTypeCode)) = ''
				 BEGIN
					SET @pm_ErrMsg = 'MediaTypeID is required'
					RETURN @pm_ErrMsg
				 END
				  IF LTRIM(RTRIM(@pm_DocumentFullName)) IS  NULL OR
				 LTRIM(RTRIM(@pm_DocumentFullName)) = ''
				 BEGIN
					SET @pm_ErrMsg = 'DocumentFullName is required'
					RETURN @pm_ErrMsg
				 END
				  IF LTRIM(RTRIM(@pm_DocumentDisplayName)) IS  NULL OR
				 LTRIM(RTRIM(@pm_DocumentDisplayName)) = ''
				 BEGIN
					SET @pm_ErrMsg = 'DocumentDiplayName is required'
					RETURN @pm_ErrMsg
				 END
				  IF LTRIM(RTRIM(@pm_document_url)) IS  NULL OR
				 LTRIM(RTRIM(@pm_document_url)) = ''
				 BEGIN
					SET @pm_ErrMsg = 'DocumentURL is required'
					RETURN @pm_ErrMsg
				 END
			      IF LTRIM(RTRIM(@pm_LastUpdatedBy)) IS NULL OR
			         LTRIM(RTRIM(@pm_LastUpdatedBy)) = ''
				 BEGIN
					SET @pm_ErrMsg = 'Last Update Name is required'
					RETURN @pm_ErrMsg
				 END
         
		 DECLARE @pm_MediaTypeID BIGINT
		 SELECT @pm_MediaTypeID = [id_media_type] FROM [dbo].[media_type] WHERE [media_type_extension] = @pm_MediaTypeCode
		 
		 --Update the table with Application setting code,value and description,LastUpdateDate,LastUpdatedBy
		 IF EXISTS ( SELECT 1 FROM [dbo].[uploaded_document] WHERE [id_uploaded_document]= @pm_DocumentID AND [id_account] = @pm_AccountID AND
		                                                           [id_application] = @pm_ApplicationID AND [id_application_tab] = @pm_ApplicationTabID AND
		                                                           [id_cycle] = @pm_CycleID AND [course_key] = @pm_CourseKey AND 
																   [id_media_type] = @pm_MediaTypeID )
                 BEGIN
				 
		 UPDATE [dbo].[uploaded_document]
		 SET [document_full_name] = @pm_DocumentFullName
		    ,[document_display_name] = @pm_DocumentDisplayName
			,[id_media_type] = @pm_MediaTypeID
			,[document_url] = @pm_document_url
			,[last_update_date] = GETDATE()
			,[last_updated_by] = @pm_LastUpdatedBy
         WHERE [id_uploaded_document]= @pm_DocumentID AND [id_account] = @pm_AccountID AND [id_application] = @pm_ApplicationID AND 
		       [id_application_tab] = @pm_ApplicationTabID AND [id_cycle] = @pm_CycleID AND [course_key] = @pm_CourseKey

			   END

          IF EXISTS ( SELECT 1 FROM [dbo].[uploaded_document] WHERE [id_uploaded_document]= @pm_DocumentID AND [id_account] = @pm_AccountID AND
		                                                           [id_application] = @pm_ApplicationID AND [id_application_tab] = @pm_ApplicationTabID AND
		                                                           [id_cycle] = @pm_CycleID AND [course_key] = @pm_CourseKey AND 
																   [id_media_type] IN ('NULL',''))
                 BEGIN

		 UPDATE [dbo].[uploaded_document]
		 SET [document_full_name] = @pm_DocumentFullName
		    ,[document_display_name] = @pm_DocumentDisplayName
			,[content_if_url] = @pm_content_if_url
			,[last_update_date] = GETDATE()
			,[last_updated_by] = @pm_LastUpdatedBy
         WHERE [id_uploaded_document]= @pm_DocumentID AND [id_account] = @pm_AccountID AND [id_application] = @pm_ApplicationID AND 
		       [id_application_tab] = @pm_ApplicationTabID AND [id_cycle] = @pm_CycleID AND [course_key] = @pm_CourseKey

		END  
	END
    



GO
/****** Object:  StoredProcedure [dbo].[Sp_GetAccounts]    Script Date: 7/10/2016 4:07:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Sp_GetAccounts]
	@pm_AccountID BIGINT ,
   @pm_Account_Code  VARCHAR(100),
   @pm_ErrMsg  VARCHAR(250) OUTPUT
   AS
   SET NOCOUNT ON
  /*-------------------------------------------------------------------------------------*
   * Procedure Name:         Sp_GetAccounts                                           *
   *                                                                                     *
   * Purpose:                This procedure will get all the accounts. This way the UI   *
   *                         will validate a user againt the account Chosen.             *
   *                                                                                     *
   * Customization ID#       None                                                        *
   *                                                                                     *
   * Parameters:             None                                                        *
   *                                                                                     *
   * Databases:              1) CIDashboards                                              *
   *                                                                                     *   
   *-------------------------------------------------------------------------------------*
   * Revision | Date Modified | Developer          | Change Summary                      *
   *-------------------------------------------------------------------------------------*
   * 1.0      | 07/05/2016    | Aravind T         | Initial Creation                    *
   *-------------------------------------------------------------------------------------*
   *          |               |                    |                                     *
   *-------------------------------------------------------------------------------------*
   *                                                                                     *
   * Medixsoft Inc. Ekhoi Inc. (c) 2015 All rights reserved                             *
   *-------------------------------------------------------------------------------------*/
   BEGIN	
   SET @pm_ErrMsg = 'SUCCESS' 
	  IF LTRIM(RTRIM(@pm_AccountID)) IS NULL OR
		 LTRIM(RTRIM(@pm_AccountID)) = ''
	     BEGIN
		    SET @pm_ErrMsg = 'Account ID is required'
			RETURN @pm_ErrMsg
	     END
	  IF LTRIM(RTRIM(@pm_Account_Code)) IS NULL OR
		 LTRIM(RTRIM(@pm_Account_Code)) = ''
	     BEGIN
		    SET @pm_ErrMsg = 'Account Code is required'
			RETURN @pm_ErrMsg
	     END
        SELECT act.id_account
			  ,act.id_account_parent
			  ,act.account_code
			  ,act.account_name
			  ,act.effective_start_date
			  ,act.effective_end_date
			  ,act.domain
			  ,act.county_number
			  ,act.county_Name
			  ,act.region_number
			  ,act.district_number
			  ,act.district_name
			  ,act.district_type
			  ,act.district_street_address
			  ,act.district_city
			  ,act.district_state
			  ,act.district_zip
			  ,act.district_phone
			  ,act.district_fax
			  ,act.district_email_address
			  ,act.district_web_site
			  ,act.district_Superintendent
			  ,act.district_lastest_enrollment
			  ,act.enabled_flag
			  ,act.Comments
			  ,act.creation_date
			  ,act.created_by
			  ,act.last_update_date
			  ,act.last_updated_by
			  ,act.id_external_reference
	      FROM Account act
			   WHERE act.id_account = @pm_AccountID AND act.enabled_flag = 1   		
			  AND 	act.account_code = CASE WHEN @pm_Account_Code = 'ALL' THEN act.account_code
                                                 ELSE @pm_Account_Code
                                              END 											  
       ORDER BY act.account_code
END   				  


GO
/****** Object:  StoredProcedure [dbo].[Sp_GetApplicationTiles]    Script Date: 7/10/2016 4:07:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

  /*-------------------------------------------------------------------------------------*
   * Procedure Name:         Sp_GetApplicationTiles										 *
   *                                                                                     *
   * Purpose:                This procedure will get all or any application tiles in	 *
   *                         	application_tile table									 *
   *                                                                                     *
   * Customization ID#       None                                                        *
   *                                                                                     *
   * Parameters:			 1) @pm_AccountID:											 *
   *						 2) @pm_Application_Tile_Code:								 *
   *						 3)	@pm_ErrMsg: Output parameter					         *
   *                                                                                     *
   * Databases:              1) CIDashboards			                                 *
   *                                                                                     *   
   *-------------------------------------------------------------------------------------*
   * Revision | Date Modified | Developer          | Change Summary                      *
   *-------------------------------------------------------------------------------------*
   * 1.0      | 07/07/2016    | Aravind T          | Initial Creation                    *
   *-------------------------------------------------------------------------------------*
   *          |               |                    |                                     *
   *-------------------------------------------------------------------------------------*
   *                                                                                     *
   * Medixsoft Inc. Ekhoi Inc. (c) 2015 All rights reserved								 *
   *-------------------------------------------------------------------------------------*/
   
   CREATE PROCEDURE [dbo].[Sp_GetApplicationTiles] 
	   @pm_AccountID BIGINT,
	   @pm_Application_Tile_Code Varchar(100),
	   @pm_ErrMsg  VARCHAR(250) OUTPUT
	     
	AS
	SET NOCOUNT ON
	BEGIN
	SET @pm_ErrMsg = 'SUCCESS' 
		IF LTRIM(RTRIM(@pm_AccountID)) IS NULL OR
			LTRIM(RTRIM(@pm_AccountID)) = ''
			BEGIN
			SET @pm_ErrMsg = 'Account ID is required'
			RETURN @pm_ErrMsg
			END
		IF LTRIM(RTRIM(@pm_Application_Tile_Code)) IS  NULL OR
			LTRIM(RTRIM(@pm_Application_Tile_Code)) = ''
			BEGIN
			SET @pm_ErrMsg = 'Application Tile Code is required'
			RETURN @pm_ErrMsg
			END
				SELECT at.id_application_tile
				  ,at.id_account
				  ,at.id_application
				  ,at.application_tile_code
				  ,at.application_tile_name
				  ,at.display_order
				  ,at.tile_location
				  ,at.enabled_flag
				  ,at.Comments
				  ,at.creation_date
				  ,at.created_by
				  ,at.last_update_date
				  ,at.last_updated_by
				  ,at.id_external_reference
			  FROM application_tile at
				LEFT JOIN [user] usr ON at.id_account = usr.id_account
				LEFT JOIN course_teacher ct ON usr.id_user = ct.id_user
				LEFT JOIN course c ON ct.id_course = c.id_course
			WHERE	at.id_account = @pm_AccountID AND at.enabled_flag = 1   		
				AND 	at.application_tile_code = CASE WHEN @pm_Application_Tile_Code = 'ALL' THEN at.application_tile_code
                                                 ELSE @pm_Application_Tile_Code
                                              END	 					
     ORDER BY at.application_tile_code		

	 END

GO
/****** Object:  StoredProcedure [dbo].[Sp_GetColumnsToEncrptDecrypt]    Script Date: 7/10/2016 4:07:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[Sp_GetColumnsToEncrptDecrypt]
   ( @pm_ID_Account        BigInt
   , @pm_TableName         VARCHAR(MAX) = NULL
   )
AS
   SET NOCOUNT ON
  /*-------------------------------------------------------------------------------------*
   * Procedure Name:         Sp_GetColumnsToEncrptDecrypt                                *
   *                                                                                     *
   * Purpose:                This procedure will get all the accounts. This way the UI   *
   *                         will validate a user againt the account Chosen.             *
   *                                                                                     *
   * Customization ID#       None                                                        *
   *                                                                                     *
   * Parameters:             1)@pm_ID_Account                                            *
   *                         2)@pm_TableName                                             *
   *                                                                                     *
   * Databases:              1)  CIDashboards                                            *
   *                                                                                     *   
   *-------------------------------------------------------------------------------------*
   * Revision | Date Modified | Developer          | Change Summary                      *
   *-------------------------------------------------------------------------------------*
   * 1.0      | 07/05/2016    | Mo. Lahlou         | Initial Creation                    *
   *-------------------------------------------------------------------------------------*
   *          |               |                    |                                     *
   *-------------------------------------------------------------------------------------*
   *                                                                                     *
   * Medixsoft Inc. Ekhoi Inc. (c) 2015 All rights reserved                              *
   *-------------------------------------------------------------------------------------*/
BEGIN
   
   DECLARE @vSqlStatment NVARCHAR(MAX) =''
    SET @vSqlStatment = 
       '
			SELECT GOS.global_option_code   AS TableName
				   , GOS.global_option_key  AS ColumnName
				FROM global_option    GOS 
					 INNER JOIN global_option_category GOC ON GOS.id_global_option_category = GOC.id_global_option_category AND 
															GOS.id_account = GOC.id_account  AND
															GOC.global_option_category_code = ''ENC''
				WHERE GOS.global_option_code = '''+'['+@pm_TableName+']'+''' 
				 AND GOC.enabled_flag = 1
				  AND GOS.enabled_flag = 1
				  AND GOS.global_option_value = ''1''
				  AND GOS.id_account = '+CONVERT(VARCHAR,@pm_ID_Account)+
		' ORDER BY GOS.display_order
       '
	  PRINT @vSqlStatment
	 -- select @vSqlStatment
	  EXECUTE sp_executesql @vSqlStatment  
	
	END


GO
/****** Object:  StoredProcedure [dbo].[Sp_GetCycles]    Script Date: 7/10/2016 4:07:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[Sp_GetCycles]
   @pm_AccountID BIGINT ,
   @pm_Cycle_Number  INT,
   @pm_ErrMsg  VARCHAR(250) OUTPUT

AS
SET NOCOUNT ON
  /*-------------------------------------------------------------------------------------*
   * Procedure Name:         GetCycles			                                         *
   *                                                                                     *
   * Purpose:                This procedure will get all Cycles information.			 *
   *                         get the Login only and only if the account is valid.        *
   *                                                                                     *
   * Customization ID#       None                                                        *
   *                                                                                     *
   * Parameters:             N/A										                 *
   *																					 *
   *                                                                                     *
   * Databases:              1) CIDashboards                                             *
   *                                                                                     *   
   *-------------------------------------------------------------------------------------*
   * Revision | Date Modified | Developer          | Change Summary                      *
   *-------------------------------------------------------------------------------------*
   * 1.0      | 07/07/2016    | Aravind T         | Initial Creation                     *
   *-------------------------------------------------------------------------------------*
   *          |               |                    |                                     *
   *-------------------------------------------------------------------------------------*
   *                                                                                     *
   * Medixsoft Inc. Ekhoi Inc. (c) 2015 All rights reserved								 *
   *-------------------------------------------------------------------------------------*/
   	 Begin
	 SET @pm_ErrMsg = 'SUCCESS' 
	  IF LTRIM(RTRIM(@pm_AccountID)) IS NULL OR
		 LTRIM(RTRIM(@pm_AccountID)) = ''
	     BEGIN
		    SET @pm_ErrMsg = 'Account ID is required'
			RETURN @pm_ErrMsg
	     END
	  IF LTRIM(RTRIM(@pm_Cycle_Number)) IS NULL OR
		 LTRIM(RTRIM(@pm_Cycle_Number)) = ''
	     BEGIN
		    SET @pm_ErrMsg = 'Cycle Number is required'
			RETURN @pm_ErrMsg
	     END
	 	  SELECT cyl.id_cycle
			  ,cyl.id_account
			  ,cyl.semester_number
			  ,cyl.cycle_number
			  ,cyl.cycle_code
			  ,cyl.cycle_name
			  ,cyl.official_start_date
			  ,cyl.official_end_date
			  ,cyl.extended_end_date
			  ,cyl.instruction_days
			  ,cyl.pk_minutes
			  ,cyl.elementary_school_minutes
			  ,cyl.blended_academy_minutes
			  ,cyl.middle_school_minutes
			  ,cyl.high_school_minutes
			  ,cyl.Comments
			  ,cyl.creation_date
			  ,cyl.created_by
			  ,cyl.last_update_date
			  ,cyl.last_updated_by
			  ,cyl.id_external_reference
	FROM cycle cyl (NOLOCK)
		INNER JOIN account act (NOLOCK) ON cyl.id_account = @pm_AccountID AND
			                            act.enabled_flag = 1 
	WHERE  cyl.cycle_number = CASE WHEN @pm_Cycle_Number = 'ALL' THEN cyl.cycle_number
                                                 ELSE @pm_Cycle_Number
                                                 END
	ORDER BY cyl.cycle_number			  	  
   END


GO
/****** Object:  StoredProcedure [dbo].[Sp_GetMediaType]    Script Date: 7/10/2016 4:07:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


  /*-------------------------------------------------------------------------------------*
   * Procedure Name:         Sp_GetMediaType 							                 *
   *                                                                                     *
   * Purpose:                This procedure will select all rows from dbo.media_type     *
   *                         	table													 *
   *                                                                                     *
   * Customization ID#       None                                                        *
   *                                                                                     *
   * Parameters:             1) @pm_media_type_extension								 *
   *						 2)	@pm_ErrMsg:	Output parameter							 *
   *                                                                                     *
   * Databases:              1) CIDashboards                                             *
   *                                                                                     *   
   *-------------------------------------------------------------------------------------*
   * Revision | Date Modified | Developer          | Change Summary                      *
   *-------------------------------------------------------------------------------------*
   * 1.0      | 07/07/2016    | Neelima R          | Initial Creation                    *
   *-------------------------------------------------------------------------------------*
   *          |               |                    |                                     *
   *-------------------------------------------------------------------------------------*
   *                                                                                     *
   * Medixsoft Inc. Ekhoi Inc. (c) 2015 All rights reserved								 *
   *-------------------------------------------------------------------------------------*/
   
   CREATE PROCEDURE [dbo].[Sp_GetMediaType] 
        @pm_AccountID BIGINT ,
   	   @pm_media_type_extension VARCHAR(30),
	   @pm_ErrMsg  VARCHAR(250) OUTPUT

	  
	AS
	SET NOCOUNT ON

	BEGIN

	 SET @pm_ErrMsg = 'SUCCESS' 
	  IF LTRIM(RTRIM(@pm_media_type_extension)) IS  NULL OR
		 LTRIM(RTRIM(@pm_media_type_extension)) = ''
	     BEGIN
		    SET @pm_ErrMsg = 'Media Type Extension is required'
			RETURN @pm_ErrMsg
	     END
		 
		SELECT DISTINCT id_media_type,media_type_extension,media_type_desc,display_order,icon_location 
		FROM [dbo].[media_type]
		WHERE UPPER(media_type_extension) = CASE WHEN UPPER(@pm_media_type_extension) = 'ALL' THEN UPPER(media_type_extension)
                                                 ELSE UPPER(@pm_media_type_extension)
                                                 END
		AND id_account = @pm_AccountID AND enabled_flag = 1
        ORDER BY 1
							   
	END  
    

GO
/****** Object:  StoredProcedure [dbo].[Sp_GetSchools]    Script Date: 7/10/2016 4:07:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Sp_GetSchools]
   @pm_AccountID BIGINT ,
   @pm_School_Number  VARCHAR(20),
   @pm_ErrMsg  VARCHAR(250) OUTPUT
AS
SET NOCOUNT ON
  /*-------------------------------------------------------------------------------------*
   * Procedure Name:         GetSchools                                                *
   *                                                                                     *
   * Purpose:                This procedure will get all school information.			 *
   *                         get the Login only and only if the account is valid.        *
   *                                                                                     *
   * Customization ID#       None                                                        *
   *                                                                                     *
   * Parameters:             N/A										                 *
   *																					 *
   *                                                                                     *
   * Databases:              1) CIDashboards                                             *
   *                                                                                     *   
   *-------------------------------------------------------------------------------------*
   * Revision | Date Modified | Developer          | Change Summary                      *
   *-------------------------------------------------------------------------------------*
   * 1.0      | 07/07/2016    | Aravind T         | Initial Creation                     *
   *-------------------------------------------------------------------------------------*
   *          |               |                    |                                     *
   *-------------------------------------------------------------------------------------*
   *                                                                                     *
   * Medixsoft Inc. Ekhoi Inc. (c) 2015 All rights reserved								 *
   *-------------------------------------------------------------------------------------*/
   	 Begin
	 SET @pm_ErrMsg = 'SUCCESS' 
	  IF LTRIM(RTRIM(@pm_AccountID)) IS NULL OR
		 LTRIM(RTRIM(@pm_AccountID)) = ''
	     BEGIN
		    SET @pm_ErrMsg = 'Account ID is required'
			RETURN @pm_ErrMsg
	     END
	  IF LTRIM(RTRIM(@pm_School_Number)) IS NULL OR
		 LTRIM(RTRIM(@pm_School_Number)) = ''
	     BEGIN
		    SET @pm_ErrMsg = 'School Number is required'
			RETURN @pm_ErrMsg
	     END
	  SELECT  scl.id_school
			  ,scl.id_account
			  ,scl.school_number
			  ,scl.school_code
			  ,scl.school_name
			  ,scl.instruction_type
			  ,scl.school_category
			  ,scl.charter_type
			  ,scl.school_street_address
			  ,scl.school_city
			  ,scl.school_state
			  ,scl.school_zip
			  ,scl.school_phone
			  ,scl.school_fax
			  ,scl.school_email_address
			  ,scl.school_web_page_address
			  ,scl.principal_last_name
			  ,scl.principal_first_name
			  ,scl.principal_middle_name
			  ,scl.principal_email
			  ,scl.Low_grade_level
			  ,scl.high_grade_level
			  ,scl.school_status
			  ,scl.school_latest_enrollment
			  ,scl.latitude
			  ,scl.longitude
			  ,scl.enabled_flag
			  ,scl.Comments
			  ,scl.creation_date
			  ,scl.created_by
			  ,scl.last_update_date
			  ,scl.last_updated_by
			  ,scl.id_external_reference
		FROM school scl (NOLOCK)
		WHERE scl.id_account = @pm_AccountID AND scl.enabled_flag = 1   		
			  AND 	scl.school_number = CASE WHEN @pm_School_Number = 'ALL' THEN scl.school_number
                                                 ELSE @pm_School_Number
                                              END	 					
     ORDER BY scl.school_number			
	  	  
   END


GO
/****** Object:  StoredProcedure [dbo].[Sp_GetUserByPerson_Select]    Script Date: 7/10/2016 4:07:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Sp_GetUserByPerson_Select]
    @pm_ID_Account bigint ,
    @pm_FirstName  VARCHAR(100),
    @pm_LastName  VARCHAR(100),
    @pm_DOB  Datetime
AS
SET NOCOUNT ON
  /*----------------------------------------------------------------------------------------*
   * Procedure Name:         Sp_GetUserByPerson_Select												*
   *																						*
   * Purpose:                This procedure will get a specific User Id attribute, It will	*
   *                         get the user only and only if the account,firstname,lastname	*
   *						 Dateof birth is valid.											*
   * Note:					Used for forgot password. not needed in this app				*
   * Customization ID#       None															*
   *																						*
   * Parameters:             1) @pm_ID_Account: Internal Account Identifier					*
   *                         2) @pm_FirstName: Internal UserFirstName Identifier			*
   *                         2) @pm_LastName: Internal UserLastName Identifier				*
   *                         2) @pm_DOB: Internal UserDateOfBirth Identifier				*
   *																						*
   * Databases:              1) CIDashboards												*
   *																						*   
   *---------------------------------------------------------------------------------------	*
   * Revision | Date Modified | Developer          | Change Summary							*
   *---------------------------------------------------------------------------------------	*
   * 1.0      | 07/05/2016    | Aravind T         | Initial Creation						*
   *---------------------------------------------------------------------------------------	*
   *          |               |                    |										*
   *---------------------------------------------------------------------------------------	*
   *																						*
   * Medixsoft Inc. Ekhoi Inc. (c) 2015 All rights reserved									*
   *---------------------------------------------------------------------------------------	*/
   BEGIN	
      DECLARE @ErrMsg	VARCHAR(500)
      SET @ErrMsg = NULL 
	  IF @pm_ID_Account IS NULL
	     BEGIN
		    SET @ErrMsg = 'Invalid Account ID'
			RETURN @ErrMsg
	     END
      IF @pm_FirstName IS NULL
	     BEGIN
		    SET @ErrMsg = 'Invalid FirstName'
			RETURN @ErrMsg
	     END
	  IF @pm_LastName IS NULL
	     BEGIN
		    SET @ErrMsg = 'Invalid LastName'
			RETURN @ErrMsg
	     END
	  IF @pm_DOB IS NULL
	     BEGIN
		    SET @ErrMsg = 'Invalid Date Of Birth'
			RETURN @ErrMsg
	     END
    SELECT usr.id_user	
			, is_active
			, is_deleted
			, Islocked_out
			, is_approved           
	     FROM [user] usr (NOLOCK)
		      INNER JOIN account act (NOLOCK) ON usr.id_account = act.id_account AND
			                            act.enabled_flag = 1 AND
										act.id_account = @pm_ID_Account
     WHERE usr.first_name = @pm_FirstName
     AND usr.last_name = @pm_LastName
     AND usr.birth_date = @pm_DOB
     
  END
  

GO
/****** Object:  StoredProcedure [dbo].[Sp_Global_Option_Category_Delete]    Script Date: 7/10/2016 4:07:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


  /*-------------------------------------------------------------------------------------*
   * Procedure Name:         Sp_Global_Option_Category_Delete						     *
   *                                                                                     *
   * Purpose:                This procedure will soft Delete a row/rows from Global 	 *
   *                         Option category table										 *
   *                                                                                     *
   * Customization ID#       None                                                        *
   *                                                                                     *
   * Parameters:             1) @pm_GlobalOptionCategoryID:								 *
   *						 2)	@pm_LastUpdatedBy:										 *
   *                         3) @pm_AccountID                                            *
   *                         4) @pm_ErrMsg output                                        *
   *                                                                                     *
   * Databases:              1) CIDashboards	                                         *
   *                                                                                     *   
   *-------------------------------------------------------------------------------------*
   * Revision | Date Modified | Developer          | Change Summary                      *
   *-------------------------------------------------------------------------------------*
   * 1.0      | 07/07/2016    | Aravind T          | Initial Creation                    *
   *-------------------------------------------------------------------------------------*
   *          |               |                    |                                     *
   *-------------------------------------------------------------------------------------*
   *                                                                                     *
   * Medixsoft Inc. Ekhoi Inc. (c) 2015 All rights reserved								 *
   *-------------------------------------------------------------------------------------*/
   
   CREATE PROCEDURE [dbo].[Sp_Global_Option_Category_Delete]
	   @pm_AccountID BIGINT ,
	   @pm_GlobalOptionCategoryID bigint,
	   @pm_LastUpdatedBy VARCHAR(100),
	   @pm_ErrMsg  VARCHAR(250) OUTPUT  
	AS
	SET NOCOUNT ON
		
	BEGIN	
		IF LTRIM(RTRIM(@pm_AccountID)) IS NULL OR
		 LTRIM(RTRIM(@pm_AccountID)) = ''
	     BEGIN
		    SET @pm_ErrMsg = 'Account ID is required'
			RETURN @pm_ErrMsg
	     END
		 IF LTRIM(RTRIM(@pm_GlobalOptionCategoryID)) IS  NULL OR
				 LTRIM(RTRIM(@pm_GlobalOptionCategoryID)) = ''
				 BEGIN
					SET @pm_ErrMsg = 'Global Option Category ID is required'
					RETURN @pm_ErrMsg
				 END
		IF LTRIM(RTRIM(@pm_LastUpdatedBy)) IS NULL OR
			     LTRIM(RTRIM(@pm_LastUpdatedBy)) = ''
				 BEGIN
					SET @pm_ErrMsg = 'Last Update Name is required'
					RETURN @pm_ErrMsg
				 END
		-- Soft Delete the record from the Global Option Category table
			UPDATE global_option_category				
				SET enabled_flag = 0,
				last_update_date = getdate(),
				last_updated_by = @pm_LastUpdatedBy
			Where id_global_option_category = @pm_GlobalOptionCategoryID
				AND id_account = @pm_AccountID
																   
	END  
	

GO
/****** Object:  StoredProcedure [dbo].[Sp_Global_Option_Category_Insert]    Script Date: 7/10/2016 4:07:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

  /*-------------------------------------------------------------------------------------*
   * Procedure Name:         Sp_Global_Option_Category_Insert							 *
   *                                                                                     *
   * Purpose:                This procedure will Insert a new row into					 *
   *                         	Global_Option_Category table							 *
   *                                                                                     *
   * Customization ID#       None                                                        *
   *                                                                                     *
   * Parameters:             1) @pm_GlobalOptionCategoryID:								 *
   *						 2) @pm_GlobalOptionCategoryCode:							 *
   *						 3)	@pm_GlobalOptionCategoryName:							 *
   *						 4)	@pm_AccountID:											 *
   *						 5)	@pm_GOC_Comments:										 *
   *						 6)	@pm_GOC_EnableFlag:										 *
   *						 7)	@pm_LastUpdatedBy:								         *
   *						 8)	@pm_ErrMsg: Output parameter					         *
   *                                                                                     *
   * Databases:              1) CIDashboards			                                 *
   *                                                                                     *   
   *-------------------------------------------------------------------------------------*
   * Revision | Date Modified | Developer          | Change Summary                      *
   *-------------------------------------------------------------------------------------*
   * 1.0      | 07/07/2016    | Aravind T          | Initial Creation                    *
   *-------------------------------------------------------------------------------------*
   *          |               |                    |                                     *
   *-------------------------------------------------------------------------------------*
   *                                                                                     *
   * Medixsoft Inc. Ekhoi Inc. (c) 2015 All rights reserved								 *
   *-------------------------------------------------------------------------------------*/
   
   CREATE PROCEDURE [dbo].[Sp_Global_Option_Category_Insert]
	   @pm_AccountID BIGINT, 
	   @pm_GlobalOptionCategoryID BIGINT,
	   @pm_GlobalOptionCategoryCode VARCHAR(100),
	   @pm_GlobalOptionCategoryName VARCHAR(250),
	   @pm_GOC_Comments VARCHAR(4000),
	   @pm_GOC_EnableFlag bit,
	   @pm_LastUpdatedBy VARCHAR(100),
	   @pm_ErrMsg  VARCHAR(250) OUTPUT
	  
	AS
	SET NOCOUNT ON
	BEGIN
	SET @pm_ErrMsg = 'SUCCESS' 
			  IF LTRIM(RTRIM(@pm_AccountID)) IS NULL OR
				 LTRIM(RTRIM(@pm_AccountID)) = ''
				 BEGIN
					SET @pm_ErrMsg = 'Account ID is required'
					RETURN @pm_ErrMsg
				 END
			  IF LTRIM(RTRIM(@pm_GlobalOptionCategoryID)) IS  NULL OR
				 LTRIM(RTRIM(@pm_GlobalOptionCategoryID)) = ''
				 BEGIN
					SET @pm_ErrMsg = 'Global Option Category ID is required'
					RETURN @pm_ErrMsg
				 END
			  IF LTRIM(RTRIM(@pm_GlobalOptionCategoryCode)) IS  NULL OR
				 LTRIM(RTRIM(@pm_GlobalOptionCategoryCode)) = ''
				 BEGIN
					SET @pm_ErrMsg = 'Global Option Category Code is required'
					RETURN @pm_ErrMsg
				 END
			  IF LTRIM(RTRIM(@pm_GlobalOptionCategoryName)) IS NULL OR
					LTRIM(RTRIM(@pm_GlobalOptionCategoryName)) = ''
				 BEGIN
					SET @pm_ErrMsg = 'Global Option Category Name is required' 
					RETURN @pm_ErrMsg
				 END
			  IF LTRIM(RTRIM(@pm_GOC_Comments)) IS  NULL OR
				 LTRIM(RTRIM(@pm_GOC_Comments)) = ''
				 BEGIN
					SET @pm_ErrMsg = 'Comments required'
					RETURN @pm_ErrMsg
				 END
			  IF LTRIM(RTRIM(@pm_GOC_EnableFlag)) IS  NULL OR
				 LTRIM(RTRIM(@pm_GOC_EnableFlag)) = ''
				 BEGIN
					SET @pm_ErrMsg = 'Enable flag is required'
					RETURN @pm_ErrMsg
				 END
			  IF LTRIM(RTRIM(@pm_LastUpdatedBy)) IS NULL OR
			     LTRIM(RTRIM(@pm_LastUpdatedBy)) = ''
				 BEGIN
					SET @pm_ErrMsg ='Name is required'
					RETURN @pm_ErrMsg
				 END
			  IF EXISTS ( SELECT 1 FROM global_option_category WHERE global_option_category_code = @pm_GlobalOptionCategoryCode
												AND	id_global_option_category != @pm_GlobalOptionCategoryID	)
				BEGIN
					SET @pm_ErrMsg = 'Global Option Category Code already exists'
					RETURN @pm_ErrMsg
				 END
			  
		 --Insert a new Global Option Category Code,Name,display order,Comments,enabled flag,LastUpdateDate,LastUpdatedBy in Global option category table
			INSERT into global_option_category				
				(	 global_option_category_code,
					 global_option_category_name,
					 id_account,
					 comments ,
					 enabled_flag,
					 creation_date,
					 created_by,
					 last_update_date,
					 last_updated_by
				)
			Values
				(	@pm_GlobalOptionCategoryCode,	
					@pm_GlobalOptionCategoryName,
					@pm_AccountID,
					@pm_GOC_Comments,
					@pm_GOC_EnableFlag,
					getdate(),
					@pm_LastUpdatedBy,
					getdate(),
					@pm_LastUpdatedBy
				)
			
			  SELECT @pm_GlobalOptionCategoryID = SCOPE_IDENTITY()								      
	END  
    

GO
/****** Object:  StoredProcedure [dbo].[Sp_Global_Option_Category_Select]    Script Date: 7/10/2016 4:07:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

  /*-------------------------------------------------------------------------------------*
   * Procedure Name:         Sp_Global_Option_Category_Select							 *
   *                                                                                     *
   * Purpose:                This procedure will get all Global Option Category			 *
   *                         Information												 *
   *                                                                                     *
   * Customization ID#       None                                                        *
   *                                                                                     *
   * Parameters:             1) @pm_GlobalOptionCategoryCode							 *
   *						 2) @pm_AccountID											 *
   *						 3) @pm_ErrMsg OUTPUT										 *
   *                                                                                     *
   * Databases:              1) CIDashboards			                                 *
   *                                                                                     *   
   *-------------------------------------------------------------------------------------*
   * Revision | Date Modified | Developer          | Change Summary                      *
   *-------------------------------------------------------------------------------------*
   * 1.0      | 07/07/2016    | Aravind T          | Initial Creation                    *
   *-------------------------------------------------------------------------------------*
   *          |               |                    |                                     *
   *-------------------------------------------------------------------------------------*
   *                                                                                     *
   * Medixsoft Inc. Ekhoi Inc. (c) 2015 All rights reserved								 *
   *-------------------------------------------------------------------------------------*/
   
   CREATE PROCEDURE [dbo].[Sp_Global_Option_Category_Select]
	    (    
			@pm_AccountID BIGINT,
			@pm_GlobalOptionCategoryCode VARCHAR(100),
		    @pm_ErrMsg  VARCHAR(250) OUTPUT
	   )
	AS
	SET NOCOUNT ON

	BEGIN
	SET @pm_ErrMsg = 'SUCCESS' 
			  IF LTRIM(RTRIM(@pm_AccountID)) IS NULL OR
				 LTRIM(RTRIM(@pm_AccountID)) = ''
				 BEGIN
					SET @pm_ErrMsg = 'Account ID is required'
					RETURN @pm_ErrMsg
				 END
			  IF LTRIM(RTRIM(@pm_GlobalOptionCategoryCode)) IS  NULL OR
				 LTRIM(RTRIM(@pm_GlobalOptionCategoryCode)) = ''
				 BEGIN
					SET @pm_ErrMsg = 'Global Option Category Code is required'
					RETURN @pm_ErrMsg
				 END
		SELECT id_global_option_category
				  ,id_global_option_category_parent
				  ,id_account
				  ,display_order
				  ,global_option_category_code
				  ,global_option_category_name
				  ,comments
				  ,enabled_flag
				  ,creation_date
				  ,created_by
				  ,last_update_date
				  ,last_updated_by
				  ,id_external_reference
			  FROM global_option_category
				WHERE global_option_category_code = CASE WHEN @pm_GlobalOptionCategoryCode = 'ALL' THEN global_option_category_code
                                                 ELSE @pm_GlobalOptionCategoryCode
                                                 END 
					AND id_account = @pm_AccountID	
			  order by display_order		   
	END  


GO
/****** Object:  StoredProcedure [dbo].[Sp_Global_Option_Category_Update]    Script Date: 7/10/2016 4:07:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

  /*-------------------------------------------------------------------------------------*
   * Procedure Name:         Sp_Global_Option_Category_Update							 *
   *                                                                                     *
   * Purpose:                This procedure will Update a row into Global_Option_Category*
   *                         	table													 *
   *                                                                                     *
   * Customization ID#       None                                                        *
   *                                                                                     *
   * Parameters:             1) @pm_GlobalOptionCategoryID:								 *
   *						 2) @pm_GlobalOptionCategoryCode:							 *
   *						 3)	@pm_GlobalOptionCategoryName:							 *
   *						 5)	@pm_Comments:											 *
   *						 6)	@pm_EnableFlag:											 *
   *						 7)	@pm_LastUpdatedBy:								         *
   *						 8)	@pm_AccountID:									         *
   *						 9)	@pm_ErrMsg: Output parameter					         *
   *                                                                                     *
   * Databases:              1) CIDashboards			                                 *
   *                                                                                     *   
   *-------------------------------------------------------------------------------------*
   * Revision | Date Modified | Developer          | Change Summary                      *
   *-------------------------------------------------------------------------------------*
   * 1.0      | 07/07/2016    | Aravind T          | Initial Creation                    *
   *-------------------------------------------------------------------------------------*
   *          |               |                    |                                     *
   *-------------------------------------------------------------------------------------*
   *                                                                                     *
   * Medixsoft Inc. Ekhoi Inc. (c) 2015 All rights reserved								 *
   *-------------------------------------------------------------------------------------*/
   
   CREATE PROCEDURE [dbo].[Sp_Global_Option_Category_Update] 
	   @pm_AccountID BIGINT,
	   @pm_GlobalOptionCategoryID BIGINT,
	   @pm_GlobalOptionCategoryCode VARCHAR(100),
	   @pm_GlobalOptionCategoryName VARCHAR(250),
	   @pm_GOC_Comments VARCHAR(4000),
	   @pm_GOC_EnableFlag bit,
	   @pm_LastUpdatedBy VARCHAR(100),	   
	   @pm_ErrMsg  VARCHAR(250) OUTPUT
	  
	AS
	SET NOCOUNT ON
	BEGIN
	SET @pm_ErrMsg = 'SUCCESS' 
			  IF LTRIM(RTRIM(@pm_AccountID)) IS NULL OR
				 LTRIM(RTRIM(@pm_AccountID)) = ''
				 BEGIN
					SET @pm_ErrMsg = 'Account ID is required'
					RETURN @pm_ErrMsg
				 END
			  IF LTRIM(RTRIM(@pm_GlobalOptionCategoryCode)) IS  NULL OR
				 LTRIM(RTRIM(@pm_GlobalOptionCategoryCode)) = ''
				 BEGIN
					SET @pm_ErrMsg = 'Global Option Category Code is required'
					RETURN @pm_ErrMsg
				 END
			  IF LTRIM(RTRIM(@pm_GlobalOptionCategoryName)) IS NULL OR
				 LTRIM(RTRIM(@pm_GlobalOptionCategoryName)) = ''
				 BEGIN
					SET @pm_ErrMsg = 'Global Option Category Name is required' 
					RETURN @pm_ErrMsg
				 END
			  IF LTRIM(RTRIM(@pm_GlobalOptionCategoryID)) IS  NULL OR
				 LTRIM(RTRIM(@pm_GlobalOptionCategoryID)) = ''
				 BEGIN
					SET @pm_ErrMsg = 'Global Option Category ID is required'
					RETURN @pm_ErrMsg
				 END
			  IF LTRIM(RTRIM(@pm_GOC_Comments)) IS  NULL OR
				 LTRIM(RTRIM(@pm_GOC_Comments)) = ''
				 BEGIN
					SET @pm_ErrMsg = 'Comments required'
					RETURN @pm_ErrMsg
				 END
			 IF LTRIM(RTRIM(@pm_GOC_EnableFlag)) IS  NULL OR
				 LTRIM(RTRIM(@pm_GOC_EnableFlag)) = ''
				 BEGIN
					SET @pm_ErrMsg = 'Enable flag is required'
					RETURN @pm_ErrMsg
				 END				 
			  IF LTRIM(RTRIM(@pm_LastUpdatedBy)) IS NULL OR
			     LTRIM(RTRIM(@pm_LastUpdatedBy)) = ''
				 BEGIN
					SET @pm_ErrMsg = 'Last Update Name is required'
					RETURN @pm_ErrMsg
				 END
			  IF EXISTS ( SELECT 1 FROM global_option_category WHERE global_option_category_code = @pm_GlobalOptionCategoryCode
												AND	id_global_option_category != @pm_GlobalOptionCategoryID	)
				BEGIN
					SET @pm_ErrMsg = 'Global Option Category Code already exists'
					RETURN @pm_ErrMsg
				 END
			
		 --Update the table with Global Option Category Code,Name,display order,Comments,enabled flag,LastUpdateDate,LastUpdatedBy
			UPDATE global_option_category				
				SET  global_option_category_code= @pm_GlobalOptionCategoryCode,
					 global_option_category_name =  @pm_GlobalOptionCategoryName,
					 comments =  @pm_GOC_Comments,
					 enabled_flag = @pm_GOC_EnableFlag,
					  last_update_date = getdate(),
					  last_updated_by = @pm_LastUpdatedBy
			  WHERE  id_global_option_category = @pm_GlobalOptionCategoryID
					AND id_account = @pm_AccountID
										      
	END  
    


GO
/****** Object:  StoredProcedure [dbo].[Sp_Global_Option_Delete]    Script Date: 7/10/2016 4:07:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


  /*-------------------------------------------------------------------------------------*
   * Procedure Name:         Sp_Global_Option_Delete						     *
   *                                                                                     *
   * Purpose:                This procedure will soft Delete a row/rows from Global 	 *
   *                         Option  table												 *
   *                                                                                     *
   * Customization ID#       None                                                        *
   *                                                                                     *
   * Parameters:             1) @pm_GlobalOptionCategoryID:								 *
   *						 2)	@pm_LastUpdatedBy:										 *
   *                         3) @pm_AccountID                                            *
   *                         4) @pm_ErrMsg output                                        *
   *                                                                                     *
   * Databases:              1) CIDashboards	                                         *
   *                                                                                     *   
   *-------------------------------------------------------------------------------------*
   * Revision | Date Modified | Developer          | Change Summary                      *
   *-------------------------------------------------------------------------------------*
   * 1.0      | 07/07/2016    | Aravind T          | Initial Creation                    *
   *-------------------------------------------------------------------------------------*
   *          |               |                    |                                     *
   *-------------------------------------------------------------------------------------*
   *                                                                                     *
   * Medixsoft Inc. Ekhoi Inc. (c) 2015 All rights reserved								 *
   *-------------------------------------------------------------------------------------*/
   
   CREATE PROCEDURE [dbo].[Sp_Global_Option_Delete]
	   @pm_AccountID BIGINT ,
	   @pm_GlobalOptionID BIGINT,
	   @pm_GlobalOptionCategoryID BIGINT,
	   @pm_LastUpdatedBy VARCHAR(100),
	   @pm_ErrMsg  VARCHAR(250) OUTPUT  
	AS
	SET NOCOUNT ON
		
	BEGIN	
		IF LTRIM(RTRIM(@pm_AccountID)) IS NULL OR
		 LTRIM(RTRIM(@pm_AccountID)) = ''
	     BEGIN
		    SET @pm_ErrMsg = 'Account ID is required'
			RETURN @pm_ErrMsg
	     END
		 IF LTRIM(RTRIM(@pm_GlobalOptionID)) IS  NULL OR
				 LTRIM(RTRIM(@pm_GlobalOptionID)) = ''
				 BEGIN
					SET @pm_ErrMsg = 'Global Option ID is required'
					RETURN @pm_ErrMsg
				 END
		 IF LTRIM(RTRIM(@pm_GlobalOptionCategoryID)) IS  NULL OR
				 LTRIM(RTRIM(@pm_GlobalOptionCategoryID)) = ''
				 BEGIN
					SET @pm_ErrMsg = 'Global Option Category Code is required'
					RETURN @pm_ErrMsg
				 END
		IF LTRIM(RTRIM(@pm_LastUpdatedBy)) IS NULL OR
			     LTRIM(RTRIM(@pm_LastUpdatedBy)) = ''
				 BEGIN
					SET @pm_ErrMsg = 'Last Update Name is required'
					RETURN @pm_ErrMsg
				 END
		-- Soft Delete the record from the Global Option table
			UPDATE global_option				
				SET enabled_flag = 0,
				last_update_date = getdate(),
				last_updated_by = @pm_LastUpdatedBy
			Where id_global_option_category = @pm_GlobalOptionCategoryID
					 AND id_global_option = @pm_GlobalOptionID
					 AND id_account = @pm_AccountID
																   
	END  
	

GO
/****** Object:  StoredProcedure [dbo].[Sp_Global_Option_Insert]    Script Date: 7/10/2016 4:07:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

  /*-------------------------------------------------------------------------------------*
   * Procedure Name:         Sp_Global_Option_Insert									 *
   *                                                                                     *
   * Purpose:                This procedure will Insert a new row into					 *
   *                         	Global_Option table										 *
   *                                                                                     *
   * Customization ID#       None                                                        *
   *                                                                                     *
   * Parameters:			 1) @pm_AccountID:											 *
   *						 2) @pm_GlobalOptionID:										 *
   *						 3)	@pm_GlobalOptionCode:									 *
   *						 2) @pm_GlobalOptionKey:									 *
   *						 3)	@pm_GlobalOptionValue:									 *
   *						 5)	@pm_GO_Comments:										 *
   *						 6)	@pm_GO_EnableFlag:										 *
   *						 7)	@pm_LastUpdatedBy:								         *
   *						 8)	@pm_ErrMsg: Output parameter					         *
   *                                                                                     *
   * Databases:              1) CIDashboards			                                 *
   *                                                                                     *   
   *-------------------------------------------------------------------------------------*
   * Revision | Date Modified | Developer          | Change Summary                      *
   *-------------------------------------------------------------------------------------*
   * 1.0      | 07/07/2016    | Aravind T          | Initial Creation                    *
   *-------------------------------------------------------------------------------------*
   *          |               |                    |                                     *
   *-------------------------------------------------------------------------------------*
   *                                                                                     *
   * Medixsoft Inc. Ekhoi Inc. (c) 2015 All rights reserved								 *
   *-------------------------------------------------------------------------------------*/
   
   CREATE PROCEDURE [dbo].[Sp_Global_Option_Insert] 
	   @pm_AccountID BIGINT,
	   @pm_GlobalOptionID BIGINT,
	   @pm_GlobalOptionCategoryID BIGINT,
	   @pm_GlobalOptionCode VARCHAR(100),
	   @pm_GlobalOptionKey VARCHAR(100),
	   @pm_GlobalOptionValue VARCHAR(250),
	   @pm_GO_DisplayOrder bigint,
	   @pm_GO_Comments VARCHAR(4000),
	   @pm_GO_EnableFlag bit,
	   @pm_LastUpdatedBy VARCHAR(100),
	   @pm_ErrMsg  VARCHAR(250) OUTPUT
	  
	AS
	SET NOCOUNT ON
	BEGIN
	SET @pm_ErrMsg = 'SUCCESS' 
			  IF LTRIM(RTRIM(@pm_AccountID)) IS NULL OR
				 LTRIM(RTRIM(@pm_AccountID)) = ''
				 BEGIN
					SET @pm_ErrMsg = 'Account ID is required'
					RETURN @pm_ErrMsg
				 END
			  IF LTRIM(RTRIM(@pm_GlobalOptionID)) IS  NULL OR
				 LTRIM(RTRIM(@pm_GlobalOptionID)) = ''
				 BEGIN
					SET @pm_ErrMsg = 'Global Option ID is required'
					RETURN @pm_ErrMsg
				 END
			  IF LTRIM(RTRIM(@pm_GlobalOptionCode)) IS  NULL OR
				 LTRIM(RTRIM(@pm_GlobalOptionCode)) = ''
				 BEGIN
					SET @pm_ErrMsg = 'Global Option Code is required'
					RETURN @pm_ErrMsg
				 END
			  IF LTRIM(RTRIM(@pm_GlobalOptionKey)) IS NULL OR
					LTRIM(RTRIM(@pm_GlobalOptionKey)) = ''
				 BEGIN
					SET @pm_ErrMsg = 'Global Option Key is required' 
					RETURN @pm_ErrMsg
				 END
			  IF LTRIM(RTRIM(@pm_GlobalOptionValue)) IS NULL OR
					LTRIM(RTRIM(@pm_GlobalOptionValue)) = ''
				 BEGIN
					SET @pm_ErrMsg = 'Global Option Value is required' 
					RETURN @pm_ErrMsg
				 END
			  IF LTRIM(RTRIM(@pm_GO_Comments)) IS  NULL OR
				 LTRIM(RTRIM(@pm_GO_Comments)) = ''
				 BEGIN
					SET @pm_ErrMsg = 'Comments required'
					RETURN @pm_ErrMsg
				 END
			  IF LTRIM(RTRIM(@pm_GO_EnableFlag)) IS  NULL OR
				 LTRIM(RTRIM(@pm_GO_EnableFlag)) = ''
				 BEGIN
					SET @pm_ErrMsg = 'Enable flag is required'
					RETURN @pm_ErrMsg
				 END
			  IF LTRIM(RTRIM(@pm_LastUpdatedBy)) IS NULL OR
			     LTRIM(RTRIM(@pm_LastUpdatedBy)) = ''
				 BEGIN
					SET @pm_ErrMsg = 'Name is required'
					RETURN @pm_ErrMsg
				 END
			  IF EXISTS ( SELECT 1 FROM global_option WHERE global_option_code = @pm_GlobalOptionCode
												AND	id_global_option != @pm_GlobalOptionID
												AND id_global_option_category = @pm_GlobalOptionCategoryID	)
				BEGIN
					SET @pm_ErrMsg = 'Global Option already exists'
					RETURN @pm_ErrMsg
				 END
		 
		 	--Insert a new Global Option  Code,Key,Value,display order,Comments,enabled flag,LastUpdateDate,LastUpdatedBy in Global option  table
				Insert into global_option
					  (    global_option_code
						  ,id_global_option_category
						  ,id_account
						  ,global_option_key
						  ,global_option_value
						  ,enabled_flag
						  ,Comments
						  ,creation_date
						  ,created_by
						  ,last_update_date
						  ,last_updated_by
					  )
				 Values
					  ( @pm_GlobalOptionCode,
					    @pm_GlobalOptionCategoryID,
					    @pm_AccountID,
						@pm_GlobalOptionKey,
						@pm_GlobalOptionValue,
						@pm_GO_EnableFlag,
						@pm_GO_Comments,				
						getdate(),
						@pm_LastUpdatedBy,
						getdate(),
						@pm_LastUpdatedBy
					  )

			  SELECT @pm_GlobalOptionID = SCOPE_IDENTITY()								      
	END  
    

GO
/****** Object:  StoredProcedure [dbo].[Sp_Global_Option_Select]    Script Date: 7/10/2016 4:07:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


  /*-------------------------------------------------------------------------------------*
   * Procedure Name:         Sp_Global_Option_Select									 *
   *                                                                                     *
   * Purpose:                This procedure will get all Global Option Category			 *
   *                         Information												 *
   *                                                                                     *
   * Customization ID#       None                                                        *
   *                                                                                     *
   * Parameters:             1) @pm_GlobalOptionCode									 *
   *						 2) @pm_AccountID											 *
   *						 3) @@pm_GlobalOptionCategoryID								 *
   *						 4) @pm_ErrMsg OUTPUT										 *
   *																					 *
   *                                                                                     *
   * Databases:              1) CIDashboards			                                 *
   *                                                                                     *   
   *-------------------------------------------------------------------------------------*
   * Revision | Date Modified | Developer          | Change Summary                      *
   *-------------------------------------------------------------------------------------*
   * 1.0      | 07/07/2016    | Aravind T          | Initial Creation                    *
   *-------------------------------------------------------------------------------------*
   *          |               |                    |                                     *
   *-------------------------------------------------------------------------------------*
   *                                                                                     *
   * Medixsoft Inc. Ekhoi Inc. (c) 2015 All rights reserved								 *
   *-------------------------------------------------------------------------------------*/
   
   CREATE PROCEDURE [dbo].[Sp_Global_Option_Select] 
	    (    @pm_GlobalOptionCode  VARCHAR(100), 
			 @pm_AccountID BIGINT,
		    @pm_GlobalOptionCategoryID BIGINT,
			@pm_ErrMsg  VARCHAR(250) OUTPUT
	    )
	AS
	SET NOCOUNT ON

	BEGIN
			  IF LTRIM(RTRIM(@pm_AccountID)) IS NULL OR
				 LTRIM(RTRIM(@pm_AccountID)) = ''
				 BEGIN
					SET @pm_ErrMsg = 'Account ID is required'
					RETURN @pm_ErrMsg
				 END
			  IF LTRIM(RTRIM(@pm_GlobalOptionCode)) IS  NULL OR
				 LTRIM(RTRIM(@pm_GlobalOptionCode)) = ''
				 BEGIN
					SET @pm_ErrMsg = 'Global Option Code is required'
					RETURN @pm_ErrMsg
				 END
			  IF LTRIM(RTRIM(@pm_GlobalOptionCategoryID)) IS  NULL OR
				 LTRIM(RTRIM(@pm_GlobalOptionCategoryID)) = ''
				 BEGIN
					SET @pm_ErrMsg = 'Global Option Category ID is required'
					RETURN @pm_ErrMsg
				 END
		 SELECT id_global_option
				  ,id_global_option_category
				  ,id_account
				  ,global_option_code
				  ,global_option_key
				  ,global_option_value
				  ,enabled_flag
				  ,display_order
				  ,Comments
				  ,creation_date
				  ,created_by
				  ,last_update_date
				  ,last_updated_by
				  ,id_external_reference
			FROM global_option
			WHERE global_option_code = CASE WHEN @pm_GlobalOptionCode = 'ALL' THEN global_option_code
                                                 ELSE @pm_GlobalOptionCode
                                                 END 
					AND id_account = @pm_AccountID	
					AND id_global_option_category = @pm_GlobalOptionCategoryID
			order by display_order	   
	END  


GO
/****** Object:  StoredProcedure [dbo].[Sp_Global_Option_Update]    Script Date: 7/10/2016 4:07:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

  /*-------------------------------------------------------------------------------------*
   * Procedure Name:         Sp_Global_Option_Update									 *
   *                                                                                     *
   * Purpose:                This procedure will Update a row into Global_Option		 *
   *                         	table													 *
   *                                                                                     *
   * Customization ID#       None                                                        *
   *                                                                                     *
   * Parameters:             1) @pm_GlobalOptionID:										 *
   *						 2) @pm_GlobalOptionCategoryID:								 *
   *						 3)	@pm_GlobalOptionCode:									 *
   *						 4) @pm_GlobalOptionKey:									 *
   *						 5)	@pm_GlobalOptionValue:									 *
   *						 6)	@pm_Comments:											 *
   *						 7)	@pm_EnableFlag:											 *
   *						 8)	@pm_LastUpdatedBy:								         *
   *						 9)	@pm_AccountID:									         *
   *						10) @pm_ErrMsg: Output parameter					         *
   *                                                                                     *
   * Databases:              1) CIDashboards			                                 *
   *                                                                                     *   
   *-------------------------------------------------------------------------------------*
   * Revision | Date Modified | Developer          | Change Summary                      *
   *-------------------------------------------------------------------------------------*
   * 1.0      | 07/07/2016    | Aravind T          | Initial Creation                    *
   *-------------------------------------------------------------------------------------*
   *          |               |                    |                                     *
   *-------------------------------------------------------------------------------------*
   *                                                                                     *
   * Medixsoft Inc. Ekhoi Inc. (c) 2015 All rights reserved								 *
   *-------------------------------------------------------------------------------------*/
   
   CREATE PROCEDURE [dbo].[Sp_Global_Option_Update] 
	   @pm_AccountID BIGINT,
	   @pm_GlobalOptionID BIGINT,
	   @pm_GlobalOptionCategoryID BIGINT,
	   @pm_GlobalOptionCode VARCHAR(100),
	   @pm_GlobalOptionKey VARCHAR(100),
	   @pm_GlobalOptionValue VARCHAR(250),
	   @pm_GO_Comments VARCHAR(4000),
	   @pm_GO_EnableFlag bit,
	   @pm_LastUpdatedBy VARCHAR(100),	   
	   @pm_ErrMsg  VARCHAR(250) OUTPUT
	  
	AS
	SET NOCOUNT ON
	BEGIN
	SET @pm_ErrMsg = 'SUCCESS' 
			  IF LTRIM(RTRIM(@pm_AccountID)) IS NULL OR
				 LTRIM(RTRIM(@pm_AccountID)) = ''
				 BEGIN
					SET @pm_ErrMsg = 'Account ID is required'
					RETURN @pm_ErrMsg
				 END
			  IF LTRIM(RTRIM(@pm_GlobalOptionID)) IS  NULL OR
				 LTRIM(RTRIM(@pm_GlobalOptionID)) = ''
				 BEGIN
					SET @pm_ErrMsg = 'Global Option ID is required'
					RETURN @pm_ErrMsg
				 END
			  IF LTRIM(RTRIM(@pm_GlobalOptionCategoryID)) IS  NULL OR
				 LTRIM(RTRIM(@pm_GlobalOptionCategoryID)) = ''
				 BEGIN
					SET @pm_ErrMsg = 'Global Option Category ID is required'
					RETURN @pm_ErrMsg
				 END
			  IF LTRIM(RTRIM(@pm_GlobalOptionCode)) IS  NULL OR
				 LTRIM(RTRIM(@pm_GlobalOptionCode)) = ''
				 BEGIN
					SET @pm_ErrMsg = 'Global Option Code is required'
					RETURN @pm_ErrMsg
				 END
			  IF LTRIM(RTRIM(@pm_GlobalOptionKey)) IS NULL OR
				 LTRIM(RTRIM(@pm_GlobalOptionKey)) = ''
				 BEGIN
					SET @pm_ErrMsg = 'Global Option Key is required' 
					RETURN @pm_ErrMsg
				 END
			  IF LTRIM(RTRIM(@pm_GlobalOptionValue)) IS NULL OR
				 LTRIM(RTRIM(@pm_GlobalOptionValue)) = ''
				 BEGIN
					SET @pm_ErrMsg = 'Global Option Valuee is required' 
					RETURN @pm_ErrMsg
				 END
			  IF LTRIM(RTRIM(@pm_GO_Comments)) IS  NULL OR
				 LTRIM(RTRIM(@pm_GO_Comments)) = ''
				 BEGIN
					SET @pm_ErrMsg = 'Comments required'
					RETURN @pm_ErrMsg
				 END
			  IF LTRIM(RTRIM(@pm_GO_EnableFlag)) IS  NULL OR
				 LTRIM(RTRIM(@pm_GO_EnableFlag)) = ''
				 BEGIN
					SET @pm_ErrMsg = 'Enable flag is required'
					RETURN @pm_ErrMsg
				 END				 
			  IF LTRIM(RTRIM(@pm_LastUpdatedBy)) IS NULL OR
			     LTRIM(RTRIM(@pm_LastUpdatedBy)) = ''
				 BEGIN
					SET @pm_ErrMsg = 'Last Update Name is required'
					RETURN @pm_ErrMsg
				 END
			  IF EXISTS ( SELECT 1 FROM global_option WHERE global_option_code = @pm_GlobalOptionCode
												AND	id_global_option != @pm_GlobalOptionID
												AND id_global_option_category = @pm_GlobalOptionCategoryID	)
				BEGIN
					SET @pm_ErrMsg = 'Global Option Category already exists'
					RETURN @pm_ErrMsg
				 END

		--Update the table with Global Option Code,Key,Value,display order,Comments,enabled flag,LastUpdateDate,LastUpdatedBy
			UPDATE global_option				
				SET  global_option_code= @pm_GlobalOptionCode,
					 global_option_key =  @pm_GlobalOptionKey,
					 global_option_value =  @pm_GlobalOptionValue,
					 comments =  @pm_GO_Comments,
					 enabled_flag = @pm_GO_EnableFlag,
					  last_update_date = getdate(),
					  last_updated_by = @pm_LastUpdatedBy
			  Where id_global_option = @pm_GlobalOptionID	 
			  AND id_global_option_category = @pm_GlobalOptionCategoryID
			  AND id_account = @pm_AccountID
		 										      
	END  
    


GO
/****** Object:  StoredProcedure [dbo].[Sp_Honorific_Select]    Script Date: 7/10/2016 4:07:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Sp_Honorific_Select]
   @pm_Honorific_Code  VARCHAR(100),
   @pm_ErrMsg  VARCHAR(250) OUTPUT
AS
SET NOCOUNT ON
  /*-------------------------------------------------------------------------------------*
   * Procedure Name:         Sp_Honorific_Select                                         *
   *                                                                                     *
   * Purpose:                This procedure will get all Honorific titles                *
   *                                                                                     *
   * Customization ID#       None                                                        *
   *                                                                                     *
   * Parameters:             None                                                        *
   *                                                                                     *
   * Databases:              1) CIDashboards                                              *
   *                                                                                     *   
   *-------------------------------------------------------------------------------------*
   * Revision | Date Modified | Developer          | Change Summary                      *
   *-------------------------------------------------------------------------------------*
   * 1.0      | 07/05/2016    | Mo. Lahlou         | Initial Creation                    *
   *-------------------------------------------------------------------------------------*
   *          |               |                    |                                     *
   *-------------------------------------------------------------------------------------*
   *                                                                                     *
   * Medixsoft Inc. Ekohi Inc. (c) 2015 All rights reserved                              *
   *-------------------------------------------------------------------------------------*/
   BEGIN 
   SET @pm_ErrMsg = 'SUCCESS' 
	   IF LTRIM(RTRIM(@pm_Honorific_Code)) IS NULL OR
		 LTRIM(RTRIM(@pm_Honorific_Code)) = ''
	     BEGIN
		    SET @pm_ErrMsg = 'Honorific Code is required'
			RETURN @pm_ErrMsg
	     END                 	   
      SELECT  hon.id_honorific
      ,hon.honorific_code
      ,hon.honorfic_name
      ,hon.enabled_flag
      ,hon.creation_date
      ,hon.created_by
      ,hon.last_update_date
      ,hon.last_updated_by
      ,hon.id_external_reference
	     FROM honorific hon
        WHERE  hon.honorific_code = CASE WHEN @pm_Honorific_Code = 'ALL' THEN hon.honorific_code
                                                 ELSE @pm_Honorific_Code
                                              END
	AND   hon.enabled_flag = 1
     ORDER BY hon.honorific_code desc 
   END


GO
/****** Object:  StoredProcedure [dbo].[Sp_Login_Select]    Script Date: 7/10/2016 4:07:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Sp_Login_Select]
   @pm_AccountID BIGINT ,
   @pm_LoginID  Varchar(100),
   @pm_UserID BIGINT,
   @pm_ErrMsg  VARCHAR(250) OUTPUT
AS
SET NOCOUNT ON
  /*-------------------------------------------------------------------------------------*
   * Procedure Name:         Sp_Login_Select                                             *
   *                                                                                     *
   * Purpose:                This procedure will get a specific Login attributes, It will*
   *                         get the Login only and only if the account is valid.        *
   *                                                                                     *
   * Customization ID#       None                                                        *
   *                                                                                     *
   * Parameters:             1) @pm_ID_Account: Internal Account Identifier              *
   *                         2) @pm_Login_ID: Internal User Identifier                   *
   *                                                                                     *
   * Databases:              1) CIDashboards                                             *
   *                                                                                     *   
   *-------------------------------------------------------------------------------------*
   * Revision | Date Modified | Developer          | Change Summary                      *
   *-------------------------------------------------------------------------------------*
   * 1.0      | 07/05/2016    | Aravind T         | Initial Creation                     *
   *-------------------------------------------------------------------------------------*
   *          |               |                    |                                     *
   *-------------------------------------------------------------------------------------*
   *                                                                                     *
   * Medixsoft Inc. Ekhoi Inc. (c) 2015 All rights reserved								 *
   *-------------------------------------------------------------------------------------*/
   BEGIN
	SET @pm_ErrMsg = 'SUCCESS'	
	IF LTRIM(RTRIM(@pm_AccountID)) IS NULL OR
		 LTRIM(RTRIM(@pm_AccountID)) = ''
	     BEGIN
		    SET @pm_ErrMsg = 'Account ID is required'
			RETURN @pm_ErrMsg
	     END
     IF LTRIM(RTRIM(@pm_LoginID)) IS NULL OR
		 LTRIM(RTRIM(@pm_LoginID)) = '' 
	     BEGIN
		    SET @pm_ErrMsg = 'Login ID is required'
			RETURN @pm_ErrMsg
	     END
	  IF  LTRIM(RTRIM(@pm_UserID)) IS NULL OR
		 LTRIM(RTRIM(@pm_UserID)) = '' 
	     BEGIN
		    SET @pm_ErrMsg = 'User ID is required'
			RETURN @pm_ErrMsg
	     END
       SELECT usr.id_user	
			, is_active
			, is_deleted
			, Islocked_out
			, is_approved       
	     FROM [user] usr (NOLOCK)
		      INNER JOIN account act (NOLOCK) ON usr.id_account = act.id_account AND
			                            act.enabled_flag = 1 AND
										act.id_account = @pm_AccountID              									  
       WHERE usr.Login_id = @pm_LoginID AND usr.id_user = 	@pm_UserID		
     ORDER BY usr.Login_id
   END
  


GO
/****** Object:  StoredProcedure [dbo].[Sp_MobileCarrier_Select]    Script Date: 7/10/2016 4:07:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE  [dbo].[Sp_MobileCarrier_Select]
   @pm_Mobile_Carrier_Code  VARCHAR(100),
   @pm_ErrMsg  VARCHAR(250) OUTPUT

AS
SET NOCOUNT ON
  /*-------------------------------------------------------------------------------------*
   * Procedure Name:         Sp_MobileCarrier_Select                                     *
   *                                                                                     *
   * Purpose:                This procedure will get all Mobile Carriers                 *
   *                                                                                     *
   * Customization ID#       None                                                        *
   *                                                                                     *
   * Parameters:             None                                                        *
   *                                                                                     *
   * Databases:              1) CIDashboards                                              *
   *                                                                                     *   
   *-------------------------------------------------------------------------------------*
   * Revision | Date Modified | Developer          | Change Summary                      *
   *-------------------------------------------------------------------------------------*
   * 1.0      | 07/05/2016    | Mo. Lahlou         | Initial Creation                    *
   *-------------------------------------------------------------------------------------*
   *          |               |                    |                                     *
   *-------------------------------------------------------------------------------------*
   *                                                                                     *
   * Medixsoft Inc. Ekhoi Inc. (c) 2015 All rights reserved                              *
   *-------------------------------------------------------------------------------------*/
   BEGIN 
   SET @pm_ErrMsg = 'SUCCESS' 
	   IF LTRIM(RTRIM(@pm_Mobile_Carrier_Code)) IS NULL OR
		 LTRIM(RTRIM(@pm_Mobile_Carrier_Code)) = ''
	     BEGIN
		    SET @pm_ErrMsg = 'Mobile Carrier Code is required'
			RETURN @pm_ErrMsg
	     END
       SELECT mcr.id_mobile_carrier
			  ,mcr.mobile_carrier_code
			  ,mcr.mobile_carrier_name
			  ,mcr.gateway
			  ,mcr.enabled_flag
			  ,mcr.creation_date
			  ,mcr.created_by
			  ,mcr.last_update_date
			  ,mcr.last_updated_by
			  ,mcr.id_external_reference
	     FROM mobile_carrier mcr
        WHERE  mcr.mobile_carrier_code = CASE WHEN @pm_Mobile_Carrier_Code = 'ALL' THEN mcr.mobile_carrier_code
                                                 ELSE @pm_Mobile_Carrier_Code
                                              END
	AND   mcr.enabled_flag = 1
     ORDER BY mcr.mobile_carrier_code 
   END


GO
/****** Object:  StoredProcedure [dbo].[Sp_School_Select]    Script Date: 7/10/2016 4:07:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE  [dbo].[Sp_School_Select]
   @pm_AccountID BIGINT ,
   @pm_School_ID  Varchar(100),
   @pm_ErrMsg  VARCHAR(250) OUTPUT
AS
SET NOCOUNT ON
  /*-------------------------------------------------------------------------------------*
   * Procedure Name:         Sp_School_Select                                            *
   *                                                                                     *
   * Purpose:                This procedure will get a specific School , It will		 *
   *                         get the particular school only and only if the account      *
   *                         is valid                                                    *
   * Customization ID#       None                                                        *
   *                                                                                     *
   * Parameters:             1) @pm_ID_Account: Internal Account Identifier              *
   *                         2) @pm_School_ID: Internal User Identifier                  *
   *                                                                                     *
   * Databases:              1) CIDashboards                                             *
   *                                                                                     *   
   *-------------------------------------------------------------------------------------*
   * Revision | Date Modified | Developer          | Change Summary                      *
   *-------------------------------------------------------------------------------------*
   * 1.0      | 07/07/2016    | Aravind T         | Initial Creation                     *
   *-------------------------------------------------------------------------------------*
   *          |               |                    |                                     *
   *-------------------------------------------------------------------------------------*
   *                                                                                     *
   * Medixsoft Inc. Ekhoi Inc. (c) 2015 All rights reserved								 *
   *-------------------------------------------------------------------------------------*/
   	 BEGIN	
      SET @pm_ErrMsg = 'SUCCESS'
	  IF LTRIM(RTRIM(@pm_AccountID)) IS NULL OR
		 LTRIM(RTRIM(@pm_AccountID)) = ''
	     BEGIN
		    SET @pm_ErrMsg = 'Account ID is required'
			RETURN @pm_ErrMsg
	     END
      IF  LTRIM(RTRIM(@pm_School_ID)) IS NULL OR
		 LTRIM(RTRIM(@pm_School_ID)) = ''
	     BEGIN
		    SET @pm_ErrMsg = 'School ID is required'
			RETURN @pm_ErrMsg
	     END 
		 SELECT  scl.id_school
			  ,scl.id_account
			  ,scl.school_number
			  ,scl.school_code
			  ,scl.school_name
			  ,scl.instruction_type
			  ,scl.school_category
			  ,scl.charter_type
			  ,scl.school_street_address
			  ,scl.school_city
			  ,scl.school_state
			  ,scl.school_zip
			  ,scl.school_phone
			  ,scl.school_fax
			  ,scl.school_email_address
			  ,scl.school_web_page_address
			  ,scl.principal_last_name
			  ,scl.principal_first_name
			  ,scl.principal_middle_name
			  ,scl.principal_email
			  ,scl.Low_grade_level
			  ,scl.high_grade_level
			  ,scl.school_status
			  ,scl.school_latest_enrollment
			  ,scl.latitude
			  ,scl.longitude
			  ,scl.enabled_flag
			  ,scl.Comments
			  ,scl.creation_date
			  ,scl.created_by
			  ,scl.last_update_date
			  ,scl.last_updated_by
			  ,scl.id_external_reference
		FROM school scl (NOLOCK)
		INNER JOIN account act (NOLOCK) ON scl.id_account = act.id_account AND
			                            act.enabled_flag = 1        AND
										act.ID_Account = @pm_AccountID		
		WHERE scl.id_school = @pm_School_ID 					
     ORDER BY scl.id_school			
	  	  
   END
   

GO
/****** Object:  StoredProcedure [dbo].[Sp_StateSession_Insert]    Script Date: 7/10/2016 4:07:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Sp_StateSession_Insert]
   @pm_AccountID BIGINT ,
   @pm_SessionGUID Varchar(100),
   @pm_ErrMsg  VARCHAR(250) OUTPUT
      
AS
SET NOCOUNT ON
  /*-------------------------------------------------------------------------------------*
   * Procedure Name:         Sp_StateSession_Insert                                       *
   *                                                                                     *
   * Purpose:                This procedure will Insert a new row SessionID	if there	 *
   *                         are no SessionGUID	and SessionStartTime					 *
   *                                                                                     *
   * Customization ID#       None                                                        *
   *                                                                                     *
   * Parameters:             1) @pm_SessionGUID: Internal Session GUID Identifier        *
   *                                                                                     *
   * Databases:              1) CIDashboards                                             *
   *                                                                                     *   
   *-------------------------------------------------------------------------------------*
   * Revision | Date Modified | Developer          | Change Summary                      *
   *-------------------------------------------------------------------------------------*
   * 1.0      | 07/05/2016    | Aravind T          | Initial Creation                    *
   *-------------------------------------------------------------------------------------*
   *          |               |                    |                                     *
   *-------------------------------------------------------------------------------------*
   *                                                                                     *
   * Medixsoft Inc. Ekhoi Inc. (c) 2015 All rights reserved								 *
   *-------------------------------------------------------------------------------------*/
   BEGIN
   SET @pm_ErrMsg = 'SUCCESS' 
	  IF LTRIM(RTRIM(@pm_AccountID)) IS NULL OR
		 LTRIM(RTRIM(@pm_AccountID)) = ''
	     BEGIN
		    SET @pm_ErrMsg = 'Account ID is required'
			RETURN @pm_ErrMsg
	     END	
      IF @pm_SessionGUID IS NOT NULL
	     BEGIN
		    INSERT INTO state_session(session_guid,id_account) OUTPUT INSERTED.id_session VALUES(@pm_SessionGUID,@pm_AccountID)
	     END
   END


GO
/****** Object:  StoredProcedure [dbo].[Sp_StateSession_Select]    Script Date: 7/10/2016 4:07:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Sp_StateSession_Select]
  @pm_AccountID BIGINT ,
  @pm_ID_Session bigint,
  @pm_ErrMsg  VARCHAR(250) OUTPUT
AS
SET NOCOUNT ON
  /*-------------------------------------------------------------------------------------*
   * Procedure Name:         Sp_StateSession_Select                                      *
   *                                                                                     *
   * Purpose:                This procedure will get a specific StateSession  attributes,*
   *                         It will get the Session information only and only if the	 *
   *						 account and user is valid.									 *
   *                                                                                     *
   * Customization ID#       None                                                        *
   *                                                                                     *
   * Parameters:             1) @pm_ID_Account: Internal Account Identifier              *
   *                         2) @pm_ID_User: Internal User Identifier                    *
   *                                                                                     *
   * Databases:              1) CIDashboards                                              *
   *                                                                                     *   
   *-------------------------------------------------------------------------------------*
   * Revision | Date Modified | Developer          | Change Summary                      *
   *-------------------------------------------------------------------------------------*
   * 1.0      | 07/05/2016    | Aravind T          | Initial Creation                    *
   *-------------------------------------------------------------------------------------*
   *          |               |                    |                                     *
   *-------------------------------------------------------------------------------------*
   *                                                                                     *
   * Medixsoft Inc. Ekhoi Inc. (c) 2015 All rights reserved                             *
   *-------------------------------------------------------------------------------------*/
   BEGIN	
       SET @pm_ErrMsg = 'SUCCESS' 
	  IF LTRIM(RTRIM(@pm_AccountID)) IS NULL OR
		 LTRIM(RTRIM(@pm_AccountID)) = ''
	     BEGIN
		    SET @pm_ErrMsg = 'Account ID is required'
			RETURN @pm_ErrMsg
	     END
	   SELECT ss.id_session
			  ,ss.session_guid
			  ,ss.session_start_time
			  ,ss.expiration_date
			  ,ss.session_end_time
			  ,ss.lock_date
			  ,ss.lock_date_local
			  ,ss.lock_cookie
			  ,ss.[time_out]
			  ,ss.locked
			  ,ss.session_data_short
			  ,ss.session_data_image
			  ,ss.flags
			  ,ss.session_data_XML
			  ,ss.host_ip
			  ,ss.cookie_data_encryption_salt
			  ,ss.id_account
			  ,ss.id_user
			  ,ss.creation_date
		FROM state_session ss 
		INNER JOIN account act (NOLOCK) ON act.id_account = @pm_AccountID AND
			                            act.enabled_flag = 1
			where id_session =  @pm_ID_Session      
   END


GO
/****** Object:  StoredProcedure [dbo].[Sp_StateSession_Timeout_Select]    Script Date: 7/10/2016 4:07:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Sp_StateSession_Timeout_Select]
		@pm_AccountID BIGINT ,
		 @pm_ID_Session bigint,
		 @pm_ErrMsg  VARCHAR(250) OUTPUT
		AS
	/*-------------------------------------------------------------------------------------*
   * Procedure Name:         Sp_StateSession_Timeout_Select                                *
   *                                                                                       *
   * Purpose:                This procedure will Update a particular session if			   *
   *						 												               *
   *																		               *
   *																		               *
   * Customization ID#       None														   *
   *                                                                                       *
   * Parameters:             1) @pm_ID_Session: Internal Session ID                        *
   *                                                                                       *
   * Databases:              1)  CIDashboards                                              *
   *                                                                                       *   
   *-------------------------------------------------------------------------------------  *
   * Revision | Date Modified | Developer          | Change Summary                        *
   *-------------------------------------------------------------------------------------  *
   * 1.0      | 07/05/2016    | Aravind T          | Initial Creation                      *
   *-------------------------------------------------------------------------------------  *
   *          |               |                    |                                       *
   *-------------------------------------------------------------------------------------  *
   *                                                                                       *
   * Medixsoft Inc. Ekhoi Inc. (c) 2015 All rights reserved                                *
   *-------------------------------------------------------------------------------------  */
   
	BEGIN
	SET @pm_ErrMsg = 'SUCCESS' 
	  IF LTRIM(RTRIM(@pm_AccountID)) IS NULL OR
		 LTRIM(RTRIM(@pm_AccountID)) = ''
	     BEGIN
		    SET @pm_ErrMsg = 'Account ID is required'
			RETURN @pm_ErrMsg
	     END
	SELECT 
		[time_out] FROM state_session ss
		INNER JOIN account act (NOLOCK) ON act.id_account = @pm_AccountID AND
			                            act.enabled_flag = 1
		WHERE id_session = @pm_ID_Session   
	END


GO
/****** Object:  StoredProcedure [dbo].[Sp_StateSession_Update]    Script Date: 7/10/2016 4:07:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

	/*-------------------------------------------------------------------------------------*
   * Procedure Name:         Sp_StateSession_Update                                        *
   *                                                                                       *
   * Purpose:                This procedure will Update a particular Application session.  *
   *                         It will update all the values based on application Session ID *
   *                         and Session ID is tied to ApplicationSessionID                *
   * Customization ID#       None														   *
   *                                                                                       *
   * Parameters:             None                                                          *
   *                                                                                       *
   * Databases:              1) CIDashboards                                               *
   *                                                                                       *   
   *-------------------------------------------------------------------------------------  *
   * Revision | Date Modified | Developer          | Change Summary                        *
   *-------------------------------------------------------------------------------------  *
   * 1.0      | 07/05/2016    | Aravind T          | Initial Creation                      *
   *-------------------------------------------------------------------------------------  *
   *          |               |                    |                                       *
   *-------------------------------------------------------------------------------------  *
   *                                                                                       *
   * Medixsoft Inc. Ekhoi Inc. (c) 2015 All rights reserved                                *
   *-------------------------------------------------------------------------------------  */

	CREATE PROCEDURE [dbo].[Sp_StateSession_Update]
		 @pm_ID_Session bigint
		,@pm_ID_Account bigint
		,@pm_ID_User bigint
		,@pm_SessionGUID nvarchar(250)
		,@pm_SessionStartTime datetime
		,@pm_ExpirationDate datetime
		,@pm_SessionEndTime datetime
		,@pm_LockDate datetime
		,@pm_LockDateLocal datetime
		,@pm_LockCookie bit
		,@pm_Timeout bit
		,@pm_Locked bit
		,@pm_SessionDataShort varbinary(max)
		,@pm_SessionDataImage image
		,@pm_Flags	bigint
		,@pm_SessionDataXML xml
		,@pm_HostIP	varchar(50)
		,@pm_CookieDataEncryptionSalt nvarchar(200)
		,@pm_ErrMsg  VARCHAR(250) OUTPUT
		
		AS
			BEGIN
			SET @pm_ErrMsg = 'SUCCESS' 
			IF LTRIM(RTRIM(@pm_ID_Account)) IS NULL OR
			   LTRIM(RTRIM(@pm_ID_Account)) = ''
			BEGIN
				SET @pm_ErrMsg = 'Account ID is required'
				RETURN @pm_ErrMsg
			END
				IF @pm_ID_Session IS NOT NULL
					UPDATE state_session
						SET		 id_user				= @pm_ID_User
								,id_account				= @pm_ID_Account
								,session_start_time		= @pm_SessionStartTime
								,expiration_date		= @pm_ExpirationDate
								,session_end_time		= @pm_SessionEndTime
								,lock_date				= @pm_LockDate
								,lock_date_local		= @pm_LockDateLocal
								,lock_cookie			= @pm_LockCookie
								,[time_out]				= @pm_Timeout
								,locked					= @pm_Locked
								,session_data_short		= @pm_SessionDataShort
								,session_data_image		= @pm_SessionDataImage
								,flags					= @pm_Flags
								,session_data_XML		= @pm_SessionDataXML  
								,host_ip				= @pm_HostIP
								,cookie_data_encryption_salt = @pm_CookieDataEncryptionSalt
								
					FROM state_session 
						 Where id_session = @pm_ID_Session
						 AND session_guid = @pm_SessionGUID
		END


GO
/****** Object:  StoredProcedure [dbo].[Sp_StateSessionApplication_Insert]    Script Date: 7/10/2016 4:07:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Sp_StateSessionApplication_Insert]
   @pm_AccountID BIGINT ,
   @pm_SessionApplicationStartTime Varchar(100),
   @pm_ID_Session BIGINT,
   @pm_ID_Session_Application BIGINT,
   @pm_ErrMsg  VARCHAR(250) OUTPUT
AS
SET NOCOUNT ON
  /*-------------------------------------------------------------------------------------*
   * Procedure Name:         Sp_StateSessionApplication_Insert                           *
   *                                                                                     *
   * Purpose:                This procedure will Insert a new row ApplicationSessionID	 *
   *                         if ApplcationSession Starts								 *
   *                                                                                     *
   * Customization ID#       None                                                        *
   *                                                                                     *
   * Parameters:             1) @pm_SessionGUID: Internal Session GUID Identifier        *
   *                                                                                     *
   * Databases:              1) CIDashboards                                             *
   *                                                                                     *   
   *-------------------------------------------------------------------------------------*
   * Revision | Date Modified | Developer          | Change Summary                      *
   *-------------------------------------------------------------------------------------*
   * 1.0      | 07/05/2016    | Aravind T          | Initial Creation                    *
   *-------------------------------------------------------------------------------------*
   *          |               |                    |                                     *
   *-------------------------------------------------------------------------------------*
   *                                                                                     *
   * Medixsoft Inc. Ekhoi Inc. (c) 2015 All rights reserved								 *
   *-------------------------------------------------------------------------------------*/
   BEGIN	
	SET @pm_ErrMsg = 'SUCCESS' 
	  IF LTRIM(RTRIM(@pm_AccountID)) IS NULL OR
		 LTRIM(RTRIM(@pm_AccountID)) = ''
	     BEGIN
		    SET @pm_ErrMsg = 'Account ID is required'
			RETURN @pm_ErrMsg
	     END
		 IF LTRIM(RTRIM(@pm_SessionApplicationStartTime)) IS NULL OR
		 LTRIM(RTRIM(@pm_SessionApplicationStartTime)) = ''
	     BEGIN
		    SET @pm_ErrMsg = 'Session Application Start Time is required'
			RETURN @pm_ErrMsg
	     END
		 IF LTRIM(RTRIM(@pm_ID_Session)) IS NULL OR
		 LTRIM(RTRIM(@pm_ID_Session)) = ''
	     BEGIN
		    SET @pm_ErrMsg = 'Session ID is required'
			RETURN @pm_ErrMsg
	     END
		 IF LTRIM(RTRIM(@pm_ID_Session_Application)) IS NULL OR
		 LTRIM(RTRIM(@pm_ID_Session_Application)) = ''
	     BEGIN
		    SET @pm_ErrMsg = 'Session Application ID is required'
			RETURN @pm_ErrMsg
	     END
      IF @pm_SessionApplicationStartTime IS NOT NULL
	     BEGIN
		    INSERT INTO state_session_application(id_account,session_application_start_time,id_session_application,id_session) OUTPUT INSERTED.id_session_application
				VALUES(@pm_AccountID,@pm_SessionApplicationStartTime,@pm_ID_Session_Application,@pm_ID_Session)
	     END
   END


GO
/****** Object:  StoredProcedure [dbo].[Sp_StateSessionApplication_Select]    Script Date: 7/10/2016 4:07:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Sp_StateSessionApplication_Select]
   @pm_ID_Account bigint, 
   @pm_ID_Session bigint,
   @pm_ID_User bigint,
   @pm_ID_SessionApplication bigint,
   @pm_ErrMsg  VARCHAR(250) OUTPUT
AS
SET NOCOUNT ON
  /*-------------------------------------------------------------------------------------*
   * Procedure Name:         Sp_StateSessionApplication_Select                           *
   *                                                                                     *
   * Purpose:                This procedure will get a specific StateSessionApplication  *
   *                         attributes,It will get the Session information only and	 *
   *						  only if the account is valid.								 *
   *                                                                                     *
   * Customization ID#       None                                                        *
   *                                                                                     *
   * Parameters:             1) @pm_ID_Account: Internal Account Identifier              *
   *                         2) @pm_ID_Session: Internal Account Identifier              *
   *                         3) @pm_ID_User: Internal Account Identifier                 *
   *                         4) @pm_ID_SessionApplication: Internal Account Identifier   *
   *                                                                                     *
   *                                                                                     *
   * Databases:              1) CIDashboards                                             *
   *                                                                                     *   
   *-------------------------------------------------------------------------------------*
   * Revision | Date Modified | Developer          | Change Summary                      *
   *-------------------------------------------------------------------------------------*
   * 1.0      | 07/05/2016    | Aravind T          | Initial Creation                    *
   *-------------------------------------------------------------------------------------*
   *          |               |                    |                                     *
   *-------------------------------------------------------------------------------------*
   *                                                                                     *
   * Medixsoft Inc. Ekhoi Inc. (c) 2015 All rights reserved                             *
   *-------------------------------------------------------------------------------------*/
   BEGIN	
      DECLARE @WhereClause NVARCHAR(MAX) = ' WHERE '
      DECLARE @SqlStatment NVARCHAR(MAX) = ''
      DECLARE @OrderBy     NVARCHAR(MAX) = 'ssa.session_application_start_time'
      DECLARE @Direction   NVARCHAR(MAX) = 'DESC'
     
      SET @pm_ErrMsg = 'SUCCESS' 
	  IF LTRIM(RTRIM(@pm_ID_SessionApplication)) IS NULL OR
		 LTRIM(RTRIM(@pm_ID_SessionApplication)) = ''
	     BEGIN
		    SET @pm_ErrMsg = 'ApplicationSession ID is required'
			RETURN @pm_ErrMsg
	     END
	  IF LTRIM(RTRIM(@pm_ID_Account)) IS NULL OR
		 LTRIM(RTRIM(@pm_ID_Account)) = ''
	     BEGIN
		    SET @pm_ErrMsg = 'Account ID is required'
			RETURN @pm_ErrMsg
	     END
	  IF LTRIM(RTRIM(@pm_ID_User)) IS NULL OR
		 LTRIM(RTRIM(@pm_ID_User)) = ''
	     BEGIN
		    SET @pm_ErrMsg = 'User ID is required'
			RETURN @pm_ErrMsg
	     END
      SET @WhereClause = @WhereClause + ' ssa.id_session_application = ' + CONVERT(VARCHAR,@pm_ID_SessionApplication) + ' AND '
	  -- Account
	     IF @pm_ID_User IS NOT NULL
	     BEGIN
	        SET @WhereClause = @WhereClause + ' ssa.id_account = ' + CONVERT(VARCHAR,@pm_ID_Account) + ' AND '
		END
      -- StateSession
	   IF @pm_ID_Session IS NOT NULL
	     BEGIN
            SET @WhereClause = @WhereClause + ' ssa.id_session = ' + CONVERT(VARCHAR,@pm_ID_Session) + ' AND '        
		 END
      -- Finalizing the Where Clause
      IF @WhereClause = ' WHERE '
         BEGIN 
            SET @WhereClause =' '
         END
      ELSE
         BEGIN
            SET @WhereClause = SUBSTRING(@WhereClause,1,LEN(@WhereClause)-4)
         END 
      SET @SqlStatment = 
	  'SELECT ssa.id_session_application
			  ,ssa.session_application_start_time
			  ,ssa.expiration_date
			  ,ssa.session_end_time
			  ,ssa.id_application
			  ,ssa.session_data_xml
			  ,ssa.id_account
			  ,ssa.id_session			 
		FROM state_session_application ssa
		     INNER JOIN account act ON ssa.id_account = act.id_account AND
			                            act.enabledflag = 1          
			LEFT JOIN state_session ss ON ss.id_session = ssa.id_session
			LEFT JOIN [user] usr ON usr.id_user = ss.id_user 					
		'
	  + @WhereClause 
	  + '  ORDER BY '+@OrderBy+' '+@Direction

      SELECT @SqlStatment
      EXECUTE sp_executesql @SqlStatment

   END


GO
/****** Object:  StoredProcedure [dbo].[Sp_User_Select]    Script Date: 7/10/2016 4:07:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Sp_User_Select]
   @pm_AccountID bigint ,
   @pm_UserID    bigint,
   @pm_ErrMsg  VARCHAR(250) OUTPUT
AS
SET NOCOUNT ON
  /*-------------------------------------------------------------------------------------*
   * Procedure Name:         Sp_User_Select                                              *
   *                                                                                     *
   * Purpose:                This procedure will get a specific User attributes, It will *
   *                         get the user only and only if the account is valid.         *
   *                                                                                     *
   * Customization ID#       None                                                        *
   *                                                                                     *
   * Parameters:             1) @pm_ID_Account: Internal Account Identifier               *
   *                         2) @pm_ID_User: Internal User Identifier                     *
   *                                                                                     *
   * Databases:              1) CIDashboards                                              *
   *                                                                                     *   
   *-------------------------------------------------------------------------------------*
   * Revision | Date Modified | Developer          | Change Summary                      *
   *-------------------------------------------------------------------------------------*
   * 1.0      | 07/05/2016    | Aravind T          | Initial Creation                    *
   *-------------------------------------------------------------------------------------*
   *          |               |                    |                                     *
   *-------------------------------------------------------------------------------------*
   *                                                                                     *
   * Medixsoft Inc. Ekhoi Inc. (c) 2015 All rights reserved                             *
   *-------------------------------------------------------------------------------------*/
   BEGIN	
      SET @pm_ErrMsg = 'SUCCESS' 
	  IF LTRIM(RTRIM(@pm_AccountID)) IS NULL OR
		 LTRIM(RTRIM(@pm_AccountID)) = ''
	     BEGIN
		    SET @pm_ErrMsg = 'Account ID is required'
			RETURN @pm_ErrMsg
	     END
      IF LTRIM(RTRIM(@pm_UserID)) IS NULL OR
		 LTRIM(RTRIM(@pm_UserID)) = ''
	     BEGIN
		    SET @pm_ErrMsg = 'User ID is required'
			RETURN @pm_ErrMsg
	     END
       SELECT usr.id_user
            , usr.first_name
            , usr.middle_name
            , usr.last_name
            , usr.full_name
            , usr.preferred_name
            , usr.alias_name
            , usr.employee_no
            , usr.generation
            , usr.comments
            , usr.birth_date
            , usr.ssn
            , usr.is_active
            , usr.creation_date
            , usr.created_by
            , usr.last_update_date
            , usr.last_updated_by
            , usr.id_external_reference
            , usr.Login_id
            , usr.domain
            , usr.email_address
            , usr.[password]
            , usr.password_salt
            , usr.is_deleted
            , usr.id_directory
            , usr.directory_type
            , usr.directory_cn
            , usr.directory_usn_changed
            , usr.directory_when_changed
            , usr.directory_when_sychronized
            , usr.Islocked_out
            , usr.last_login_date
            , usr.password_expiry_date
            , usr.mobile_phone_number
            , usr.password_format
            , usr.mobile_pin
            , usr.is_approved
            , usr.last_password_changed_date
            , usr.last_locked_out_date
            , usr.failed_password_attempt_count
            , usr.failed_password_answer_attempt_count
            , usr.id_honorific
			, hnr.honorific_code
			, hnr.honorfic_name
            , usr.id_mobile_carrier
            , mbc.mobile_carrier_code
            , mbc.mobile_carrier_name
            , mbc.gateway
            , usr.id_account
            , act.account_code
            , act.account_name
            , usr.last_approved_date
            , usr.last_active_date
            , usr.last_deleted_date
            , usr.failed_login_count
			, img.id_image
			, img.id_image_type
			, img.image_code
			, img.image_full_path
			, img.image_name
			, img.width
			, img.height
			, img.pixel_format
			, img.resolution
			, img.image_prerendered_file_folder			
	     FROM [user] usr
		      INNER JOIN account act ON usr.id_account = act.id_account AND
			                            act.enabled_flag = 1            AND
										act.id_account = @pm_AccountID
              LEFT JOIN honorific hnr ON hnr.id_honorific = usr.id_honorific AND
			                             hnr.enabled_flag = 1                 
              LEFT JOIN mobile_carrier mbc ON mbc.id_mobile_carrier = usr.id_mobile_carrier AND
			                                 mbc.enabled_flag = 1 
			  LEFT JOIN [Image]       img ON img.ID_User = usr.ID_User AND
			                                 img.enabled_flag = 1  	   AND 
											 img.ID_Account  = act.ID_Account 
        WHERE usr.id_user = @pm_UserID
     ORDER BY usr.id_user
   END


GO
